package com.github.wxiaoqi.security.biz.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.fightGroup.FightGroupCategoryInfoBiz;
import com.github.wxiaoqi.security.biz.biz.fightGroup.FightGroupGoodsInfoBiz;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupDetailParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupGoodsDetailParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 拼团相关接口
 * @author he
 *
 */
@Slf4j
@Api(tags="拼团管理")
@Controller
@RequestMapping("api/fightGroup")
public class FightGroupController extends BaseController{
	
	@Autowired
	private FightGroupGoodsInfoBiz fightGroupGoodsInfoBiz;
	@Autowired
	private FightGroupCategoryInfoBiz fightGroupCategoryInfoBiz;
	
	/**
	 * 查询拼团商品信息列表
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询拼团商品信息列表")
	@RequestMapping(value = "/query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryFightGroupGoodsInfo(@RequestBody BaseParam baseParam) throws Exception {
		Map<String, Object> response = fightGroupGoodsInfoBiz.queryFightGroupGoodsInfo(EntityUtils.beanToMap(baseParam));
		return response;
    }
	
	/**
	 * 查询拼团商品信息明细
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询拼团商品信息明细")
	@RequestMapping(value = "/queryGoodsDetailInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryFightGroupGoodsDetailInfo(@RequestBody FightGroupGoodsDetailParam fightGroupDetailParam) throws Exception {
		Map<String, Object> response = fightGroupGoodsInfoBiz.queryFightGroupGoodsDetailInfo(EntityUtils.beanToMap(fightGroupDetailParam));
		return response;
	}
	
	/**
	 * 查询拼团详情
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询拼团详情")
	@RequestMapping(value = "/queryDetailInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryFightGroupDetailInfo(@RequestBody FightGroupDetailParam fightGroupDetailParam) throws Exception {
		Map<String, Object> response = fightGroupCategoryInfoBiz.queryFightGroupDetailInfo(EntityUtils.beanToMap(fightGroupDetailParam));
		return response;
	}

}
