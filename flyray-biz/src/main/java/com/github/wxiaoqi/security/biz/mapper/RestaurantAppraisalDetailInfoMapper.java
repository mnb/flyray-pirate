package com.github.wxiaoqi.security.biz.mapper;

import java.util.List;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantAppraisalDetailInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 菜品评价明细表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantAppraisalDetailInfoMapper extends Mapper<RestaurantAppraisalDetailInfo> {
	
	/**
	 * 按评论时间降序查询评论明细列表
	 * @param restaurantAppraisalDetailInfo
	 * @return
	 */
	public List<RestaurantAppraisalDetailInfo> queryDetailInfoList(RestaurantAppraisalDetailInfo restaurantAppraisalDetailInfo);
	
}
