package com.github.wxiaoqi.security.biz.entity.fightGroup;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 团成员信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Table(name = "fight_group_member_info")
public class FightGroupMemberInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "mer_id")
	private String merId;

	//团id
	@Column(name = "group_id")
	private Integer groupId;

	//用户账号
	@Column(name = "per_id")
	private String perId;

	//用户名称
	@Column(name = "name")
	private String name;

	//用户头像
	@Column(name = "user_head_portrait")
	private String userHeadPortrait;

	//省
	@Column(name = "province")
	private String province;

	//市
	@Column(name = "city")
	private String city;
	
	//创建时间
	@Column(name = "create_time")
	private String createTime;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerId() {
		return merId;
	}
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 设置：团id
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	/**
	 * 获取：团id
	 */
	public Integer getGroupId() {
		return groupId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：用户名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：用户名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：用户头像
	 */
	public void setUserHeadPortrait(String userHeadPortrait) {
		this.userHeadPortrait = userHeadPortrait;
	}
	/**
	 * 获取：用户头像
	 */
	public String getUserHeadPortrait() {
		return userHeadPortrait;
	}
	/**
	 * 设置：省
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * 获取：省
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * 设置：市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：市
	 */
	public String getCity() {
		return city;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
