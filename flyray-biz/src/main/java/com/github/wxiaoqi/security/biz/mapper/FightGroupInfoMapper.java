package com.github.wxiaoqi.security.biz.mapper;

import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 团信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@org.apache.ibatis.annotations.Mapper
public interface FightGroupInfoMapper extends Mapper<FightGroupInfo> {
	
}
