package com.github.wxiaoqi.security.biz.entity.community;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Table(name = "cms_activity_info")
public class ActivityInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private String id;
	
	    //所属兴趣组
    @Column(name = "interest_group_id")
    private String interestGroupId;
	
	    //活动名称
    @Column(name = "activity_name")
    private String activityName;
	
	    //活动logo
    @Column(name = "activity_logo")
    private String activityLogo;
	
	    //活动摘要
    @Column(name = "activity_des")
    private String activityDes;
	
	    //活动内容
    @Column(name = "activity_content")
    private String activityContent;
	
	    //活动开始时间
    @Column(name = "activity_start_time")
    private Date activityStartTime;
	
	    //活动结束时间
    @Column(name = "activity_end_time")
    private Date activityEndTime;
	
	    //活动地点
    @Column(name = "activity_addr")
    private String activityAddr;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //创建者
    @Column(name = "create_by")
    private String createBy;
	
	    //状态标识 00：无效；10：推荐；20：置顶
    @Column(name = "flag")
    private String flag;
	
	    //活动花絮
    @Column(name = "highlights")
    private String highlights;
    
    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
    
    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	

	/**
	 * 设置：序号
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：所属兴趣组
	 */
	public void setInterestGroupId(String interestGroupId) {
		this.interestGroupId = interestGroupId;
	}
	/**
	 * 获取：所属兴趣组
	 */
	public String getInterestGroupId() {
		return interestGroupId;
	}
	/**
	 * 设置：活动名称
	 */
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	/**
	 * 获取：活动名称
	 */
	public String getActivityName() {
		return activityName;
	}
	/**
	 * 设置：活动logo
	 */
	public void setActivityLogo(String activityLogo) {
		this.activityLogo = activityLogo;
	}
	/**
	 * 获取：活动logo
	 */
	public String getActivityLogo() {
		return activityLogo;
	}
	/**
	 * 设置：活动摘要
	 */
	public void setActivityDes(String activityDes) {
		this.activityDes = activityDes;
	}
	/**
	 * 获取：活动摘要
	 */
	public String getActivityDes() {
		return activityDes;
	}
	/**
	 * 设置：活动内容
	 */
	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}
	/**
	 * 获取：活动内容
	 */
	public String getActivityContent() {
		return activityContent;
	}
	/**
	 * 设置：活动开始时间
	 */
	public void setActivityStartTime(Date activityStartTime) {
		this.activityStartTime = activityStartTime;
	}
	/**
	 * 获取：活动开始时间
	 */
	public Date getActivityStartTime() {
		return activityStartTime;
	}
	/**
	 * 设置：活动结束时间
	 */
	public void setActivityEndTime(Date activityEndTime) {
		this.activityEndTime = activityEndTime;
	}
	/**
	 * 获取：活动结束时间
	 */
	public Date getActivityEndTime() {
		return activityEndTime;
	}
	/**
	 * 设置：活动地点
	 */
	public void setActivityAddr(String activityAddr) {
		this.activityAddr = activityAddr;
	}
	/**
	 * 获取：活动地点
	 */
	public String getActivityAddr() {
		return activityAddr;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建者
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建者
	 */
	public String getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：状态标识 00：无效；10：推荐；20：置顶
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}
	/**
	 * 获取：状态标识 00：无效；10：推荐；20：置顶
	 */
	public String getFlag() {
		return flag;
	}
	/**
	 * 设置：活动花絮
	 */
	public void setHighlights(String highlights) {
		this.highlights = highlights;
	}
	/**
	 * 获取：活动花絮
	 */
	public String getHighlights() {
		return highlights;
	}
	
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	@Override
	public String toString() {
		return "ActivityInfo [id=" + id + ", interestGroupId=" + interestGroupId + ", activityName=" + activityName
				+ ", activityLogo=" + activityLogo + ", activityDes=" + activityDes + ", activityContent="
				+ activityContent + ", activityStartTime=" + activityStartTime + ", activityEndTime=" + activityEndTime
				+ ", activityAddr=" + activityAddr + ", createTime=" + createTime + ", createBy=" + createBy + ", flag="
				+ flag + ", highlights=" + highlights + ", merchantId=" + merchantId + ", platformId=" + platformId
				+ "]";
	}

}
