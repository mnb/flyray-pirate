package com.github.wxiaoqi.security.biz.entity.restaurant;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 菜品类目表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_dishes_category_info")
public class RestaurantDishesCategoryInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//菜品类目id
	@Column(name = "category_id")
	private Integer categoryId;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "mer_id")
	private String merId;

	//类目名称
	@Column(name = "name")
	private String name;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：类目名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：类目名称
	 */
	public String getName() {
		return name;
	}
}
