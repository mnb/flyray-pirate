package com.github.wxiaoqi.security.biz.biz.fightGroup;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 团成员信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Service
public class FightGroupMemberInfoBiz extends BaseBiz<FightGroupMemberInfoMapper,FightGroupMemberInfo> {
}