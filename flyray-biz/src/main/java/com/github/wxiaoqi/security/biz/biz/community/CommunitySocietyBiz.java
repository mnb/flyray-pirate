package com.github.wxiaoqi.security.biz.biz.community;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.entity.community.CommunitySociety;
import com.github.wxiaoqi.security.biz.mapper.CommunitySocietyMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQuerySocietyParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Slf4j
@Service
public class CommunitySocietyBiz extends BaseBiz<CommunitySocietyMapper,CommunitySociety> {
	
	
	/**
	 * 查询社群
	 * @param map
	 * @return
	 */
	public Map<String, Object> querySocietyInfo(CmsQuerySocietyParam param) {
		Example example =new Example(CommunitySociety.class);
		example.createCriteria().andEqualTo("merchantId", param.getMerchantId());
		example.createCriteria().andEqualTo("platformId",param.getPlatformId());
		Integer page = param.getCurrentPage();
		Integer limit = param.getPageSize();
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
		Query query = new Query(map);
		Page<CommunitySociety> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<CommunitySociety> list = mapper.selectByExample(example);
		TableResultResponse<CommunitySociety> table = new TableResultResponse<>(result.getTotal(), list);
		log.info("社群列表。。。{}"+list);
		Map<String, Object> respMap = new java.util.HashMap<>();
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		respMap.put("success", true);
		respMap.put("body", table);
		return respMap;
	}
	
}