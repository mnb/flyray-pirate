package com.github.wxiaoqi.security.biz.mapper;

import com.github.wxiaoqi.security.biz.entity.community.CommunityViewpoint;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@org.apache.ibatis.annotations.Mapper
public interface CommunityViewpointMapper extends Mapper<CommunityViewpoint> {
	
}
