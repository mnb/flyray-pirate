package com.github.wxiaoqi.security.biz.biz.community;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.entity.community.CommunityViewpoint;
import com.github.wxiaoqi.security.biz.mapper.CommunityViewpointMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryViewPointParam;
import com.github.wxiaoqi.security.common.cms.request.CmsSaveViewPointParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.ImageBase64;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;



/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@Slf4j
@Service
public class CommunityViewpointBiz extends BaseBiz<CommunityViewpointMapper,CommunityViewpoint> {
	
	@Value("${spring.imgPath}")
	private String imgPath;
	
	/**
	 * 查询观点列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryViewPoint(CmsQueryViewPointParam param) {
		 log.info("查询观点列表，请求参数。。。{}"+param);
		 Example example = new Example(CommunityViewpoint.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("viewType", param.getViewType());
		 criteria.andEqualTo("merchantId", param.getMerchantId());
		 criteria.andEqualTo("platformId",param.getPlatformId());
		 example.setOrderByClause("point_time desc");
		 List<CommunityViewpoint> list = mapper.selectByExample(example);
		 log.info("观点列表。。。{}"+list);
		 Map<String, Object> respMap = new java.util.HashMap<>();
		 respMap.put("code", ResponseCode.OK.getCode());
		 respMap.put("msg", ResponseCode.OK.getMessage());
		 respMap.put("success", true);
		 respMap.put("body", list);
		 return respMap;
	}
	
	/**
	 * 添加观点
	 * @param param
	 * @return
	 */
	public Map<String, Object> addViewPoint(CmsSaveViewPointParam param) {
		Long id = SnowFlake.getId();
		String pointText = param.getPointText();
		String customerId = param.getCustomerId();
		String imgFile64 = param.getPointImg();
		String viewType = param.getViewType();
		CommunityViewpoint viewpoint = new CommunityViewpoint();
		if ("" != imgFile64 && null != imgFile64) {
			String img64[] = imgFile64.split(",");
			String imgFileName = param.getImgFileName();
			String suffix = imgFileName.substring(imgFileName.lastIndexOf(".") + 1);  
			Long time = new Date().getTime();
			String newName = time + "." + suffix;
			String url = imgPath + File.separator + id + File.separator + newName;
			log.info("生产图片路径---{}",url);
			Boolean flag = ImageBase64.generateImage(img64[1], url);
			log.info("生产图片路径和目录---{}",flag);
			viewpoint.setPointImg(newName);
		}
		viewpoint.setPointText(pointText);
		viewpoint.setCustomerId(customerId);
		viewpoint.setFavortCount(0);
		viewpoint.setCommentCount(0);
		viewpoint.setId(String.valueOf(id));
		viewpoint.setPointTime(new Date());
		viewpoint.setViewType(viewType);
		try {
			mapper.insert(viewpoint);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Map<String, Object> respMap = new HashMap<>();
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		respMap.put("success", true);
		return respMap;
	}
	
}