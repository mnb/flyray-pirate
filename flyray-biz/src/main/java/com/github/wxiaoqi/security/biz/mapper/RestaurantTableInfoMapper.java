package com.github.wxiaoqi.security.biz.mapper;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantTableInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 餐桌信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantTableInfoMapper extends Mapper<RestaurantTableInfo> {
	
}
