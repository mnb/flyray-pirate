package com.github.wxiaoqi.security.biz.biz.fightGroup;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupCategoryInfo;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupGoodsInfo;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupGoodsPicture;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupInfo;
import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.mapper.FightGroupCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupGoodsInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupGoodsPictureMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 团类目信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Service
public class FightGroupCategoryInfoBiz extends BaseBiz<FightGroupCategoryInfoMapper,FightGroupCategoryInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(FightGroupCategoryInfoBiz.class);

	@Autowired
	private FightGroupGoodsInfoMapper fightGroupGoodsInfoMapper;
	@Autowired
	private FightGroupInfoMapper fightGroupInfoMapper;
	@Autowired
	private FightGroupCategoryInfoMapper fightGroupCategoryInfoMapper;
	@Autowired
	private FightGroupMemberInfoMapper fightGroupMemberInfoMapper;
	@Autowired
	private FightGroupGoodsPictureMapper fightGroupGoodsPictureMapper;

	/**
	 * 查询拼团详情
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryFightGroupDetailInfo(Map<String, Object> request){
		logger.info("查询拼团详情请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String goodsId = (String) request.get("goodsId");
		String perId = (String) request.get("perId");
		String groupId = (String) request.get("groupId");
		FightGroupGoodsInfo fightGroupGoodsInfo = new FightGroupGoodsInfo();
		fightGroupGoodsInfo.setPlatformId(platFormId);
		fightGroupGoodsInfo.setMerId(merId);
		fightGroupGoodsInfo.setGoodsId(goodsId);
		FightGroupGoodsInfo goodsInfoInfo = fightGroupGoodsInfoMapper.selectOne(fightGroupGoodsInfo);
		
		FightGroupGoodsPicture fightGroupGoodsPicture = new FightGroupGoodsPicture();
		fightGroupGoodsPicture.setPlatformId(platFormId);
		fightGroupGoodsPicture.setMerId(merId);
		fightGroupGoodsPicture.setGoodsId(Integer.valueOf(goodsId));
		List<FightGroupGoodsPicture> pictureInfo = fightGroupGoodsPictureMapper.select(fightGroupGoodsPicture);

		//查询团
		FightGroupInfo fightGroupInfo = new FightGroupInfo();
		fightGroupInfo.setPlatformId(platFormId);
		fightGroupInfo.setMerId(merId);
		fightGroupInfo.setGroupId(Integer.valueOf(groupId));
		FightGroupInfo selectGroupInfo = fightGroupInfoMapper.selectOne(fightGroupInfo);

		//查询团类目
		FightGroupCategoryInfo fightGroupCategoryInfo = new FightGroupCategoryInfo();
		fightGroupCategoryInfo.setPlatformId(platFormId);
		fightGroupCategoryInfo.setMerId(merId);
		fightGroupCategoryInfo.setGoodsId(Integer.valueOf(goodsId));
		fightGroupCategoryInfo.setGroupsId(selectGroupInfo.getGroupsId());
		FightGroupCategoryInfo groupCategoryInfo = fightGroupCategoryInfoMapper.selectOne(fightGroupCategoryInfo);

		//查询团成员
		Example example = new Example(FightGroupMemberInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platFormId);
		criteria.andEqualTo("merId", merId);
		criteria.andEqualTo("groupId", groupId);
		example.setOrderByClause("create_time");
		List<FightGroupMemberInfo> selectByExample = fightGroupMemberInfoMapper.selectByExample(example);
		
		
		FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
		fightGroupMemberInfo.setPlatformId(platFormId);
		fightGroupMemberInfo.setMerId(merId);
		fightGroupMemberInfo.setGroupId(Integer.valueOf(groupId));
		fightGroupMemberInfo.setPerId(selectGroupInfo.getHeadPerId());
		FightGroupMemberInfo selectMemberInfo = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);

		//查询用户是否参团
		fightGroupMemberInfo.setPerId(perId);
		FightGroupMemberInfo selectByPerId = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);
		Timestamp endDate = selectGroupInfo.getEndDate();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> fightGroupMap = EntityUtils.beanToMap(selectGroupInfo);
		Map<String, Object> memberMap = EntityUtils.beanToMap(selectMemberInfo);
		fightGroupMap.putAll(memberMap);
		if(null != selectByPerId){
			//标识已参团
			fightGroupMap.put("isJoin", "00");//00表示已参团
		}
		fightGroupMap.put("endDate", sdf.format(endDate));
		fightGroupMap.put("groupName", groupCategoryInfo.getGroupName());
		fightGroupMap.put("peopleNum", groupCategoryInfo.getPeopleNum());
		if(StringUtils.isEmpty(groupCategoryInfo.getPeopleNum())){
			fightGroupMap.put("lastPeoplenum", "*");
		}else{
			fightGroupMap.put("lastPeoplenum", groupCategoryInfo.getPeopleNum()-selectGroupInfo.getJoinedGroupNum());
		}

		response.put("pictureInfo", pictureInfo);
		response.put("memberInfo", selectByExample);
		response.put("fightGroupMap", fightGroupMap);
		response.put("goodsInfo", goodsInfoInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("查询拼团详情响应。。。。。。{}", response);
		return response;
	}
}