package com.github.wxiaoqi.security.biz.mapper;

import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupCategoryInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 团类目信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@org.apache.ibatis.annotations.Mapper
public interface FightGroupCategoryInfoMapper extends Mapper<FightGroupCategoryInfo> {
	
}
