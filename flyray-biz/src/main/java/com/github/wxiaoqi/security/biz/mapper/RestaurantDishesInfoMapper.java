package com.github.wxiaoqi.security.biz.mapper;

import java.util.List;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 菜品信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantDishesInfoMapper extends Mapper<RestaurantDishesInfo> {
	
	/**
	 * 查询点餐菜品列表
	 * @param restaurantDishesInfo
	 * @return
	 */
	public List<RestaurantDishesInfo> queryOrderDishes(RestaurantDishesInfo restaurantDishesInfo);
	
	/**
	 * 查询外卖菜品列表
	 * @param restaurantDishesInfo
	 * @return
	 */
	public List<RestaurantDishesInfo> queryTakeawayDishes(RestaurantDishesInfo restaurantDishesInfo);
	
}
