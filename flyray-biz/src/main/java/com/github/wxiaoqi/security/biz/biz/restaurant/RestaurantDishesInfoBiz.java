package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 菜品信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantDishesInfoBiz extends BaseBiz<RestaurantDishesInfoMapper,RestaurantDishesInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantDishesInfoBiz.class);
	
	@Autowired
	private RestaurantDishesInfoMapper restaurantDishesInfoMapper;
	
	/**
	 * 菜品详情查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDishesDetailInfo(Map<String, Object> request){
		logger.info("菜品详情查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String dishesId = (String) request.get("dishesId");
		RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
		restaurantDishesInfo.setPlatformId(platFormId);
		restaurantDishesInfo.setMerId(merId);
		restaurantDishesInfo.setDishesId(Integer.valueOf(dishesId));
		RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", selectDishesInfo);
 		response.put("dishesInfo", map);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("菜品详情查询响应。。。。。。{}", response);
		return response;
	}
}