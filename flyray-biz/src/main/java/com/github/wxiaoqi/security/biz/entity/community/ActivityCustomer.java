package com.github.wxiaoqi.security.biz.entity.community;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Table(name = "cms_activity_customer")
public class ActivityCustomer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //活动序号
    @Id
    private String activityId;
	
	    //用户序号
    @Column(name = "customer_id")
    private String customerId;
	

	/**
	 * 设置：活动序号
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	/**
	 * 获取：活动序号
	 */
	public String getActivityId() {
		return activityId;
	}
	/**
	 * 设置：用户序号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户序号
	 */
	public String getCustomerId() {
		return customerId;
	}
	@Override
	public String toString() {
		return "ActivityCustomer [activityId=" + activityId + ", customerId=" + customerId + "]";
	}
	
}
