package com.github.wxiaoqi.security.biz.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.biz.client.PythonFeignClient;
import com.github.wxiaoqi.security.biz.entity.Message;

import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author yangyongjie
 * @create 2017-10-22 20:30
 */
@RestController
@RequestMapping("test")
public class PythonController {
    //@Autowired
    //private  PythonFeignClient pythonFeignClient;
    
    @ApiOperation(value = "测试使用", notes = "")
    @RequestMapping(value = "/test/{id}", method = RequestMethod.GET)
    public String getTest(@PathVariable int id) {
    	return "ok";
        //return pythonFeignClient.getTest(id);
    }

    @ApiOperation(value = "测试使用", notes = "")
    @RequestMapping(value = "/message/{id}", method = RequestMethod.GET)
    public List<Message> getMsg(@PathVariable Long id) {
    	return null;
        //return pythonFeignClient.getMsg(id);
    }
}