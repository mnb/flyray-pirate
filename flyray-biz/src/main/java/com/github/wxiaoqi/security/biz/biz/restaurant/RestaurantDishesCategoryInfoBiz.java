package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesCategoryInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDishesInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDishesInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantShoppingCartInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/**
 * 菜品类目表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantDishesCategoryInfoBiz extends BaseBiz<RestaurantDishesCategoryInfoMapper,RestaurantDishesCategoryInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantDishesCategoryInfoBiz.class);

	@Autowired
	private RestaurantDishesCategoryInfoMapper restaurantDishesCategoryInfoMapper;
	@Autowired
	private RestaurantDishesInfoMapper restaurantDishesInfoMapper;
	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantShoppingCartInfoMapper restaurantShoppingCartInfoMapper;
	
	/**
	 * 点餐餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryOrderDishesInfo(Map<String, Object> request){
		logger.info("点餐餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String tableId = (String) request.get("tableId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			int id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerId(merId);
			restaurantDishesInfo.setCategoryId(id);
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryOrderDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerId(merId);
				restaurantShoppingCartInfo.setDishesId(dishesInfo.getDishesId());
				restaurantShoppingCartInfo.setTableId(Integer.valueOf(tableId));
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("1");//点餐
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("点餐餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 预约餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryReservationDishesInfo(Map<String, Object> request){
		logger.info("预约餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			int id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerId(merId);
			restaurantDishesInfo.setCategoryId(id);
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryOrderDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerId(merId);
				restaurantShoppingCartInfo.setDishesId(dishesInfo.getDishesId());
				restaurantShoppingCartInfo.setPerId(perId);
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("1");//点餐
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("预约餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 外卖餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTakeawayDishesInfo(Map<String, Object> request){
		logger.info("外卖餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			int id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerId(merId);
			restaurantDishesInfo.setCategoryId(id);
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryTakeawayDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerId(merId);
				restaurantShoppingCartInfo.setDishesId(dishesInfo.getDishesId());
				restaurantShoppingCartInfo.setPerId(perId);
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("2");//外卖
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("外卖餐品信息查询响应。。。。。。{}", response);
		return response;
	}
}