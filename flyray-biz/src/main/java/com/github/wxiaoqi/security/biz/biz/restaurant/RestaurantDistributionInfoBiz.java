package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantDistributionInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantDistributionInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 配送信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantDistributionInfoBiz extends BaseBiz<RestaurantDistributionInfoMapper,RestaurantDistributionInfo> {
	
private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantDistributionInfoBiz.class);
	
	@Autowired
	private RestaurantDistributionInfoMapper restaurantDistributionInfoMapper;
	
	/**
	 * 默认配送地址查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDefaultDistributionInfo(Map<String, Object> request){
		logger.info("默认配送地址查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setDefaultType("1");//默认
		RestaurantDistributionInfo selectDistributionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		response.put("selectDistributionInfo", selectDistributionInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("默认配送地址查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 配送地址列表查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDistributionListInfo(Map<String, Object> request){
		logger.info("配送地址列表查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerId(merId);
		restaurantDistributionInfo.setPerId(perId);
		
		//查询非地址列表
		List<RestaurantDistributionInfo> infoList = restaurantDistributionInfoMapper.selectNotDefault(restaurantDistributionInfo);
		response.put("distributionListInfo", infoList);
		
		//查询默认地址
		restaurantDistributionInfo.setDefaultType("1");//默认
		RestaurantDistributionInfo selectDistributionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		response.put("selectDistributionInfo", selectDistributionInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("配送地址列表查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 设置默认配送地址
	 * @param request
	 * @return
	 */
	public Map<String, Object> settingDefaultDistribution(Map<String, Object> request){
		logger.info("设置默认配送地址请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		Integer distributionId = (Integer) request.get("distributionId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setDefaultType("1");//默认
		//将原来默认改为非默认
		RestaurantDistributionInfo selectDefaultInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		selectDefaultInfo.setDefaultType(null);
		restaurantDistributionInfoMapper.updateByPrimaryKey(selectDefaultInfo);
		
		//设置默认地址
		restaurantDistributionInfo.setDefaultType(null);
		restaurantDistributionInfo.setId(Long.valueOf(distributionId));
		RestaurantDistributionInfo selectDistributionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		selectDistributionInfo.setDefaultType("1");//默认
		restaurantDistributionInfoMapper.updateByPrimaryKey(selectDistributionInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("设置默认配送地址响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 删除配送地址
	 * @param request
	 * @return
	 */
	public Map<String, Object> deleteDistributionInfo(Map<String, Object> request){
		logger.info("删除配送地址请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		Integer distributionId = (Integer) request.get("distributionId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setId(Long.valueOf(distributionId));
		restaurantDistributionInfoMapper.delete(restaurantDistributionInfo);		
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("删除配送地址响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 添加配送地址
	 * @param request
	 * @return
	 */
	public Map<String, Object> addDistributionInfo(Map<String, Object> request){
		logger.info("添加配送地址请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String distributionName = (String) request.get("distributionName");
		String distributionPhone = (String) request.get("distributionPhone");
		String distributionProvince = (String) request.get("distributionProvince");
		String distributionCity = (String) request.get("distributionCity");
		String distributionArea = (String) request.get("distributionArea");
		String distributionAddress = (String) request.get("distributionAddress");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setDistributionName(distributionName);
		restaurantDistributionInfo.setDistributionPhone(distributionPhone);
		restaurantDistributionInfo.setDistributionProvince(distributionProvince);
		restaurantDistributionInfo.setDistributionCity(distributionCity);
		restaurantDistributionInfo.setDistributionArea(distributionArea);
		restaurantDistributionInfo.setDistributionAddress(distributionAddress);
		restaurantDistributionInfoMapper.insert(restaurantDistributionInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("添加配送地址响应。。。。。。{}", response);
		return response;
	}
}