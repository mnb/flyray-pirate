package com.github.wxiaoqi.security.biz.biz.community;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.community.ActivityInfo;
import com.github.wxiaoqi.security.biz.mapper.ActivityInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryActivityParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Slf4j
@Service
public class ActivityInfoBiz extends BaseBiz<ActivityInfoMapper,ActivityInfo> {
	
	
	/**
	 * 查询活动列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryActivity(CmsQueryActivityParam param) {
		log.info("查询活动列表，请求参数。。。{}"+param);
		 Example example =new Example(ActivityInfo.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("merchantId",param.getMerchantId());
		 criteria.andEqualTo("platformId",param.getPlatformId());
		 example.setOrderByClause("create_time desc");
		 List<ActivityInfo> list = mapper.selectByExample(example);
		 log.info("活动列表。。。{}"+list);
		 
		 Map<String, Object> respMap = new HashMap<>();
		 respMap.put("body", list);
		 respMap.put("code", ResponseCode.OK.getCode());
		 respMap.put("msg", ResponseCode.OK.getMessage());
		 respMap.put("success", true);
		 respMap.put("total", list.size());
		 return respMap;
	}
	
	/**
	 * 查询热门活动列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryHotActivity(CmsQueryActivityParam param) {
		log.info("查询热门活动列表，请求参数。。。{}"+param);
		 Example example =new Example(ActivityInfo.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("merchantId",param.getMerchantId());
		 criteria.andEqualTo("flag",param.getFlag());
		 criteria.andEqualTo("platformId",param.getPlatformId());
		 example.setOrderByClause("create_time desc");
		 List<ActivityInfo> list = mapper.selectByExample(example);
		 log.info("热门活动列表。。。{}"+list);
		 
		 Map<String, Object> respMap = new HashMap<>();
		 if (list != null && list.size() > 0) {
			if (list.size() > 3) {
				List<ActivityInfo> list1 = new ArrayList<>();
				for (int i = 0; i < 3; i++) {
					list1.add(list.get(i));
				}
				respMap.put("body", list1);
			}else {
				respMap.put("body", list);
			}
			 respMap.put("code", ResponseCode.OK.getCode());
			 respMap.put("msg", ResponseCode.OK.getMessage());
			 respMap.put("success", true);
		}else {
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
			respMap.put("success", false);
		}
		 return respMap;
	}
}