package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantTableInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantTableInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/**
 * 餐桌信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantTableInfoBiz extends BaseBiz<RestaurantTableInfoMapper,RestaurantTableInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantTableInfoBiz.class);

	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantTableInfoMapper restaurantTableInfoMapper;

	/**
	 * 餐桌信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTableInfo(Map<String, Object> request){
		logger.info("餐桌信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		RestaurantInfo selectRestaurantInfo = restaurantInfoMapper.selectOne(restaurantInfo);
		Map<String, Object> map = EntityUtils.beanToMap(selectRestaurantInfo);
		RestaurantTableInfo restaurantTableInfo = new RestaurantTableInfo();
		restaurantTableInfo.setPlatformId(platFormId);
		restaurantTableInfo.setMerId(merId);
		List<RestaurantTableInfo> selectRestaurantTableInfo = restaurantTableInfoMapper.select(restaurantTableInfo);
		response.put("info", selectRestaurantTableInfo);
		response.put("company", map);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("餐桌信息查询响应。。。。。。{}", response);
		return response;
	}
}