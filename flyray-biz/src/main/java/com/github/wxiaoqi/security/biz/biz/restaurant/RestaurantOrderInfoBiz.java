package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantOrderInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantOrderInfoMapper;
import com.github.wxiaoqi.security.biz.mapper.RestaurantShoppingCartInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 订单表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantOrderInfoBiz extends BaseBiz<RestaurantOrderInfoMapper,RestaurantOrderInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantOrderInfoBiz.class);
	
	@Autowired
	private RestaurantOrderInfoMapper restaurantOrderInfoMapper;
	@Autowired
	private RestaurantShoppingCartInfoMapper restaurantShoppingCartInfoMapper;
	
	/**
	 * 提交外卖订单
	 * @param request
	 * @return
	 */
	public Map<String, Object> settingTakeawayOrder(Map<String, Object> request){
		logger.info("提交外卖订单请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		String perId = (String) request.get("perId");
		String orderInfo = (String) request.get("orderInfo");
		JSONObject orderInfoObject = (JSONObject) JSONObject.parse(orderInfo);
		JSONObject recipientsObject = (JSONObject) orderInfoObject.get("recipients");
		String remark = (String) recipientsObject.get("remark");
		String totalprice = (String) orderInfoObject.get("totalprice");
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId(platFormId);
		restaurantOrderInfo.setMerId(merId);
		restaurantOrderInfo.setPerId(perId);
		restaurantOrderInfo.setPayStatus("02");//待支付
		restaurantOrderInfo.setPayTime(new Date());
		restaurantOrderInfo.setPrice(new BigDecimal(totalprice));
		restaurantOrderInfo.setRemark(remark);
		restaurantOrderInfo.setType("3");//外卖
		restaurantOrderInfoMapper.insert(restaurantOrderInfo);
		
		//修改购物车订单状态
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setPlatformId(platFormId);
		restaurantShoppingCartInfo.setMerId(merId);
		restaurantShoppingCartInfo.setPerId(perId);
		restaurantShoppingCartInfo.setStatus("1");//已选择
		restaurantShoppingCartInfo.setType("2");//1点餐 2外卖
		List<RestaurantShoppingCartInfo> selectList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
		for(RestaurantShoppingCartInfo oneInfo:selectList){
			oneInfo.setStatus("2");//已提交
			restaurantShoppingCartInfoMapper.updateByPrimaryKey(oneInfo);
		}
		//调支付
		
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("提交外卖订单响应。。。。。。{}", response);
		return response;
	}
}