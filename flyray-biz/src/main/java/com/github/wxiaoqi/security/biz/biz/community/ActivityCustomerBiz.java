package com.github.wxiaoqi.security.biz.biz.community;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.community.ActivityCustomer;
import com.github.wxiaoqi.security.biz.mapper.ActivityCustomerMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Service
public class ActivityCustomerBiz extends BaseBiz<ActivityCustomerMapper,ActivityCustomer> {
}