package com.github.wxiaoqi.security.biz.biz.community;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.entity.community.CommunityViewFavort;
import com.github.wxiaoqi.security.biz.entity.community.CommunityViewpoint;
import com.github.wxiaoqi.security.biz.mapper.CommunityViewFavortMapper;
import com.github.wxiaoqi.security.biz.mapper.CommunityViewpointMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@Slf4j
@Service
public class CommunityViewFavortBiz extends BaseBiz<CommunityViewFavortMapper,CommunityViewFavort> {
	
	@Autowired
	private CommunityViewpointMapper viewpointMapper;
	
	public Map<String, Object> doFavort(Map<String, Object> param) {
		log.info("点赞或取消....start...{}", param);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String customerId = (String) param.get("customerId");
			Integer favortStatus = (Integer) param.get("favortStatus");
			String pointId = (String) param.get("pointId");
			String merchantId = (String) param.get("merchantId");
			String platformId = (String) param.get("platformId");
			//根据观点编号查询出观点，然后点赞数量+1
			Map<String, Object> pointMap = new HashMap<String, Object>();
			CommunityViewpoint point = viewpointMapper.selectByPrimaryKey(pointId);
			CommunityViewFavort favort1 = new CommunityViewFavort();
			favort1.setCustomerId(customerId);
			favort1.setFavortStatus(favortStatus);
			favort1.setPointId(pointId);
			favort1.setMerchantId(merchantId);
			favort1.setPlatformId(platformId);
			List<CommunityViewFavort> favortList = mapper.select(favort1);
			CommunityViewFavort favort = new CommunityViewFavort();
			log.info("点赞或取消....favortList.size...{}", favortList.size());
			log.info("点赞或取消....favortStatus...{}", favortStatus);
			if (null != favortList && favortList.size() > 0){
				//说明已经点赞过--更新
				favort = favortList.get(0);
				if(favortStatus == 1){
					favort.setFavortStatus(2);
					Integer count = point.getFavortCount();
					count = count - 1;
					point.setFavortCount(count);
					viewpointMapper.updateByPrimaryKeySelective(point);
				}else if(favortStatus == 2){
					favort.setFavortStatus(1);
					Integer count = point.getFavortCount();
					count = count + 1;
					point.setFavortCount(count);
					viewpointMapper.updateByPrimaryKeySelective(point);
				}
				favort.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				mapper.updateByPrimaryKeySelective(favort);
			} else {
				//没有点赞过--插入
				favort.setId(String.valueOf(SnowFlake.getId()));
				if(favortStatus == 1){
					favort.setFavortStatus(2);
					Integer count = point.getFavortCount();
					count = count - 1;
					point.setFavortCount(count);
					viewpointMapper.updateByPrimaryKeySelective(point);
				}else if(favortStatus == 2){
					favort.setFavortStatus(1);
					Integer count = point.getFavortCount();
					count = count + 1;
					point.setFavortCount(count);
					viewpointMapper.updateByPrimaryKeySelective(point);
					favort.setPlatformId(platformId);
					favort.setMerchantId(merchantId);
					favort.setCustomerId(customerId);
					favort.setPointId(pointId);
					favort.setCreateTime(new Timestamp(System.currentTimeMillis()));
					favort.setUpdateTime(new Timestamp(System.currentTimeMillis()));
					mapper.insert(favort);
				}
				
			}
			result.put("favort", favort);
			result.put("code", "00");
			log.info("点赞或取消....result...{}", result);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "01");
		}
		
		return result;
	}
}