package com.github.wxiaoqi.security.biz.entity.community;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Table(name = "cms_community_society")
public class CommunitySociety implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键id
    @Id
    private String id;
	
	    //二维码
    @Column(name = "society_qr")
    private String societyQr;
	
	    //介绍内容
    @Column(name = "society_content")
    private String societyContent;
	
    //商户号
	@Column(name = "merchant_id")
	private String merchantId;
	
	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	/**
	 * 设置：主键id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：主键id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：二维码
	 */
	public void setSocietyQr(String societyQr) {
		this.societyQr = societyQr;
	}
	/**
	 * 获取：二维码
	 */
	public String getSocietyQr() {
		return societyQr;
	}
	/**
	 * 设置：介绍内容
	 */
	public void setSocietyContent(String societyContent) {
		this.societyContent = societyContent;
	}
	/**
	 * 获取：介绍内容
	 */
	public String getSocietyContent() {
		return societyContent;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	@Override
	public String toString() {
		return "CommunitySociety [id=" + id + ", societyQr=" + societyQr + ", societyContent=" + societyContent
				+ ", merchantId=" + merchantId + ", platformId=" + platformId + "]";
	}
	
}
