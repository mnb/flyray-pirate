package com.github.wxiaoqi.security.biz.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.biz.config.FeignConfiguration;
import com.github.wxiaoqi.security.biz.entity.Message;

import feign.Headers;

/**
 * ${DESCRIPTION}
 *
 * @author yangyongjie
 * @create 2017-10-22 20:30
 */

//@FeignClient(name = "flyray-admin",configuration = FeignConfiguration.class)
public interface PythonFeignClient {
    //parse param like /message?id=12
    @RequestMapping("/message/{id}")
    List<Message> getMsg(@RequestParam("id") Long id);
    //parse url like /test
    @RequestMapping("/test/{id}")
    String getTest(@RequestParam("id") int id);
}