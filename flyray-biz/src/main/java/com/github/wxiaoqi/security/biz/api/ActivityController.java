package com.github.wxiaoqi.security.biz.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.biz.community.ActivityInfoBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryActivityParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;


/**
 * 小程序活动相关接口
 * @author Administrator
 *
 */
@Api(tags="活动管理")
@Slf4j
@Controller
@RequestMapping("api/activities")
public class ActivityController extends BaseController {
	
	@Autowired
	private ActivityInfoBiz activityBiz;
	
	@ApiOperation("查询热门活动列表")
	@ResponseBody
	@RequestMapping(value = "/hotList", method = RequestMethod.POST)
	public Map<String, Object> queryHotActivies(@RequestBody CmsQueryActivityParam param) {
		log.info("查询热门活动 请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = activityBiz.queryHotActivity(param);
		log.info("查询热门活动 响应参数：{}"+param);
		return respMap;
	}
	
	
	/**
	 * 查询所有活动列表
	 * @param param
	 * @return
	 */
	@ApiOperation("查询所有活动列表")
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody CmsQueryActivityParam param) {
		log.info("查询所有活动------start------{}", param);
		Map<String, Object> respMap = activityBiz.queryActivity(param);
		log.info("查询活动首页信息------查询推荐活动记录------{}", respMap);
		return respMap;
	}
}
