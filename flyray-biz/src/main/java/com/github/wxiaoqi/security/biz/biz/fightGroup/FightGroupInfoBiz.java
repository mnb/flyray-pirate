package com.github.wxiaoqi.security.biz.biz.fightGroup;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.fightGroup.FightGroupInfo;
import com.github.wxiaoqi.security.biz.mapper.FightGroupInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 团信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Service
public class FightGroupInfoBiz extends BaseBiz<FightGroupInfoMapper,FightGroupInfo> {
}