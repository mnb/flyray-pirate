package com.github.wxiaoqi.security.biz.biz.community;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.entity.community.CommunityComment;
import com.github.wxiaoqi.security.biz.entity.community.CommunityViewpoint;
import com.github.wxiaoqi.security.biz.mapper.CommunityCommentMapper;
import com.github.wxiaoqi.security.biz.mapper.CommunityViewpointMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.enums.CommentModuleNo;
import com.github.wxiaoqi.security.common.util.PageUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@Slf4j
@Service
public class CommunityCommentBiz extends BaseBiz<CommunityCommentMapper,CommunityComment> {
	
	@Autowired
	private CommunityViewpointMapper viewPointMapper;
	
	/**
	 * 查询观点评论 
	 */
	@ResponseBody
	@RequestMapping(value="/query", method = RequestMethod.POST)
	public Map<String, Object> queryComment(@RequestBody Map<String, String> param) {

		log.info("请求查询话题---start---{}",param);
		String currentPage = param.get("currentPage");//当前页
		String pageSize = param.get("pageSize");//条数
		String id = param.get("id");//话题编号
		String commentModuleNo = param.get("commentModuleNo");//评论模块编号
		Map<String, Object> queryMap = new HashMap<>();
		queryMap.put("commentTargetId", id);
		queryMap.put("commentModuleNo", commentModuleNo);
		CommunityComment comment1 = new CommunityComment();
		comment1.setCommentTargetId(id);
		comment1.setCommentModuleNo(commentModuleNo);
		Integer total = mapper.selectCount(comment1);
		int pageSizeInt = Integer.valueOf(pageSize);
		PageUtils pageUtil = new PageUtils(total, pageSizeInt, Integer.valueOf(currentPage));
		param.put("totalCount", total.toString());
		log.info("请求查询话题--pageUtil---{}",pageUtil.toString());
		if (isLastPage(param)) {
			return ResponseHelper.success(null,pageUtil, "01", "已经到最后一条了~");
		}
		//queryMap.put("commentType", "1");
		
		List<CommunityComment> list = mapper.select(comment1);
		log.info("请求查询话题--list---{}",list.toString());
		String format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);  
		for (Object object : list) {
			CommunityComment comment = (CommunityComment) object;
//			CustomerBase customer = customerBaseService.queryByCustomerId(comment.getCommentBy());
//			comment.setCommentImg(customer.getAvatar());
			String time = sdf.format(comment.getCommentTime());
			try {
				comment.setCommentTime(sdf.parse(time));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return ResponseHelper.success(list,pageUtil, "00", "查询成功");
	
	}
	
	/**
	 * 查询观点评论 
	 * 查询多级评论
	 */
	@ResponseBody
	@RequestMapping(value="/queryMany", method = RequestMethod.POST)
	public Map<String, Object> queryTwo(@RequestBody Map<String, String> param) {
		log.info("查询观点评论 ---start---{}",param);
		String commentTargetId = param.get("commentTargetId");//观点编号
		Map<String, Object> queryMap = new HashMap<>();
		String commentModuleNo = param.get("commentModuleNo");//评论模块编号
		queryMap.put("commentModuleNo", commentModuleNo);
		queryMap.put("commentTargetId", commentTargetId);
		queryMap.put("commentType", "1");
		CommunityComment comment1 = new CommunityComment();
		comment1.setCommentTargetId(commentTargetId);
		comment1.setCommentModuleNo(commentModuleNo);
		comment1.setCommentType("1");
		List<CommunityComment> commentList = mapper.select(comment1);
		List<Map<String, Object>> resultMaps = new ArrayList<>();
		String format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);  
		Map<String, Object> resultMap;
		for (CommunityComment comment : commentList) {
			try {
				//评论者头像
//				CustomerBase customer = customerBaseService.queryByCustomerId(comment.getCommentBy());
//				comment.setCommentImg(customer.getAvatar());
				String time = sdf.format(comment.getCommentTime());
				comment.setCommentTime(sdf.parse(time));
				resultMap = new HashMap<>();
//				resultMap.putAll(BeanUtils.objectToMap(comment));
				Map<String, Object> subQueryMap = new HashMap<>();
				subQueryMap.put("commentModuleNo", commentModuleNo);
				subQueryMap.put("commentTargetId", commentTargetId);
				subQueryMap.put("commentType", 2);//查询类型为回复的
				subQueryMap.put("parentId", comment.getId());//查询类型为回复的
//				List<Map<String, Object>> subCommentList = commentService.queryList(subQueryMap);
//				//回复者头像
//				for (Map<String, Object> map : subCommentList) {
//					String commentBy = (String) map.get("commentBy");
//					CustomerBase customerp = customerBaseService.queryByCustomerId(commentBy);
//					map.put("commentImg", customer.getAvatar());
//					comment.setCommentImg(customer.getAvatar());
//					String timep = sdf.format(comment.getCommentTime());
//					map.put("commentTimes", timep);
//				}
//				resultMap.put("subComents", subCommentList);
				resultMaps.add(resultMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ResponseHelper.success(resultMaps,null, "00", "查询成功");
	}
	
	/**
	 * 观点回复添加
	 */
	@ResponseBody
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public Map<String, Object> addPointComment(@RequestBody Map<String, Object> param) {
		log.info("观点回复添加 ---start---{}",param);
		String commentType = (String) param.get("commentType");//1评论2回复
		String commentBy = (String) param.get("commentBy");
		String commentModuleNo = (String) param.get("commentModuleNo");
//		CustomerBase custome = customerBaseService.queryByCustomerId(commentBy);
//		param.put("commentByName", custome.getNickname());
		if("2".equals(commentType)) {
			//2、回复
			String commentTargetUserId = (String) param.get("commentTargetUserId");
//			CustomerBase targetCustome = customerBaseService.queryByCustomerId(commentTargetUserId);
//			param.put("commentTargetUserName", targetCustome.getNickname());
			param.put("commentTargetUserId", commentTargetUserId);
		}
		try {
			//如果是观点更新评论量
			if(CommentModuleNo.home_viewpoint.getCode().equals(commentModuleNo)){
				String commentTargetId = (String) param.get("commentTargetId");
				Map<String, Object> pointMap = new HashMap<String, Object>();
				pointMap.put("id", commentTargetId);
				CommunityViewpoint communityViewpoint = new CommunityViewpoint();
				communityViewpoint.setId(commentTargetId);
				List<CommunityViewpoint> pointList = viewPointMapper.select(communityViewpoint);
				if(pointList.size() == 0){
					return ResponseHelper.success(null,null, "01", "未查询到观点记录");
				}
				CommunityViewpoint point = pointList.get(0);
				Integer commenCount = point.getCommentCount();
				commenCount = commenCount + 1;
				point.setCommentCount(commenCount);
				pointMap.put("commentCount", commenCount);
				viewPointMapper.updateByPrimaryKey(point);
			}
			log.info("观点回复添加 ---查询完用户名后---{}",param);
//			CommunityComment comment = commentService.saveAll(param);
//			//评论者头像
////			CustomerBase customer = customerBaseService.queryByCustomerId(comment.getCommentBy());
////			comment.setCommentImg(customer.getAvatar());
//			String format = "yyyy-MM-dd HH:mm:ss";
//			SimpleDateFormat sdf = new SimpleDateFormat(format); 
//			String time = sdf.format(comment.getCommentTime());
//			comment.setCommentTime(sdf.parse(time));
//			log.info("观点回复添加 ---comment---{}",comment.toString());
			return ResponseHelper.success(null,null, "00", "评论成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseHelper.success(null,null, "01", "评论异常");
	}
	
	protected boolean isLastPage(Map<String, String> param) {
		
		String currentPage = (String) param.get("currentPage");//当前页
		PageUtils pageUtil = new PageUtils(Integer.valueOf(param.get("totalCount")), 10, Integer.valueOf(currentPage));
		Integer nextPage = Integer.valueOf(currentPage) + 1;
		boolean flag =  false;
		if(nextPage > pageUtil.getTotalPage()){
			//下一页超出总页数
			flag =  true;
		}
		return flag;
	}
}