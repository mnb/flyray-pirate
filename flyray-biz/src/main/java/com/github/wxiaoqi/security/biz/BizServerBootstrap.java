package com.github.wxiaoqi.security.biz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.github.wxiaoqi.security.auth.client.EnableAceAuthClient;
import com.spring4all.swagger.EnableSwagger2Doc;

/**
 * ${DESCRIPTION}
 *
 * @author bolei
 * @create 2018-4-2 20:30
 */
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients({"com.github.wxiaoqi.security.auth.client.feign","com.github.wxiaoqi.security.biz.client"})
@EnableScheduling
@EnableAceAuthClient
@MapperScan("com.github.wxiaoqi.security.biz.mapper")
@EnableSwagger2Doc
public class BizServerBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(BizServerBootstrap.class, args);
    }
}
