package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantPictureInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/**
 * 餐厅信息表
 *
 * @author he
 * @date 2018-06-29 10:32:56
 */
@Service
public class RestaurantInfoBiz extends BaseBiz<RestaurantInfoMapper,RestaurantInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantInfoBiz.class);
	
	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantPictureInfoBiz restaurantPictureInfoBiz;
	
	/**
	 * 餐厅信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryRestaurantInfo(Map<String, Object> request){
		logger.info("餐厅信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platFormId");
		String merId = (String) request.get("merId");
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(RestaurantInfo restaurantInfos:restaurantInfoList){
			RestaurantPictureInfo restaurantPictureInfo = new RestaurantPictureInfo();
			restaurantPictureInfo.setMerId(restaurantInfos.getMerId());
			restaurantPictureInfo.setPlatformId(restaurantInfos.getPlatformId());
			List<RestaurantPictureInfo> selectList = restaurantPictureInfoBiz.selectList(restaurantPictureInfo);
			Map<String, Object> map = EntityUtils.beanToMap(restaurantInfos);
			map.put("pictureList", selectList);
			list.add(map);
		}
		response.put("info", list);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("餐厅信息查询响应。。。。。。{}", response);
		return response;
	}
	
}