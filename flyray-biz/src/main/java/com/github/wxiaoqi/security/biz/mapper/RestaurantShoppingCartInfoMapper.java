package com.github.wxiaoqi.security.biz.mapper;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantShoppingCartInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 购物车信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantShoppingCartInfoMapper extends Mapper<RestaurantShoppingCartInfo> {
	
	/**
	 * 查询金额总和
	 * @param restaurantShoppingCartInfo
	 * @return
	 */
	public String queryCountByPrice(RestaurantShoppingCartInfo restaurantShoppingCartInfo);
	
	/**
	 * 查询数量总和
	 * @param restaurantShoppingCartInfo
	 * @return
	 */
	public Integer queryCountByCount(RestaurantShoppingCartInfo restaurantShoppingCartInfo);
	
}
