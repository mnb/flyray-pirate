package com.github.wxiaoqi.security.biz.biz.restaurant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantReservationInfo;
import com.github.wxiaoqi.security.biz.mapper.RestaurantReservationInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 预订信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantReservationInfoBiz extends BaseBiz<RestaurantReservationInfoMapper,RestaurantReservationInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantReservationInfoBiz.class);
	
	@Autowired
	private	RestaurantReservationInfoMapper restaurantReservationInfoMapper;
	
	/**
	 * 提交预订信息
	 * @param request
	 * @return
	 */
	public Map<String, Object> submitReservationInfo(Map<String, Object> request){
		logger.info("提交预订信息请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String reservation = (String) request.get("reservation");
		JSONObject reservationObject = (JSONObject) JSONObject.parse(reservation);
		String platFormId = (String) reservationObject.get("platFormId");
		String merId = (String) reservationObject.get("merId");
		String perId = (String) reservationObject.get("perId");
		String reservationName = (String) reservationObject.get("reservationName");
		String reservationPeople = (String) reservationObject.get("reservationPeople");
		String reservationPhone = (String) reservationObject.get("reservationPhone");
		String reservationRemark = (String) reservationObject.get("reservationRemark");
		String reservationDate = (String) reservationObject.get("reservationDate");
		String reservationTime = (String) reservationObject.get("reservationTime");
		RestaurantReservationInfo restaurantReservationInfo = new RestaurantReservationInfo();
		restaurantReservationInfo.setPlatformId(platFormId);
		restaurantReservationInfo.setMerId(merId);
		restaurantReservationInfo.setPerId(perId);
		restaurantReservationInfo.setReservationName(reservationName);
		restaurantReservationInfo.setReservationPeople(reservationPeople);
		restaurantReservationInfo.setReservationPhone(reservationPhone);
		restaurantReservationInfo.setReservationRemark(reservationRemark);
		restaurantReservationInfo.setStatus("1");//已预订
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
		try {
			restaurantReservationInfo.setReservationDate(sdfDate.parse(reservationDate));
			restaurantReservationInfo.setReservationTime(sdfTime.parse(reservationTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		restaurantReservationInfoMapper.insert(restaurantReservationInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("提交预订信息响应。。。。。。{}", response);
		return response;
	}
}