package com.github.wxiaoqi.security.biz.mapper;

import com.github.wxiaoqi.security.biz.entity.restaurant.RestaurantInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 餐厅信息表
 * 
 * @author he
 * @date 2018-06-29 10:32:56
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantInfoMapper extends Mapper<RestaurantInfo> {
	
}
