package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallComment;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface CommentFeign {

	@RequestMapping(value = "/admin/comment/list")
	public Object list(@RequestParam(value = "userId") String userId, @RequestParam(value = "valueId") String valueId, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/comment/create", method = RequestMethod.POST)
	public Object create(LitemallComment comment);

	@RequestMapping(value = "/admin/comment/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/comment/update", method = RequestMethod.POST)
	public Object update(LitemallComment comment);

	@RequestMapping(value = "/admin/comment/delete", method = RequestMethod.POST)
	public Object delete(LitemallComment comment);

}
