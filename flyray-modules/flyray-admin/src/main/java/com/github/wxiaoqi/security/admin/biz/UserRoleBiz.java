package com.github.wxiaoqi.security.admin.biz;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.admin.entity.UserRole;
import com.github.wxiaoqi.security.admin.mapper.UserRoleMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:17:59 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class UserRoleBiz extends BaseBiz<UserRoleMapper,UserRole> {

	public List<UserRole> getUserRoles(Long id) {
		return mapper.getUserRoles(id);
	}

}
