package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.annotation.LoginAdmin;
import com.github.wxiaoqi.security.admin.mall.feign.AuthFeign;

@RestController
@RequestMapping("login")
public class AuthController {
	
    private final Log logger = LogFactory.getLog(AuthController.class);
    
    @Autowired
    private AuthFeign authFeign;

    /*
     *  { username : value, password : value }
     */
    @PostMapping("/login")
    public Object login(@RequestBody String body){
        return authFeign.login(body);
    }

    /*
     *
     */
    @PostMapping("/logout")
    public Object login(@LoginAdmin Integer adminId){
        return authFeign.login(adminId);
    }
}
