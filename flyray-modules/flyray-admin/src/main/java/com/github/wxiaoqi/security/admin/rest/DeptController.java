package com.github.wxiaoqi.security.admin.rest;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.biz.DeptBiz;
import com.github.wxiaoqi.security.admin.constant.AdminCommonConstant;
import com.github.wxiaoqi.security.admin.entity.Dept;
import com.github.wxiaoqi.security.admin.entity.Menu;
import com.github.wxiaoqi.security.admin.vo.DeptTree;
import com.github.wxiaoqi.security.admin.vo.MenuTree;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.TreeUtil;

import tk.mybatis.mapper.entity.Example;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/

@RestController
@RequestMapping("dept")
public class DeptController extends BaseController<DeptBiz, Dept> {
	
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
    @ResponseBody
    public List<DeptTree> getTree(String title) {
        Example example = new Example(Dept.class);
        if (StringUtils.isNotBlank(title)) {
            example.createCriteria().andLike("title", "%" + title + "%");
        }
        return getDeptTree(baseBiz.selectByExample(example), AdminCommonConstant.ROOT);
    }
	
	private List<DeptTree> getDeptTree(List<Dept> depts,int root) {
        List<DeptTree> trees = new ArrayList<DeptTree>();
        DeptTree node = null;
        for (Dept dept : depts) {
            node = new DeptTree();
            BeanUtils.copyProperties(dept, node);
            node.setLabel(dept.getName());
            trees.add(node);
        }
        return TreeUtil.bulid(trees,root) ;
    }

}
