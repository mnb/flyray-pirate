package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallIssue;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface IssueFeign {

	@RequestMapping(value = "/admin/issue/list")
	public Object list(@RequestParam(value = "question") String question, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/issue/create", method = RequestMethod.POST)
	public Object create(LitemallIssue brand);

	@RequestMapping(value = "/admin/issue/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/issue/update", method = RequestMethod.POST)
	public Object update(LitemallIssue brand);

	@RequestMapping(value = "/admin/issue/delete", method = RequestMethod.POST)
	public Object delete(LitemallIssue brand);

}
