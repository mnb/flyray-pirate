package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallGoodsSpecification;
import com.github.wxiaoqi.security.admin.mall.feign.GoodsSpecificationFeign;

@RestController
@RequestMapping("goods-specification")
public class GoodsSpecificationController {
	
    private final Log logger = LogFactory.getLog(GoodsSpecificationController.class);
    
    @Autowired
    private GoodsSpecificationFeign goodsSpecificationFeign;

    @GetMapping("/list")
    public Object list(Integer goodsId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return goodsSpecificationFeign.list(goodsId, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create( @RequestBody LitemallGoodsSpecification goodsSpecification){
        return goodsSpecificationFeign.create(goodsSpecification);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return goodsSpecificationFeign.read(id);
    }

    @PostMapping("/update")
    public Object update( @RequestBody LitemallGoodsSpecification goodsSpecification){
        return goodsSpecificationFeign.update(goodsSpecification);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallGoodsSpecification goodsSpecification){
        return goodsSpecificationFeign.delete(goodsSpecification);
    }

    @GetMapping("/volist")
    public Object volist( Integer id){
        return goodsSpecificationFeign.volist(id);
    }

}
