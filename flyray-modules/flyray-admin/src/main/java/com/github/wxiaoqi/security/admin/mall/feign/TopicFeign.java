package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallTopic;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface TopicFeign {

	@RequestMapping(value = "/admin/topic/list")
	public Object list(@RequestParam(value = "title") String title, @RequestParam(value = "subtitle") String subtitle,@RequestParam(value = "page", defaultValue = "1") Integer page,@RequestParam(value = "limit", defaultValue = "10") Integer limit,@RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/topic/create")
	public Object create(LitemallTopic topic);

	@RequestMapping(value = "/admin/topic/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/topic/update")
	public Object update(LitemallTopic topic);

	@RequestMapping(value = "/admin/topic/delete")
	public Object delete(LitemallTopic topic);

}
