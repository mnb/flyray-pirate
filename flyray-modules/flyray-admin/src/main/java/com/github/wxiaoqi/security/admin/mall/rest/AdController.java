package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallAd;
import com.github.wxiaoqi.security.admin.mall.feign.AdFeign;

@RestController
@RequestMapping("ad")
public class AdController {
	
    private final Log logger = LogFactory.getLog(AdController.class);
    
    @Autowired
    private AdFeign adFeign;

    @GetMapping("/list")
    public Object list(String name, String content,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return adFeign.list(name, content, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallAd ad){
        return adFeign.create(ad);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return adFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallAd ad){
        return adFeign.update(ad);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallAd ad){
        return adFeign.delete(ad);
    }

}
