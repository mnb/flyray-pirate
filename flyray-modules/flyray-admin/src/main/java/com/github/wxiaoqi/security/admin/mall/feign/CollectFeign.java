package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallCollect;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface CollectFeign {

	@RequestMapping(value = "/admin/collect/list")
	public Object list(@RequestParam(value = "userId") String userId, @RequestParam(value = "valueId") String valueId, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/collect/create")
	public Object create(LitemallCollect collect);

	@RequestMapping(value = "/admin/collect/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/collect/update")
	public Object update(LitemallCollect collect);

	@RequestMapping(value = "/admin/collect/delete")
	public Object delete(LitemallCollect collect);

}
