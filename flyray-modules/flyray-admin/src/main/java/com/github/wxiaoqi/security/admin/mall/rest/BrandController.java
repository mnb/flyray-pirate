package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallBrand;
import com.github.wxiaoqi.security.admin.mall.feign.BrandFeign;

@RestController
@RequestMapping("brand")
public class BrandController {
	
    private final Log logger = LogFactory.getLog(BrandController.class);
    
    @Autowired
    private BrandFeign brandFeign;

    @GetMapping("/list")
    public Object list(String id, String name,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return brandFeign.list(id, name, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallBrand brand){
        return brandFeign.create(brand);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return brandFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallBrand brand){
        return brandFeign.update(brand);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallBrand brand){
        return brandFeign.delete(brand);
    }

}
