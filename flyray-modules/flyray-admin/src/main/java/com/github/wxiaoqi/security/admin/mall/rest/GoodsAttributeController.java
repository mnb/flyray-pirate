package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallGoodsAttribute;
import com.github.wxiaoqi.security.admin.mall.feign.GoodsAttributeFeign;

@RestController
@RequestMapping("goods-attribute")
public class GoodsAttributeController {
	
    private final Log logger = LogFactory.getLog(GoodsAttributeController.class);
    
    @Autowired
    private GoodsAttributeFeign goodsAttributeFeign;

    @GetMapping("/list")
    public Object list(Integer goodsId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return goodsAttributeFeign.list(goodsId, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallGoodsAttribute goodsAttribute){
        return goodsAttributeFeign.create(goodsAttribute);
    }

    @GetMapping("/read")
    public Object read(Integer id){
    	return goodsAttributeFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallGoodsAttribute goodsAttribute){
        return goodsAttributeFeign.update(goodsAttribute);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallGoodsAttribute goodsAttribute){
        return goodsAttributeFeign.delete(goodsAttribute);
    }

}
