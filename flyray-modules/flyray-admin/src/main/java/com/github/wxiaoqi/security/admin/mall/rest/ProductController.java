package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallProduct;
import com.github.wxiaoqi.security.admin.mall.feign.ProductFeign;

@RestController
@RequestMapping("product")
public class ProductController {
	
    private final Log logger = LogFactory.getLog(ProductController.class);
    
    @Autowired
    private ProductFeign productFeign;

    @GetMapping("/list")
    public Object list(Integer goodsId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return productFeign.list(goodsId, page, limit, sort, order);
    }

    /**
     *
     * @param adminId
     * @param litemallProduct
     * @return
     */
    @PostMapping("/create")
    public Object create( @RequestBody LitemallProduct litemallProduct){
        return productFeign.create(litemallProduct);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return productFeign.read(id);
    }

    @PostMapping("/update")
    public Object update( @RequestBody LitemallProduct product){
        return productFeign.update(product);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallProduct product){
        return productFeign.delete(product);
    }

}
