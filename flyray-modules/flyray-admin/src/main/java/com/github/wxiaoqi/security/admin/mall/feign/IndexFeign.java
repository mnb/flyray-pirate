package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface IndexFeign {
	
	@RequestMapping(value = "/admin/index/index")
	public Object index();

}
