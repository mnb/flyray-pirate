package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.annotation.LoginAdmin;
import com.github.wxiaoqi.security.admin.mall.domain.LitemallAddress;
import com.github.wxiaoqi.security.admin.mall.feign.AddressFeign;
import com.github.wxiaoqi.security.admin.mall.util.ResponseUtil;

@RestController
@RequestMapping("address")
public class AddressController {
	
    private final Log logger = LogFactory.getLog(AddressController.class);
    
    @Autowired
    private AddressFeign addressFeign;

    @GetMapping("/list")
    public Object list(Integer userId, String name,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return addressFeign.list(userId, name, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallAddress address){
        return addressFeign.create(address);
    }

    @GetMapping("/read")
    public Object read(Integer addressId){
        return addressFeign.read(addressId);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallAddress address){
        return addressFeign.update(address);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallAddress address){
        return addressFeign.delete(address);
    }

}
