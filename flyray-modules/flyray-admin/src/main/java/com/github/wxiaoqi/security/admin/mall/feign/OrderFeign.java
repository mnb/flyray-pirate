package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallOrder;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface OrderFeign {

	@RequestMapping(value = "/admin/order/list")
	public Object list(@RequestParam(value = "userId") Integer userId, @RequestParam(value = "orderSn") String orderSn, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	/*
	 * 目前的逻辑不支持管理员创建
	 */
	@RequestMapping(value = "/admin/order/create", method = RequestMethod.POST)
	public Object create(LitemallOrder order);

	@RequestMapping(value = "/admin/order/read")
	public Object read(Integer id);

	/*
	 * 目前仅仅支持管理员设置发货相关的信息
	 */
	@RequestMapping(value = "/admin/order/update", method = RequestMethod.POST)
	public Object update(LitemallOrder order);

	@RequestMapping(value = "/admin/order/delete", method = RequestMethod.POST)
	public Object delete(LitemallOrder order);

}
