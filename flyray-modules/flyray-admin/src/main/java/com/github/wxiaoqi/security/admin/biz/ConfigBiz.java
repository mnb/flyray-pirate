package com.github.wxiaoqi.security.admin.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.admin.entity.Config;
import com.github.wxiaoqi.security.admin.mapper.ConfigMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午2:20:01 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class ConfigBiz extends BaseBiz<ConfigMapper,Config> {

}
