package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallSearchHistory;
import com.github.wxiaoqi.security.admin.mall.feign.HistoryFeign;

@RestController
@RequestMapping("history")
public class HistoryController {
	
    private final Log logger = LogFactory.getLog(HistoryController.class);
    
    @Autowired
    private HistoryFeign historyFeign;

    @GetMapping("/list")
    public Object list(String userId, String keyword,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return historyFeign.list(userId, keyword, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create( @RequestBody LitemallSearchHistory history){
        return historyFeign.create(history);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return historyFeign.read(id);
    }

    @PostMapping("/update")
    public Object update( @RequestBody LitemallSearchHistory history){
        return historyFeign.update(history);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallSearchHistory history){
        return historyFeign.delete(history);
    }

}
