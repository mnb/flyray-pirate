package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallKeyword;
import com.github.wxiaoqi.security.admin.mall.feign.KeywordFeign;

@RestController
@RequestMapping("keyword")
public class KeywordController {
	
    private final Log logger = LogFactory.getLog(KeywordController.class);
    
    @Autowired
    private KeywordFeign keywordFeign;

    @GetMapping("/list")
    public Object list(String keyword, String url,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return keywordFeign.list(keyword, url, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create( @RequestBody LitemallKeyword keywords){
        return keywordFeign.create(keywords);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return keywordFeign.read(id);
    }

    @PostMapping("/update")
    public Object update( @RequestBody LitemallKeyword keywords){
        return keywordFeign.update(keywords);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallKeyword brand){
        return keywordFeign.delete(brand);
    }

}
