package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.annotation.LoginAdmin;
import com.github.wxiaoqi.security.admin.mall.domain.LitemallCategory;
import com.github.wxiaoqi.security.admin.mall.feign.CategoryFeign;

@RestController
@RequestMapping("category")
public class CategoryController {
	
    private final Log logger = LogFactory.getLog(CategoryController.class);
    
    @Autowired
    private CategoryFeign categoryFeign;

    @GetMapping("/list")
    public Object list(String id, String name,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return categoryFeign.list(id, name, page, limit, sort, order);
    }

    @PostMapping("/create")
    public Object create(@RequestBody LitemallCategory category){
        return categoryFeign.create(category);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return categoryFeign.read(id);
    }

    @PostMapping("/update")
    public Object update(@RequestBody LitemallCategory category){
        return categoryFeign.update(category);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallCategory category){
        return categoryFeign.delete(category);
    }

    @GetMapping("/l1")
    public Object catL1(@LoginAdmin Integer adminId) {
        return categoryFeign.catL1(adminId);
    }

}
