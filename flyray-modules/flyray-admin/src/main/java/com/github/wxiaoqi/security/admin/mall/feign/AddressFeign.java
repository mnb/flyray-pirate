package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallAddress;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface AddressFeign {

	@RequestMapping(value = "/admin/address/list")
	public Object list(@RequestParam(value = "userId") Integer userId,@RequestParam(value = "name") String name, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit,@RequestParam(value = "sort") String sort,@RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/address/create")
	public Object create(LitemallAddress address);

	@RequestMapping(value = "/admin/address/read")
	public Object read(Integer addressId);

	@RequestMapping(value = "/admin/address/update")
	public Object update(LitemallAddress address);

	@RequestMapping(value = "/admin/address/delete")
	public Object delete(LitemallAddress address);

}
