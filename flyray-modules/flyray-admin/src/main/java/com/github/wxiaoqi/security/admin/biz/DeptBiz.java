package com.github.wxiaoqi.security.admin.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.admin.entity.Dept;
import com.github.wxiaoqi.security.admin.mapper.DeptMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:05:54 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class DeptBiz extends BaseBiz<DeptMapper,Dept> {

	public void updateByPlatformId(Dept dept) {
		mapper.updateByPlatformId(dept);
	}

}
