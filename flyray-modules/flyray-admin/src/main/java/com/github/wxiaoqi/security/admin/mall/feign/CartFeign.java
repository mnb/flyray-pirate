package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallCart;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface CartFeign {

	@RequestMapping(value = "/admin/cart/list")
	public Object list(@RequestParam(value = "userId") Integer userId, @RequestParam(value = "goodsId") Integer goodsId, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	/*
	 * 目前的逻辑不支持管理员创建
	 */
	@RequestMapping(value = "/admin/cart/create")
	public Object create(LitemallCart cart);

	@RequestMapping(value = "/admin/cart/read")
	public Object read(Integer id);

	/*
	 * 目前的逻辑不支持管理员创建
	 */
	@RequestMapping(value = "/admin/cart/update")
	public Object update(LitemallCart cart);

	@RequestMapping(value = "/admin/cart/delete")
	public Object delete(LitemallCart cart);

}
