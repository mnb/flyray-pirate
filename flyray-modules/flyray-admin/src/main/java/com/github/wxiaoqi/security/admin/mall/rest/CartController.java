package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallCart;
import com.github.wxiaoqi.security.admin.mall.feign.CartFeign;

@RestController
@RequestMapping("cart")
public class CartController {
	
    private final Log logger = LogFactory.getLog(CartController.class);
    
    @Autowired
    private CartFeign cartFeign;

    @GetMapping("/list")
    public Object list(Integer userId, Integer goodsId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return cartFeign.list(userId, goodsId, page, limit, sort, order);
    }

    /*
     * 目前的逻辑不支持管理员创建
     */
    @PostMapping("/create")
    public Object create(@RequestBody LitemallCart cart){
        return cartFeign.create(cart);
    }

    @GetMapping("/read")
    public Object read(Integer id){
        return cartFeign.read(id);
    }

    /*
     * 目前的逻辑不支持管理员创建
     */
    @PostMapping("/update")
    public Object update(@RequestBody LitemallCart cart){
        return cartFeign.update(cart);
    }

    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallCart cart){
        return cartFeign.delete(cart);
    }

}
