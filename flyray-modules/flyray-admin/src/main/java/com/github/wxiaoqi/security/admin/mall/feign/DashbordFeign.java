package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.admin.mall.annotation.LoginAdmin;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface DashbordFeign {
	
	@RequestMapping(value = "/admin/dashboard")
	public Object info(@LoginAdmin Integer adminId);

}
