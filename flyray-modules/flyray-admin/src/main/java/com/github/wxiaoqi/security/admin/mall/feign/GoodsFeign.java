package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallGoods;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface GoodsFeign {

	@RequestMapping(value = "/admin/goods/list")
	public Object list(@RequestParam(value = "goodsSn") String goodsSn, @RequestParam(value = "name") String name, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/goods/create", method = RequestMethod.POST)
	public Object create(LitemallGoods goods);

	@RequestMapping(value = "/admin/goods/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/goods/update", method = RequestMethod.POST)
	public Object update(LitemallGoods goods);

	@RequestMapping(value = "/admin/goods/delete", method = RequestMethod.POST)
	public Object delete(LitemallGoods goods);

}
