package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.annotation.LoginAdmin;
import com.github.wxiaoqi.security.admin.mall.feign.DashbordFeign;

@RestController
@RequestMapping("dashboard")
public class DashbordController {
	
    private final Log logger = LogFactory.getLog(DashbordController.class);
    
    @Autowired
    private DashbordFeign dashbordFeign;

    @GetMapping("")
    public Object info(@LoginAdmin Integer adminId){
        return dashbordFeign.info(adminId);
    }

}
