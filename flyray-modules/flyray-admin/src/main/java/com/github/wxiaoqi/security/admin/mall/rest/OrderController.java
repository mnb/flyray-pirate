package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallOrder;
import com.github.wxiaoqi.security.admin.mall.feign.OrderFeign;

@RestController
@RequestMapping("order")
public class OrderController {
	
    private final Log logger = LogFactory.getLog(OrderController.class);
    
    @Autowired
    private OrderFeign orderFeign;

    @GetMapping("/list")
    public Object list(Integer userId, String orderSn,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                       String sort, String order){
        return orderFeign.list(userId, orderSn, page, limit, sort, order);
    }

    /*
     * 目前的逻辑不支持管理员创建
     */
    @PostMapping("/create")
    public Object create( @RequestBody LitemallOrder order){
        return orderFeign.create(order);
    }

    @GetMapping("/read")
    public Object read( Integer id){
        return orderFeign.read(id);
    }

    /*
     * 目前仅仅支持管理员设置发货相关的信息
     */
    @PostMapping("/update")
    public Object update( @RequestBody LitemallOrder order){
        return orderFeign.update(order);
    }

    @PostMapping("/delete")
    public Object delete( @RequestBody LitemallOrder order){
        return orderFeign.delete(order);
    }

}
