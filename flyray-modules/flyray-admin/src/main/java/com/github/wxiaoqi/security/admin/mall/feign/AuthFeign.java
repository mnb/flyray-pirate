package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.admin.mall.annotation.LoginAdmin;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface AuthFeign {
	
	@RequestMapping(value = "/admin/login/login")
	public Object login(String body);

	@RequestMapping(value = "/admin/login/logout")
    public Object login(@LoginAdmin Integer adminId);

}
