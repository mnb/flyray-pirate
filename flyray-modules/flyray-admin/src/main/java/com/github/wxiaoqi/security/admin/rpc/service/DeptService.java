package com.github.wxiaoqi.security.admin.rpc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.admin.biz.DeptBiz;
import com.github.wxiaoqi.security.admin.entity.Dept;

/** 
* @author: bolei
* @date：2018年4月16日 下午2:22:58 
* @description：类说明
*/

@Service
public class DeptService {

	@Autowired
    private DeptBiz deptBiz;
	
	/**
	 * 新增机构部门
	 * @param entity
	 */
	public void addDept(Dept entity) {
		deptBiz.insertSelective(entity);
    }

	public void updateByPlatformId(Dept dept) {
		deptBiz.updateByPlatformId(dept);
	}
}
