package com.github.wxiaoqi.security.admin.mall.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.mall.feign.IndexFeign;

@RestController
@RequestMapping("index")
public class IndexController {
	
    private final Log logger = LogFactory.getLog(IndexController.class);
    
    @Autowired
    private IndexFeign indexFeign;

    @RequestMapping("/index")
    public Object index(){
        return indexFeign.index();
    }

}
