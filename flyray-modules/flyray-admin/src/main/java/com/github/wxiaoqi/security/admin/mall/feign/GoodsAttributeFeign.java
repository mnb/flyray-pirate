package com.github.wxiaoqi.security.admin.mall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.wxiaoqi.security.admin.mall.domain.LitemallGoodsAttribute;

@FeignClient(value = "mall-admin-api", configuration = {})
public interface GoodsAttributeFeign {

	@RequestMapping(value = "/admin/goods-attribute/list")
	public Object list(@RequestParam(value = "goodsId") Integer goodsId, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit, @RequestParam(value = "sort") String sort, @RequestParam(value = "order") String order);

	@RequestMapping(value = "/admin/goods-attribute/create", method = RequestMethod.POST)
	public Object create(LitemallGoodsAttribute goodsAttribute);

	@RequestMapping(value = "/admin/goods-attribute/read")
	public Object read(Integer id);

	@RequestMapping(value = "/admin/goods-attribute/update", method = RequestMethod.POST)
	public Object update(LitemallGoodsAttribute goodsAttribute);

	@RequestMapping(value = "/admin/goods-attribute/delete", method = RequestMethod.POST)
	public Object delete(LitemallGoodsAttribute goodsAttribute);

}
