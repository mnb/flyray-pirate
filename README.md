# flyray-pirate
代号为：海盗

spring cloud开发的一套集商城、区块链token发行流通、社区运营等功能的系统

![项目进度](flyray-doc/qq.png)


# 管理后台UI
![项目进度](flyray-doc/login.png)
![项目进度](flyray-doc/index.png)
![项目进度](flyray-doc/dept.png)
![项目进度](flyray-doc/role0.png)
![项目进度](flyray-doc/role.png)

#### 项目代码自动生成步骤
1、修改builder目录下的数据库地址和表名
2、eclipse选中项目右键 run as maven build
3、Goals: mybatis-generator:generate
4、执行 run

# 启动指南

## 须知

## 后端工程启动
### 环境须知
- mysql一个，redis一个，rabbitmq一个
- jdk1.8
- IDE需安装lombok插件

### 运行步骤
- 运行数据库脚本：依次运行数据库：flyray-admin/db/init.sql、flyray-auth-server/db/init.sql、
- 修改配置数据库配置：flyray-admin/src/main/resources/application.yml、flyray-gate/src/main/resources/application.yml
- 按`顺序`运行main类：CenterBootstrap（flyray-center）、ConfigBootstrap（flyray-config）、AuthBootstrap（flyray-auth-server）、AdminBootstrap（flyray-admin）、GatewayServerBootstrap（flyray-gateway-v2）

## 前端工程地址
[点击查看地址](https://gitee.com/boleixiongdi/flyray-pirate-admin-ui)