package com.github.wxiaoqi.security.auth.client.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.github.wxiaoqi.security.auth.client.config.UserAuthConfig;
import com.github.wxiaoqi.security.common.context.BaseContextHandler;
import com.github.wxiaoqi.security.common.exception.BaseException;
import com.github.wxiaoqi.security.common.exception.auth.ClientTokenException;
import com.github.wxiaoqi.security.common.exception.auth.ValidateParamException;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.FilterUtil;
import com.github.wxiaoqi.security.common.util.ValidateSign;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.util.StringUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
/**
 * 校验传参拦截器
 * 验证验签 和校验公共的参数
 */
@Slf4j
public class ValidateParamInterceptor extends HandlerInterceptorAdapter {
	private Logger logger = LoggerFactory.getLogger(ValidateParamInterceptor.class);
	@Value("${salt.key}")
	private String saltkey;//验证签名用的盐值
	@SuppressWarnings("unused")
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String requestjsonStr=EntityUtils.getBodyData(request); 
		logger.info("==============校验验签开始==============");
		logger.info("请求地址------"+request.getRequestURI());
    	logger.info("请求参数字符串------"+requestjsonStr);
		/**
    	 * 获取请求参数
    	 * 验证验签
    	 * */
    	if(StringUtils.isEmpty(requestjsonStr)){//请求参数不能为空
    		throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),ResponseCode.INVALID_FIELDS.getMessage(),false);
    	}
    	Map<String, Object>  decrypqustMap= JSON.parseObject(requestjsonStr);
    	//Map<String, Object>  decrypqustMap= JSON.parseObject(requestjsonStr,new TypeReference<Map<String, Object>>() {});
    	String sign= (String) decrypqustMap.get("sign");
    	if(StringUtils.isEmpty(sign)){//签名不能为空
    		throw new ValidateParamException(ResponseCode.SIGN_NOTNULL.getCode(),ResponseCode.SIGN_NOTNULL.getMessage(),false);
    	}
    	decrypqustMap.remove("sign");
    	//签名用的盐值  在配置文件中配置
    	if(!ValidateSign.checkSign(sign, saltkey, decrypqustMap)){
    		log.info("签名失败:"+"--之前的签名:--"+sign+"---签名盐值--"+saltkey+"---排序后的参数---"+FilterUtil.createLinkString(decrypqustMap));
    		throw new ValidateParamException(ResponseCode.SIGN_MATCH_FAIL.getCode(),ResponseCode.SIGN_MATCH_FAIL.getMessage(),false);
		}
    	logger.info("==============校验公共参数开始==============");
    	logger.info("==服务请求地址==="+request.getRequestURI());
    	if (request.getRequestURI().equals("/customers/userRegister")) {//用户注册
    		if (StringUtil.isEmpty((String) decrypqustMap.get("mobile"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"手机号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("password"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"密码不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("loginRole"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"登录角色不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("simCode"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"短信验证码不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("platformId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"平台编号不能为空",false);
			}
    		
    	}
    	if (request.getRequestURI().equals("/personals/cardInfo/getPersonalBindCardInfoList")||
    			request.getRequestURI().equals("/customers/getCustomerStatistics")||
    			request.getRequestURI().equals("/customers/getSecurityInfo")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
    	}
    	if (request.getRequestURI().equals("/customers/savePersonalBaseInfo")||
    			request.getRequestURI().equals("/personals/verifiedRealName")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("infoType"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"信息类型不能为空",false);
			}
    	}
    	if (request.getRequestURI().equals("/customers/modifyCustomerMobile")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("oldMobile"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"原手机号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("oldSmsCode"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"原手机号验证码不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("newMobile"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"新手机号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("newSmsCode"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"新手机号验证码不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("platformId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"平台编号不能为空",false);
			}
    	}
    	if (request.getRequestURI().equals("/customers/updatePassword")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
//    		if (StringUtil.isEmpty((String) decrypqustMap.get("oldPassword"))) {
//    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"原密码不能为空",false);
//			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("password"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"新密码不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/customers/setPayPassword")||request.getRequestURI().equals("/customers/updatePayPassword")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("mobile"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"手机号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("smsCode"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"手机验证码不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("paymentPassword"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"支付密码不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/customers/updatePayPassword")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("oldPaymentPassword"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"原支付密码不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/customers/getCustomerMessage")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("isread"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"已读状态不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("msgType"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"消息类型不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/customers/updateCustomerMessage")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("type"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"操作类型不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("msgIds"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"消息编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/customers/updateCustomerMessage")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"会员编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("type"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"操作类型不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("msgIds"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"消息编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhTask/selectOneTask")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("id"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhTask/queryTaskPage")) {
    		if (null==decrypqustMap.get("page")) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"当前页码不能为空",false);
			}
    		if (null==decrypqustMap.get("limit")) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"每页数量不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhTaskReceiver/receiver")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("custId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("taskId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"任务编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhTaskReceiver/receiverTaskList")||
    			request.getRequestURI().equals("/bhTaskReceiver/queryAllSend")||
    			request.getRequestURI().equals("/bhResume/queryUseableResume")||
    			request.getRequestURI().equals("/bhIndustry/queryAttention")||
    			request.getRequestURI().equals("/bhTask/queryAttentionTask")||
    			request.getRequestURI().equals("/hunterCenter/queryHunterInfo")
    			) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("custId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		
    	}
    	
    	if (request.getRequestURI().equals("/customers/getLastIncomeRecord")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("platformId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"平台编号不能为空",false);
			}
    		
    	}
    	//申请成为猎手或者悬赏方
    	if (request.getRequestURI().equals("/customers/applyRole")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("operType"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"操作类型不能为空",false);
			}
    		
    	}
    	//切换角色
    	if (request.getRequestURI().equals("/customers/changeRole")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("customerId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("lastLoginRole"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"切换角色不能为空",false);
			}
    		
    	}
    	
    	if (request.getRequestURI().equals("/bhResume/info")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("id"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"编号不能为空",false);
			}
    		
    	}
    	if (request.getRequestURI().equals("/bhResume/send")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("custId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		
    		if (StringUtil.isEmpty((String) decrypqustMap.get("taskId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"任务编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("resumeIds"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"简历编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("platformId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"平台编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhResume/del")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("id"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"简历编号不能为空",false);
			}
    	}
    	if (request.getRequestURI().equals("/bhTask/queyOwnTask")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("merId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"商户编号不能为空",false);
			}
    		
    	}
    	
    	if (request.getRequestURI().equals("/bhTask/queyOwnTask")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("merId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"商户编号不能为空",false);
			}
    	}
    	if (request.getRequestURI().equals("/bhTask/queyOwnTask")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("merId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"商户编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhIndustry/updateAttentionIndustry")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("perId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		/*if (StringUtil.isEmpty((String) decrypqustMap.get("industry"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"行业编号不能为空",false);
			}*/
    	}
    	
    	if (request.getRequestURI().equals("/bhFunction/updateAttentionFunction")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("custId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		/*if (StringUtil.isEmpty((String) decrypqustMap.get("function"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"职能编号不能为空",false);
			}*/
    	}
    	
    	if (request.getRequestURI().equals("/bhLable/updatemyLable")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("custId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"用户编号不能为空",false);
			}
    		/*if (StringUtil.isEmpty((String) decrypqustMap.get("lable"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"标签编号不能为空",false);
			}*/
    	}
    	
    	if (request.getRequestURI().equals("/bhTask/queryMyOtherTask")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("merId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"商户编号不能为空",false);
			}
    	}
    	if (request.getRequestURI().equals("/bhResumeUsed/info")||request.getRequestURI().equals("/bhResumeUsed/inviteInterview")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("resumeId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"简历编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhResumeUsed/updateReceiveResume")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("resumeId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"简历编号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("status"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"状态不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhResumeUsed/info")||request.getRequestURI().equals("/bhResumeUsed/inviteInterview")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("resumeId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"简历编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/bhTask/queryOtherSamePosition")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("taskId"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"任务编号不能为空",false);
			}
    	}
    	
    	if (request.getRequestURI().equals("/customers/resetPassword")) {
    		if (StringUtil.isEmpty((String) decrypqustMap.get("mobile"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"手机号不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("smsCode"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"手机验证码不能为空",false);
			}
    		if (StringUtil.isEmpty((String) decrypqustMap.get("password"))) {
    			throw new ValidateParamException(ResponseCode.INVALID_FIELDS.getCode(),"登录密码不能为空",false);
			}
    	}
    	
    	
		if(null!=requestjsonStr&&!"".equals(requestjsonStr)){
			request.setAttribute("param", requestjsonStr);
		}
		return super.preHandle(request, response, handler);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		BaseContextHandler.remove();
		super.afterCompletion(request, response, handler, ex);
	}
	
}
