package com.github.security.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author lengleng
 */
@EnableDiscoveryClient
@EnableConfigServer
@SpringBootApplication
public class FlyrayConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlyrayConfigApplication.class, args);
    }
}
