package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.CustomerMessage;
import com.github.wxiaoqi.security.crm.core.entity.CustomerMessageRel;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 16:17:27
 */
@org.apache.ibatis.annotations.Mapper
public interface CustomerMessageRelMapper extends Mapper<CustomerMessageRel> {
	/**
	 * 更新用户消息关联表
	 * @author centerroot
	 * @time 创建时间:2018年5月23日下午5:23:37
	 * @param param
	 */
	void updateUserMessage(Map<String, Object> param);
}
