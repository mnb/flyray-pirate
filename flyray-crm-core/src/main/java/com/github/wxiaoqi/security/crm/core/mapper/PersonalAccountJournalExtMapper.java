package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournalExt;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账户流水扩展表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalAccountJournalExtMapper extends Mapper<PersonalAccountJournalExt> {
	
}
