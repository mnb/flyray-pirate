package com.github.wxiaoqi.security.crm.core.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.CustomerBiz;
import com.github.wxiaoqi.security.crm.core.client.UserCenterFeignClient;
import com.github.wxiaoqi.security.crm.core.entity.Customer;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 会员信息
 * @author Administrator
 *
 */

@Slf4j
@Controller
@RequestMapping("admin/customers")
public class CustomerController extends BaseController<CustomerBiz,Customer> {

	@Autowired
	private CustomerBiz customerBiz;
	@Autowired
	private UserCenterFeignClient userCenterFeignClient;
	
	
	/**
	 * 查询个人会员列表
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/queryPersonalMemberList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<Map<String, Object>> queryPersonalMemberList(@RequestBody Map<String, Object> param) {
		param.put("type", "001");
		log.info("查询会员列表，请求参数：{}"+param);
		TableResultResponse<Map<String, Object>> response = customerBiz.queryMemberList(param);
		log.info("查询会员列表，响应参数：{}"+response);
		return response;
	}
	
	/**
	 * 查询商户会员列表
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/queryMerchantMemberList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<Map<String, Object>> queryMerchantMemberList(@RequestBody Map<String, Object> param) {
		param.put("type", "002");
		log.info("查询会员列表，请求参数：{}"+param);
		TableResultResponse<Map<String, Object>> response = customerBiz.queryMemberList(param);
		log.info("查询会员列表，响应参数：{}"+response);
		return response;
	}
	
	/**
	 * 查询会员信息
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/queryMemberInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryMemberInfo(@RequestBody Map<String, Object> param) {
		log.info("查询会员信息，请求参数：{}"+param);
		Map<String, Object> resMap = customerBiz.queryMemberInfo(param);
		Map<String, Object> hunterMap = userCenterFeignClient.hunterCenterInfoManager(param);
		resMap.put("hunterMap", hunterMap);
		log.info("查询会员信息，响应参数：{}"+resMap);
		return resMap;
	}
	/**
	 * 查询商户信息
	 */
	@RequestMapping(value = "/queryMerchantInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryMerchantInfo(@RequestBody Map<String, Object> param) {
		log.info("查询会员信息，请求参数：{}"+param);
		Map<String, Object> resMap = customerBiz.queryMemberInfo(param);
		Map<String, Object> hunterMap = userCenterFeignClient.rewardCenterInfoManager(param);
		resMap.put("hunterMap", hunterMap);
		log.info("查询会员信息，响应参数：{}"+resMap);
		return resMap;
	}
	/**
	 * 修改会员状态
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/updateAccountStatus",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateAccountStatus(@RequestBody Map<String, Object> param) {
		log.info("修改会员信息，请求参数：{}"+param);
		String accountStatus = (String) param.get("accountStatus");
		if (accountStatus.equals("01")) {
			log.info("用户禁用。。。"+param.get("customerId"));
		}else {
			log.info("用户启用。。。"+param.get("customerId"));
		}
		Map<String, Object> resMap = customerBiz.updateAccountStatus(param);
		log.info("修改会员信息，响应参数：{}"+resMap);
		return resMap;
	}
	
}