package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountJournalExtMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountJournalMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

/**
 * 个人账户流水（充、转、提、退、冻结流水）
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class PersonalAccountJournalBiz extends BaseBiz<PersonalAccountJournalMapper,PersonalAccountJournal> {

	@Autowired
	private PersonalAccountJournalMapper personalAccountJournalMapper;
	@Autowired
	private PersonalAccountJournalExtMapper personalAccountJournalExtMapper;
	/**
	 * 统计昨日用户收入
	 * @author centerroot
	 * @time 创建时间:2018年5月18日下午1:55:18
	 * @param custNo
	 * @return
	 */
	public Map<String, Object> getYesterdayIncome(PersonalBase personalBase){
		log.info("统计昨日用户收入===请求参数：{}",personalBase);
		Map<String, Object> respMap = new HashMap<String, Object>();
		Example example = new Example(PersonalAccountJournal.class);
        example.createCriteria().andEqualTo("perId", personalBase.getPerId());
        example.createCriteria().andEqualTo("inOutFlag", "1");// 来账
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        yesterday.set(Calendar.HOUR_OF_DAY, 0);
        yesterday.set(Calendar.MINUTE, 0);
        yesterday.set(Calendar.SECOND, 0);
        yesterday.set(Calendar.MILLISECOND, 0);
        Timestamp startTime = new Timestamp(yesterday.getTimeInMillis());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String Time001 = format.format(yesterday.getTime());
        System.out.println("001日期:" + Time001);
        yesterday.add(Calendar.DATE, 0);
        Timestamp endTime = new Timestamp(yesterday.getTimeInMillis());
        String Time002 = format.format(yesterday.getTime());
        System.out.println("001日期:" + Time002);
        example.createCriteria().andBetween("createtime", startTime, endTime);
		List<PersonalAccountJournal> CustAccountDetailList = personalAccountJournalMapper.selectByExample(example);
		if (null != CustAccountDetailList && CustAccountDetailList.size() > 0) {
			BigDecimal yesterdayTotal = BigDecimal.ZERO;
			for (int i = 0; i < CustAccountDetailList.size(); i++) {
				PersonalAccountJournal custAccountDetailItem = CustAccountDetailList.get(0);
				if (null != custAccountDetailItem.getTradeAmt()) {
					yesterdayTotal.add(custAccountDetailItem.getTradeAmt());
				}
			}
			respMap.put("yesterdayIncome", yesterdayTotal.toString());
		} else {
			respMap.put("yesterdayIncome", "0.00");
		}
		
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());

		log.info("统计昨日用户收入===响应参数：{}",respMap);
		return respMap;
	}
	
	
	/**
	 * 查询流水记录
	 * @param map
	 * @return
	 */
	public List<PersonalAccountJournal> queryAccountJournalList(Map<String, Object> map) {
		log.info("查询个人流水记录参数。。。{}"+map);
		List<PersonalAccountJournal> list = personalAccountJournalMapper.queryAccountJournal(map);
		log.info("查询个人流水记录。。。{}"+list);
		return list;
	}
	
	
	
	
}