package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournalInfo;
import com.github.wxiaoqi.security.crm.core.entity.UnderLineRecharge;

import tk.mybatis.mapper.common.Mapper;

/**
 * 企业账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantAccountJournalMapper extends Mapper<MerchantAccountJournal> {
	/**
	 * 查询商户账户记录
	 * @author centerroot
	 * @time 创建时间:2018年5月24日上午10:29:43
	 * @param param
	 * @return
	 */
	List<MerchantAccountJournalInfo> queryTransactionRecord(Map<String, Object> param);
	
	
	/**
	 * 查询商户流水
	 * @param param
	 * @return
	 */
	List<MerchantAccountJournal> queryMerchantJournal(Map<String, Object> param);
	
	
	/**
	 * 查询线下充值记录
	 * @param param
	 * @return
	 */
	List<UnderLineRecharge> queryUnderlinerecharge(Map<String, Object> param);
}
