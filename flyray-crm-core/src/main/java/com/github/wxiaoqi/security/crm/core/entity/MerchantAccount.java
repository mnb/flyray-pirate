package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 企业账户
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "merchant_account")
public class MerchantAccount implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //账户编号
    @Id
    private String accId;
	
	    //商户编号
    @Column(name = "MER_ID")
    private String merId;
	
	    //账户类型    ACC001：余额账户
    @Column(name = "ACC_TYPE")
    private String accType;
	
	    //币种  CNY：人民币
    @Column(name = "CCY")
    private String ccy;
	
	    //账户余额
    @Column(name = "ACC_BALANCE")
    private BigDecimal accBalance;
	
	    //
    @Column(name = "CHECKSUM")
    private String checksum;

	    //账户状态 00：正常，01：冻结
	@Column(name = "ACCOUNT_STATUS")
	private String accountStatus;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：账户编号
	 */
	public void setAccId(String accId) {
		this.accId = accId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccId() {
		return accId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：账户类型    ACC001：余额账户
	 */
	public void setAccType(String accType) {
		this.accType = accType;
	}
	/**
	 * 获取：账户类型    ACC001：余额账户
	 */
	public String getAccType() {
		return accType;
	}
	/**
	 * 设置：币种  CNY：人民币
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	/**
	 * 获取：币种  CNY：人民币
	 */
	public String getCcy() {
		return ccy;
	}
	/**
	 * 设置：账户余额
	 */
	public void setAccBalance(BigDecimal accBalance) {
		this.accBalance = accBalance;
	}
	/**
	 * 获取：账户余额
	 */
	public BigDecimal getAccBalance() {
		return accBalance;
	}
	/**
	 * 设置：
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	/**
	 * 获取：
	 */
	public String getChecksum() {
		return checksum;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	@Override
	public String toString() {
		return "MerchantAccount [accId=" + accId + ", merId=" + merId + ", accType=" + accType + ", ccy=" + ccy
				+ ", accBalance=" + accBalance + ", checksum=" + checksum + ", accountStatus=" + accountStatus
				+ ", createtime=" + createtime + ", updatetime=" + updatetime + "]";
	}
	
	
}
