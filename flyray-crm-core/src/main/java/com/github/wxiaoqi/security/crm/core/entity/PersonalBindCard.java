package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 用户绑卡表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 16:28:16
 */
@Table(name = "personal_bind_card")
public class PersonalBindCard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String seqNo;
	
	    //个人信息编号
    @Column(name = "PER_ID")
    private String perId;
	
	    //银行卡号  隐藏中间数位的银行卡号
    @Column(name = "BIND_CARD_NO")
    private String bindCardNo;
	
	    //加密银行卡号
    @Column(name = "BIND_ENCRYPT_CARD_NO")
    private String bindEncryptCardNo;
	
	    //收款人姓名
    @Column(name = "PAYEE_NAME")
    private String payeeName;
	
	    //银行编号
    @Column(name = "BANK_NO")
    private String bankNo;
	
	    //银行名称
    @Column(name = "BANK_NAME")
    private String bankName;
	
	    //支行编号
    @Column(name = "SUBBRANCH_NO")
    private String subbranchNo;
	
	    //支行信息
    @Column(name = "SUBBRANCH_NAME")
    private String subbranchName;

	    //绑定状态  00：绑定  01：解绑
	@Column(name = "STATUS")
	private String status;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：流水号
	 */
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：流水号
	 */
	public String getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：个人信息编号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：银行卡号  隐藏中间数位的银行卡号
	 */
	public void setBindCardNo(String bindCardNo) {
		this.bindCardNo = bindCardNo;
	}
	/**
	 * 获取：银行卡号  隐藏中间数位的银行卡号
	 */
	public String getBindCardNo() {
		return bindCardNo;
	}
	/**
	 * 设置：加密银行卡号
	 */
	public void setBindEncryptCardNo(String bindEncryptCardNo) {
		this.bindEncryptCardNo = bindEncryptCardNo;
	}
	/**
	 * 获取：加密银行卡号
	 */
	public String getBindEncryptCardNo() {
		return bindEncryptCardNo;
	}
	/**
	 * 设置：收款人姓名
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}
	/**
	 * 获取：收款人姓名
	 */
	public String getPayeeName() {
		return payeeName;
	}
	/**
	 * 设置：银行编号
	 */
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	/**
	 * 获取：银行编号
	 */
	public String getBankNo() {
		return bankNo;
	}
	/**
	 * 设置：银行名称
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * 获取：银行名称
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * 设置：支行编号
	 */
	public void setSubbranchNo(String subbranchNo) {
		this.subbranchNo = subbranchNo;
	}
	/**
	 * 获取：支行编号
	 */
	public String getSubbranchNo() {
		return subbranchNo;
	}
	/**
	 * 设置：支行信息
	 */
	public void setSubbranchName(String subbranchName) {
		this.subbranchName = subbranchName;
	}
	/**
	 * 获取：支行信息
	 */
	public String getSubbranchName() {
		return subbranchName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	@Override
	public String toString() {
		return "PersonalBindCard [seqNo=" + seqNo + ", perId=" + perId + ", bindCardNo=" + bindCardNo
				+ ", bindEncryptCardNo=" + bindEncryptCardNo + ", payeeName=" + payeeName + ", bankNo=" + bankNo
				+ ", bankName=" + bankName + ", subbranchNo=" + subbranchNo + ", subbranchName=" + subbranchName
				+ ", status=" + status + ", createtime=" + createtime + ", updatetime=" + updatetime + "]";
	}
}
