package com.github.wxiaoqi.security.crm.core.api;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.github.wxiaoqi.security.common.crm.request.ModifyCustomerMobileParam;
import com.github.wxiaoqi.security.common.crm.request.SavePersonalBaseInfoParam;
import com.github.wxiaoqi.security.common.crm.request.SetPayPasswordParam;
import com.github.wxiaoqi.security.common.crm.request.UpdatePasswordParam;
import com.github.wxiaoqi.security.common.crm.request.UpdatePayPasswordParam;
import com.github.wxiaoqi.security.common.crm.request.VerifiedRealNameParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.biz.CustomerBiz;
import com.github.wxiaoqi.security.crm.core.biz.PasswordBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Api(tags="用户信息管理")
@Controller
@RequestMapping("customers")
public class CustomerAction extends BaseAction{
	@Autowired
	private CustomerBiz customerBiz;
	@Autowired
	private PasswordBiz passwordBiz;
	
	/**
	 * 获取用户统计数据
	 * @author centerroot
	 * @time 创建时间:2018年5月18日下午5:01:13
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("获取用户统计数据")
	@RequestMapping(value = "/getCustomerStatistics",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getCustomerStatistics(@ApiParam("用户编号") @RequestParam(value = "customerId", required = true) String customerId) throws Exception{
		log.info("获取用户统计数据 请求参数：{}",customerId);
		Map<String, Object> respMap = customerBiz.getCustomerStatistics(customerId);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("获取用户统计数据 响应参数：{}",respMap);
		return respMap;
	}
	
	
	/**
	 * 用户基本信息录入
	 * @author centerroot
	 * @time 创建时间:2018年5月21日上午10:43:56
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("用户基本信息录入")
	@RequestMapping(value = "/savePersonalBaseInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> savePersonalBaseInfo(@RequestBody SavePersonalBaseInfoParam baseInfoParam) throws Exception{
		log.info("用户基本信息录入 请求参数：{}",EntityUtils.beanToMap(baseInfoParam));
		Map<String, Object> respMap = customerBiz.savePersonalBaseInfo(baseInfoParam);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("用户基本信息录入 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 用户安全信息查询
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("用户安全信息查询")
	@RequestMapping(value = "/getSecurityInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getSecurityInfo(@ApiParam("用户编号") @RequestParam(value = "customerId", required = true) String customerId) throws Exception{
		log.info("用户安全信息状态查询 请求参数：{}",customerId);
		Map<String, Object> respMap = customerBiz.getSecurityInfo(customerId);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("用户安全信息状态查询 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 用户安全信息修改手机号
	 * 1、判断用户是否存在
	 * 2、判断用户原手机号是否正确
	 * 3、判断原手机号和原验证码正确性
	 * 4、判断新手机号和新验证码正确性
	 * 5、更换手机号
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/modifyCustomerMobile",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> modifyCustomerMobile(ModifyCustomerMobileParam param) throws Exception{
		log.info("用户安全信息修改手机号 请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = customerBiz.modifyCustomerMobile(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("用户安全信息修改手机号 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 用户/商户实名认证
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/verifiedRealName",method = RequestMethod.POST)
    @ResponseBody
//    public Map<String, Object> verifiedRealName(@RequestBody Map<String, Object> param) throws Exception{
	public Map<String, Object> verifiedRealName(VerifiedRealNameParam param) throws Exception{
		log.info("用户/商户实名认证    请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = customerBiz.verifiedRealName(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("用户/商户实名认证 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 修改用户登录密码
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/updatePassword",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> updatePassword(UpdatePasswordParam param) throws Exception{
		log.info("修改用户登录密码    请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = passwordBiz.updatePassword(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("修改用户登录密码  响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 修改用户支付密码
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/updatePayPassword",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> updatePayPassword(UpdatePayPasswordParam param) throws Exception{
		log.info("修改用户支付密码    请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = passwordBiz.updatePayPassword(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("修改用户支付密码  响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 设置用户支付密码
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/setPayPassword",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> setPayPassword(SetPayPasswordParam param) throws Exception{
		log.info("设置用户支付密码    请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = passwordBiz.setPayPassword(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("设置用户支付密码  响应参数：{}",respMap);
		return respMap;
	}

	/***
	 * 根据手机号获取用户信息
	 * @param username登录用户名即手机号;
	 * password 登录密码;
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryCustomerinfoByMobile", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryCustomerinfoByMobile(HttpServletRequest request) throws Exception {
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("根据手机号获取用户信息 请求参数：{}", param);
		Map<String, Object> respMap = customerBiz.queryCustomerinfoByMobile(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("根据手机号获取用户信息  响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 查询用户消息列表
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getCustomerMessage",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getCustomerMessage(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("查询用户消息列表    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.getCustomerMessage(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查询用户消息列表    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 修改用户消息状态
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/updateCustomerMessage",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> updateCustomerMessage(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("修改用户消息状态    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.updateCustomerMessage(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("修改用户消息状态    响应参数：{}",respMap);
		return respMap;
	}
	
	
	/**
	 * 查询用户最后收入记录
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getLastIncomeRecord",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getLastIncomeRecord(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("查询用户最后收入记录    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.getLastIncomeRecord(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查询用户最后收入记录    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询用户交易记录
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryPersonaTransactionRecord",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryPersonaTransactionRecord(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("查询用户交易记录    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.queryPersonaTransactionRecord(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查询用户交易记录    响应参数：{}",respMap);
		return respMap;
	}
	
	
	/**
	 * 查询商户交易记录
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryMerchantTransactionRecord",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryMerchantTransactionRecord(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("查询商户交易记录    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.queryMerchantTransactionRecord(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查询商户交易记录    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 新用户注册
	 * @param 
	 * @throws Exception 
	 * 
	 * */
	@RequestMapping(value = "/userRegister",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> userRegister(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("用户注册    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.userRegister(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("用户注册    响应参数：{}",respMap);
		return respMap;
	}
	
	/***
	 * 申请成为猎手或者悬赏方
	 * */
	@RequestMapping(value = "/applyRole",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> applyRole(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("申请成为猎手或者悬赏方    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.applyRole(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("申请成为猎手或者悬赏方    响应参数：{}",respMap);
		return respMap;
	}
	/***
	 * 
	 * 切换登录角色
	 * */
	@RequestMapping(value = "/changeRole",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> changeRole(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("切换登录角色    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.changeRole(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("切换登录角色    响应参数：{}",respMap);
		return respMap;
	}
	
	/***
	 * 当前登录用户退出登录
	 * 
	 * */
	@RequestMapping(value = "/userLoginout",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> userLoginout(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("当前登录用户退出登录    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.userLoginout(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("当前登录用户退出登录    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 重置用户登录密码
	 * @param request
	 * @author hexufeng
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/resetPassword",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> resetPassword(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("重置用户登录密码    请求参数：{}",param);
		Map<String, Object> respMap = passwordBiz.resetPassword(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("重置用户登录密码  响应参数：{}",respMap);
		return respMap;
	}
	
}