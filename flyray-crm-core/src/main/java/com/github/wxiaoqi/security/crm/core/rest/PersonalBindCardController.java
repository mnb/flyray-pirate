package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBindCardBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBindCard;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("personals/bindCard")
public class PersonalBindCardController extends BaseController<PersonalBindCardBiz,PersonalBindCard> {

}