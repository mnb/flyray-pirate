package com.github.wxiaoqi.security.crm.core.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.crm.core.entity.Customer;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerMapper;
import com.github.wxiaoqi.security.common.crm.request.SetPayPasswordParam;
import com.github.wxiaoqi.security.common.crm.request.UpdatePasswordParam;
import com.github.wxiaoqi.security.common.crm.request.UpdatePayPasswordParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.MD5;
import com.github.wxiaoqi.security.common.util.SHA256Utils;

import lombok.extern.slf4j.Slf4j;

/**
 * 密码相关服务
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-17 15:32:14
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class PasswordBiz {
	@Autowired
	private CustomerMapper customerMapper;
	@Autowired
	private SmsGateWayBiz smsGateWayBiz;
	@Autowired
	private RedisTemplate redisTemplate;
	/**
	 * 设置交易密码
	 * 2、判断手机号和验证码正确性
	 * 3、设置交易密码
	 * @author centerroot
	 * @time 创建时间:2018年5月22日下午2:55:13
	 * @param param
	 * @return
	 */
	public Map<String, Object> setPayPassword(SetPayPasswordParam param){
		log.info("设置交易密码  请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = new HashMap<String, Object>();
		/*String customerId = (String) param.get("customerId");
		String mobile = (String) param.get("mobile");
		String smsCode = (String) param.get("smsCode");
		String paymentPassword = (String) param.get("paymentPassword");*/
		Customer customer = customerMapper.selectByPrimaryKey(param.getCustomerId());
		if (null != customer) {
			Map<String, Object> smsMap = new HashMap<String, Object>();
			smsMap.put("mobile", param.getMobile());
			smsMap.put("code", param.getSmsCode());
			Map<String, Object> smsMapResp = smsGateWayBiz.verificationCode(smsMap);
			// 判断手机号和验证码正确性
			if (ResponseCode.OK.getCode().equals(smsMapResp.get("code")) && (Boolean)smsMapResp.get("verifyStatus")) {
				String payPWD = SHA256Utils.SHA256(param.getPaymentPassword()+param.getCustomerId());
				customer.setPayPassword(payPWD);
				customer.setPayPasswordStatus("00");
				customer.setUpdatetime(new Timestamp(System.currentTimeMillis()));
				customerMapper.updateByPrimaryKey(customer);

				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
			} else {
				respMap.put("code", ResponseCode.SMS_CODE_MATCH_FAIL.getCode());
				respMap.put("msg", ResponseCode.SMS_CODE_MATCH_FAIL.getMessage());
			}
		}else{
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}

		log.info("设置交易密码   响应参数：{}",respMap);
		return respMap;
	}


	/**
	 * 修改支付密码
	 * 2、判断原交易密码正确
	 * 3、判断手机号和验证码正确
	 * 4、更改交易密码
	 * @author centerroot
	 * @time 创建时间:2018年5月22日下午3:27:49
	 * @param param
	 * @return
	 */
	public Map<String, Object> updatePayPassword(UpdatePayPasswordParam param){
		log.info("修改支付密码  请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		Customer customer = customerMapper.selectByPrimaryKey(param.getCustomerId());
		if (null != customer) {
			String oldPayPWD = SHA256Utils.SHA256(param.getOldPaymentPassword()+param.getCustomerId());
			if (oldPayPWD.equals(customer.getPayPassword())) {
				Map<String, Object> smsMap = new HashMap<String, Object>();
				smsMap.put("mobile", param.getMobile());
				smsMap.put("code", param.getSmsCode());
				Map<String, Object> smsMapResp = smsGateWayBiz.verificationCode(smsMap);
				// 判断手机号和验证码正确性
				if (ResponseCode.OK.getCode().equals(smsMapResp.get("code")) && (Boolean)smsMapResp.get("verifyStatus")) {
					String payPWD = SHA256Utils.SHA256(param.getPaymentPassword()+param.getCustomerId());
					customer.setPayPassword(payPWD);
					customer.setPayPasswordStatus("00");
					customer.setUpdatetime(new Timestamp(System.currentTimeMillis()));
					customerMapper.updateByPrimaryKey(customer);

					respMap.put("code", ResponseCode.OK.getCode());
					respMap.put("msg", ResponseCode.OK.getMessage());
				} else {
					respMap.put("code", ResponseCode.SMS_CODE_MATCH_FAIL.getCode());
					respMap.put("msg", ResponseCode.SMS_CODE_MATCH_FAIL.getMessage());
				}
			} else {
				respMap.put("code", ResponseCode.PAY_PWD_ERROR.getCode());
				respMap.put("msg", ResponseCode.PAY_PWD_ERROR.getMessage());
			}
		}else{
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}

		log.info("修改支付密码   响应参数：{}",respMap);
		return respMap;
	}

	/**
	 * 修改登录密码
	 * 2、判断用户密码锁定状态
	 * 3、判断原登录密码正确
	 * 4、更改登录密码
	 * @author centerroot
	 * @time 创建时间:2018年5月22日下午3:27:49
	 * @param param
	 * @return
	 */
	public Map<String, Object> updatePassword(UpdatePasswordParam param){
		log.info("修改登录密码  请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		/*String customerId = (String) param.get("customerId");
		String oldPassword = (String) param.get("oldPassword");
		String password = (String) param.get("password");*/

		Customer customer = customerMapper.selectByPrimaryKey(param.getCustomerId());
		if (null != customer) {
			if ("00".equals(customer.getPasswordStatus())) {
				//判断如果没有传原密码且用户信息表中密码为空则不进行校验
				if(StringUtils.isEmpty(param.getOldPassword()) && StringUtils.isEmpty(customer.getPassword())){
					String newPassword = MD5.sign(customer.getPassword(), customer.getSaltvalue(), "utf-8");
					customer.setPassword(newPassword);
					customer.setUpdatetime(new Timestamp(System.currentTimeMillis()));
					customerMapper.updateByPrimaryKey(customer);

					respMap.put("code", ResponseCode.OK.getCode());
					respMap.put("msg", ResponseCode.OK.getMessage());
				}else{
					String oldPWD = MD5.sign(param.getOldPassword(), customer.getSaltvalue(), "utf-8");
					log.info("原： "+param.getOldPassword()+" /加密： "+oldPWD+" /用户密码： "+customer.getPassword());
					if (oldPWD.equals(customer.getPassword())) {
						String newPassword = MD5.sign(customer.getPassword(), customer.getSaltvalue(), "utf-8");
						customer.setPassword(newPassword);
						customer.setUpdatetime(new Timestamp(System.currentTimeMillis()));
						customerMapper.updateByPrimaryKey(customer);

						respMap.put("code", ResponseCode.OK.getCode());
						respMap.put("msg", ResponseCode.OK.getMessage());
					} else {
						respMap.put("code", ResponseCode.PWD_ERROR.getCode());
						respMap.put("msg", ResponseCode.PWD_ERROR.getMessage());
					}
				}
			} else {
				respMap.put("code", ResponseCode.LOCK_USER_PWD.getCode());
				respMap.put("msg", ResponseCode.LOCK_USER_PWD.getMessage());
			}
		}else{
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}

		log.info("修改登录密码   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 重置用户登录密码
	 * @param param
	 * @return
	 */
	public Map<String, Object> resetPassword(Map<String, Object> param){
		log.info("重置用户登录密码  请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String mobile = (String) param.get("mobile");
		String smsCode = (String) param.get("smsCode");
		String password = (String) param.get("password"); 
		Customer queryParam = new Customer();
		queryParam.setMobile(mobile);
		Customer customer = customerMapper.selectOne(queryParam);
		if (null != customer) {
			// 判断手机号和验证码正确性
			log.info("重置用户登录密码---redis 缓存的短信验证码--"+redisTemplate.opsForValue().get("SMS_CODE_KEY:" + mobile));
			if (smsCode.equals(redisTemplate.opsForValue().get("SMS_CODE_KEY:" + mobile))) {
				String saltvalue=customer.getSaltvalue();
				String newpassword=MD5.sign(password,saltvalue, "utf-8");
				customer.setPassword(newpassword);
				customer.setPasswordStatus("00");
				customer.setUpdatetime(new Timestamp(System.currentTimeMillis()));
				customerMapper.updateByPrimaryKey(customer);

				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
			} else {
				respMap.put("code", ResponseCode.SMS_CODE_MATCH_FAIL.getCode());
				respMap.put("msg", ResponseCode.SMS_CODE_MATCH_FAIL.getMessage());
			}
		}else{
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}

		log.info("重置用户登录密码   响应参数：{}",respMap);
		return respMap;
	}

}