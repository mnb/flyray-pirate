package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 个人账单
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@Table(name = "personal_billing")
public class PersonalBilling implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //账单编号
    @Id
    private String billId;
	
	    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
	
	    //个人信息编号
    @Column(name = "PER_ID")
    private String perId;
	
	    //账务流水号
    @Column(name = "ACC_JOURNAL_ID")
    private String accJournalId;
	
	    //订单号
    @Column(name = "ORDER_NO")
    private String orderNo;
	
	    //账单类型
    @Column(name = "BILL_TYPE")
    private String billType;
	
	    //来往标志  1：来账   2：往账
    @Column(name = "IN_OUT_FLAG")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "TRAN_AMT")
    private BigDecimal tranAmt;
	
	    //资金来源编号
    @Column(name = "INFLOW_ID")
    private String inflowId;
	
	    //资金来源类型    CUST001：个人客户    CUST002：企业客户   CUST003：第三方
    @Column(name = "INFLOW_TYPE")
    private String inflowType;
	
	    //资金去向编号
    @Column(name = "OUTFLOW_ID")
    private String outflowId;
	
	    //资金去向类型  CUST001：个人客户    CUST002：企业客户   CUST003：第三方
    @Column(name = "OUTFLOW_TYPE")
    private String outflowType;
	
	    //商品说明
    @Column(name = "PRODUCT_INFO")
    private String productInfo;
	
	    //付款方式     账户类型+银行卡
    @Column(name = "PAY_WAY")
    private String payWay;
	
	    //备注
    @Column(name = "REMARK")
    private String remark;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	

	/**
	 * 设置：账单编号
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}
	/**
	 * 获取：账单编号
	 */
	public String getBillId() {
		return billId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人信息编号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：账务流水号
	 */
	public void setAccJournalId(String accJournalId) {
		this.accJournalId = accJournalId;
	}
	/**
	 * 获取：账务流水号
	 */
	public String getAccJournalId() {
		return accJournalId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：账单类型
	 */
	public void setBillType(String billType) {
		this.billType = billType;
	}
	/**
	 * 获取：账单类型
	 */
	public String getBillType() {
		return billType;
	}
	/**
	 * 设置：来往标志  1：来账   2：往账
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	/**
	 * 获取：来往标志  1：来账   2：往账
	 */
	public String getInOutFlag() {
		return inOutFlag;
	}
	/**
	 * 设置：交易金额
	 */
	public void setTranAmt(BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getTranAmt() {
		return tranAmt;
	}
	/**
	 * 设置：资金来源编号
	 */
	public void setInflowId(String inflowId) {
		this.inflowId = inflowId;
	}
	/**
	 * 获取：资金来源编号
	 */
	public String getInflowId() {
		return inflowId;
	}
	/**
	 * 设置：资金来源类型    CON001：个人客户    CON002：企业客户
	 */
	public void setInflowType(String inflowType) {
		this.inflowType = inflowType;
	}
	/**
	 * 获取：资金来源类型    CON001：个人客户    CON002：企业客户
	 */
	public String getInflowType() {
		return inflowType;
	}
	/**
	 * 设置：资金去向编号
	 */
	public void setOutflowId(String outflowId) {
		this.outflowId = outflowId;
	}
	/**
	 * 获取：资金去向编号
	 */
	public String getOutflowId() {
		return outflowId;
	}
	/**
	 * 设置：资金去向类型  CON001：个人客户    CON002：企业客户   CON003：第三方
	 */
	public void setOutflowType(String outflowType) {
		this.outflowType = outflowType;
	}
	/**
	 * 获取：资金去向类型  CON001：个人客户    CON002：企业客户   CON003：第三方
	 */
	public String getOutflowType() {
		return outflowType;
	}
	/**
	 * 设置：商品说明
	 */
	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}
	/**
	 * 获取：商品说明
	 */
	public String getProductInfo() {
		return productInfo;
	}
	/**
	 * 设置：付款方式     账户类型+银行卡
	 */
	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}
	/**
	 * 获取：付款方式     账户类型+银行卡
	 */
	public String getPayWay() {
		return payWay;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
}
