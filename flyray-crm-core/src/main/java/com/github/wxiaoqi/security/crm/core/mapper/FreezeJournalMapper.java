package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.FreezeJournal;
import tk.mybatis.mapper.common.Mapper;

/**
 * 冻结流水表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-01 14:54:58
 */
@org.apache.ibatis.annotations.Mapper
public interface FreezeJournalMapper extends Mapper<FreezeJournal> {
	
}
