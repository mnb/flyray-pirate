package com.github.wxiaoqi.security.crm.core.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.biz.SmsGateWayBiz;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller
@RequestMapping("smsGateWay")
public class SmsGateWayAction extends BaseAction{
	@Autowired
	private SmsGateWayBiz smsGateWayBiz;
	
	/**
	 * 发送短信验证码
	 * @author centerroot
	 * @time 创建时间:2018年5月22日上午11:11:37
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/sendSMSCode",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> sendSMSCode(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("发送短信验证码  请求参数：{}",param);
		Map<String, Object> respMap = smsGateWayBiz.sendCode(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("发送短信验证码  响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 校验短信验证码
	 * @author centerroot
	 * @time 创建时间:2018年5月22日上午11:11:37
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/verifySMSCode",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> verifySMSCode(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("校验短信验证码  请求参数：{}",param);
		Map<String, Object> respMap = smsGateWayBiz.verificationCode(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("校验短信验证码 响应参数：{}",respMap);
		return respMap;
	}
	
}