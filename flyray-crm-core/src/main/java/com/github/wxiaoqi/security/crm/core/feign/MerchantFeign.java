package com.github.wxiaoqi.security.crm.core.feign;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.CustomerBiz;
import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseBiz;
import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseExtBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseExtBiz;
import com.github.wxiaoqi.security.crm.core.entity.Customer;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBaseExt;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantBaseExtMapper;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("feign/merchants")
public class MerchantFeign {
	@Autowired
	private CustomerBiz customerBiz;
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	@Autowired
	private PersonalAccountBiz personalAccountBiz;
	@Autowired
	private MerchantAccountBiz merchantAccountBiz;
	@Autowired
	private MerchantBaseExtBiz merchantBaseExtBiz;
	
	/**
	 * 平台开通商户账户
	 * @author centerroot
	 * @time 创建时间:2018年5月31日下午5:40:21
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/planformOpenAccount",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> planformOpenAccount(@RequestBody Map<String, Object> param){
		log.info("平台开通商户账户    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.planformOpenAccount(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("success", true);
		} else {
			respMap.put("success", false);
		}
		log.info("平台开通商户账户    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 判断商户是否存在 
	 */
	@RequestMapping(value = "/exist", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> exist(@RequestBody Map<String, Object> param) {
		log.info("判断商户是否存在 请求参数：{}", param);
		String merId = (String) param.get("merId");
		MerchantBase base = merchantBaseBiz.selectById(merId);
		Map<String, Object> result = new HashMap<String, Object>();
		if(base == null){
			result.put("code", ResponseCode.MER_NOTEXIST.getCode());
			result.put("msg", ResponseCode.MER_NOTEXIST.getMessage());
			result.put("success", false);
		}else {
			result.put("code", ResponseCode.OK.getCode());
			result.put("msg", ResponseCode.OK.getMessage());
			result.put("success", true);
		}
		log.info("判断商户是否存在 响应参数:{}", result);
		return result;
	}
	
	/**
	 * 查询商户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年6月4日下午3:24:56
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> info(@RequestBody Map<String, Object> param) {
		Map<String, Object> result = new HashMap<String, Object>();
		String merId = (String) param.get("merId");
		MerchantBase base = merchantBaseBiz.selectById(merId);
		if(base == null){
			result.put("code", ResponseCode.MER_NOTEXIST.getCode());
			result.put("msg", ResponseCode.MER_NOTEXIST.getMessage());
			result.put("success", false);
			return result;
		}else{
			Map<String, Object> baseMap = EntityUtils.beanToMap(base);
			result.put("merchantBase",baseMap);
		}
		
		Customer customerReq = new Customer();
		customerReq.setMerId(merId);
		Customer customerResp = customerBiz.selectOne(customerReq);
		if (null != customerResp) {
			result.put("mobile",customerResp.getMobile());
		} else {
			result.put("mobile","");
		}
				
		MerchantBaseExt merchantBaseExtReq = new MerchantBaseExt();
		merchantBaseExtReq.setMerId((String) param.get("merId"));
		MerchantBaseExt merchantBaseExt = merchantBaseExtBiz.selectOne(merchantBaseExtReq);
		if (null != merchantBaseExt && null != merchantBaseExt.getInterview()) {
			log.info("根据商户编号查询商户基础信息扩展表记录结果：{}",merchantBaseExt.toString());
			result.put("interview", String.valueOf(merchantBaseExt.getInterview()));
		} else {
			result.put("interview", "0");
		}
		
		
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		result.put("success", true);
		return result;
	}
	
	/**
	 * 线下充值（充值余额、充值邀请面试次数）
	 * @author centerroot
	 * @time 创建时间:2018年6月22日下午2:38:43
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/underlineRecharge",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> underlineRecharge(@RequestBody Map<String, Object> param){
		log.info("线下充值（充值余额、充值邀请面试次数）    请求参数：{}",param);
		Map<String, Object> respMap = merchantBaseBiz.underlineRecharge(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("success", true);
		} else {
			respMap.put("success", false);
		}
		log.info("线下充值（充值余额、充值邀请面试次数）    响应参数：{}",respMap);
		return respMap;
	}
	

}
