package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBilling;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBillingMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人账单
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class PersonalBillingBiz extends BaseBiz<PersonalBillingMapper,PersonalBilling> {
}