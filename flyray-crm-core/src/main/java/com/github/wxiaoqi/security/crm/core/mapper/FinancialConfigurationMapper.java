package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.FinancialConfiguration;

import tk.mybatis.mapper.common.Mapper;

/**
 * 财务配置
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-20 14:19:07
 */
public interface FinancialConfigurationMapper extends Mapper<FinancialConfiguration> {
	
	
	void addConfiguration(FinancialConfiguration configuration);
	
	void updateConfiguration(FinancialConfiguration configuration);
	
	List<FinancialConfiguration> queryConfiguration(Map<String, Object> map);
}
