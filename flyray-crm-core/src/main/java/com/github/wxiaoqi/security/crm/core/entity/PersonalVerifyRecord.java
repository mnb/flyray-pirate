package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 个人实名认证记录
 * @author chj
 *
 */
public class PersonalVerifyRecord implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//个人信息编号
	private String perId;
	
	//姓名
	private String realName;
	
	//身份证号
	private String idNumber;
	
	//实名状态
	private String verifyFlag;
	
	//创建时间
	private Timestamp createTime;
	
	//身份证正面
	private String idPositive;
	
	//身份证反面
	private String idNegative;

	public String getPerId() {
		return perId;
	}

	public void setPerId(String perId) {
		this.perId = perId;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getVerifyFlag() {
		return verifyFlag;
	}

	public void setVerifyFlag(String verifyFlag) {
		this.verifyFlag = verifyFlag;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getIdPositive() {
		return idPositive;
	}

	public void setIdPositive(String idPositive) {
		this.idPositive = idPositive;
	}

	public String getIdNegative() {
		return idNegative;
	}

	public void setIdNegative(String idNegative) {
		this.idNegative = idNegative;
	}

	@Override
	public String toString() {
		return "PersonalVerifyRecord [perId=" + perId + ", realName=" + realName + ", idNumber=" + idNumber
				+ ", verifyFlag=" + verifyFlag + ", createTime=" + createTime + ", idPositive=" + idPositive
				+ ", idNegative=" + idNegative + "]";
	}
	
	
}
