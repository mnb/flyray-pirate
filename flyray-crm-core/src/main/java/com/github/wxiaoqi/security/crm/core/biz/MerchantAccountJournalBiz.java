package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.UnderLineRecharge;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountJournalMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;

/**
 * 企业账户流水（充、转、提、退、冻结流水）
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class MerchantAccountJournalBiz extends BaseBiz<MerchantAccountJournalMapper,MerchantAccountJournal> {
	
	
	@Autowired
	private MerchantAccountJournalMapper accountJournalMapper;
	
	
	/**
	 * 查询账户流水
	 * @param map
	 * @return
	 */
	public List<MerchantAccountJournal> queryAccountJournal(Map<String, Object> map) {
		log.info("查询账户流水参数。。。{}"+map);
		List<MerchantAccountJournal> list = accountJournalMapper.queryMerchantJournal(map);
		log.info("查询账户流水。。。{}"+list);
		return list;
	}
	
	
	/**
	 * 线下充值记录
	 * @author chj
	 * @param map
	 * @return
	 */
	public List<UnderLineRecharge> queryUnderLineRecharge(Map<String, Object> map) {
		log.info("线下充值记录参数。。。{}"+map);
		List<UnderLineRecharge> list = accountJournalMapper.queryUnderlinerecharge(map);
		log.info("线下充值记录。。。{}"+list);
		return list;
	}
}