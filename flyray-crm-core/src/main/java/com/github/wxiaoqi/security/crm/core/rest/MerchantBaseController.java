package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 商户基础信息
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("admin/merchants/base")
public class MerchantBaseController extends BaseController<MerchantBaseBiz,MerchantBase> {

	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	
	/**
	 * 查询商户实名记录
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryRealNameRecord",method = RequestMethod.GET)
	public Map<String, Object> queryRealNameRecord() {
		log.info("查询商户实名记录，请求参数。。。{}");
		Map<String, Object> res = merchantBaseBiz.queryRealNameRecord();
		log.info("查询商户实名记录，响应参数。。。{}"+res);
		return res;
	}
	
	
	/**
	 * 修改商户实名状态
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateVerifyFlag",method = RequestMethod.POST)
	public Map<String, Object> updateVerifyFlag(Map<String, Object> map) {
		log.info("修改商户实名状态，请求参数。。。{}"+map);
		Map<String, Object> res = merchantBaseBiz.updateVerifyFlag(map);
		
		return res;
	}
	
}