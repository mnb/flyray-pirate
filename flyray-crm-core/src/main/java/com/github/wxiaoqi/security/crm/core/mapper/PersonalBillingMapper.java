package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBilling;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账单
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBillingMapper extends Mapper<PersonalBilling> {
	
}
