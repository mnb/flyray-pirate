package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.PlatformAccoutConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformAccoutConfigMapper;
import com.github.wxiaoqi.security.crm.core.mapper.UnfreezeJournalMapper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.MD5;

import lombok.extern.slf4j.Slf4j;

/**
 * 平台账户配置表
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class PlatformAccoutConfigBiz extends BaseBiz<PlatformAccoutConfigMapper,PlatformAccoutConfig> {

	@Autowired
	private PlatformAccoutConfigMapper platformAccoutConfigMapper;
	/**
	 * 判断平台账户类型是否存在
	 * @author centerroot
	 * @time 创建时间:2018年6月6日下午2:37:20
	 * @param param
	 * @return
	 */
	public Map<String, Object> isExist(Map<String, Object> param){
		log.info("判断平台账户类型是否存在   请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = (String) param.get("platformId");
		String accType = (String) param.get("accType");
		String conType = (String) param.get("conType");
		
		PlatformAccoutConfig platformAccoutConfigReq = new PlatformAccoutConfig();
		platformAccoutConfigReq.setAccType(accType);
		platformAccoutConfigReq.setConType(conType);
		platformAccoutConfigReq.setPlatformId(platformId);
		PlatformAccoutConfig platformAccoutConfigResp = platformAccoutConfigMapper.selectOne(platformAccoutConfigReq);
		
		if (null != platformAccoutConfigResp) {
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			respMap.put("code", ResponseCode.PLAT_ACC_TYPE_NOT_EXIST.getCode());
			respMap.put("msg", ResponseCode.PLAT_ACC_TYPE_NOT_EXIST.getMessage());
		}
		
		log.info("判断平台账户类型是否存在   响应参数：{}",respMap);
		return respMap;
	}
	
}