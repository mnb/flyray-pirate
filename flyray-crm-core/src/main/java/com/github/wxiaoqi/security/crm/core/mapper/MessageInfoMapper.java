package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.CustomerMessage;
import com.github.wxiaoqi.security.crm.core.entity.MessageInfo;
import tk.mybatis.mapper.common.Mapper;

/**
 * 消息表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@org.apache.ibatis.annotations.Mapper
public interface MessageInfoMapper extends Mapper<MessageInfo> {
	/**
	 * 查询用户消息
	 * @author centerroot
	 * @time 创建时间:2018年5月23日下午4:49:28
	 * @param param
	 * @return
	 */
	List<CustomerMessage> queryCustomerMessage(Map<String, Object> param);
	
}
