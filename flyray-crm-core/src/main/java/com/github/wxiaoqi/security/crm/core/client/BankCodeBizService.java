package com.github.wxiaoqi.security.crm.core.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.wxiaoqi.security.common.admin.pay.response.QueryBankCodeResponse;

@FeignClient(value = "flyray-pay-core")
public interface BankCodeBizService {

	@RequestMapping(value = "rest/payForAnother/queryBankCode",method = RequestMethod.POST)
	public QueryBankCodeResponse queryBankCode();
}
