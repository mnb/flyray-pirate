package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 注册客户（会员）信息表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "customer")
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //用户编号
    @Id
    private String customerId;
	
	    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
	    //用户类型  00:平台（渠道）  01：普通客户
	@Column(name = "CUSTOMER_TYPE")
	private String customerType;
		
	    //手机号
    @Column(name = "MOBILE")
    private String mobile;
	
	    //登录密码
    @Column(name = "PASSWORD")
    private String password;
	
	    //支付密码
    @Column(name = "PAY_PASSWORD")
    private String payPassword;
	
	    //密码状态 00：正常，01：锁定
    @Column(name = "PASSWORD_STATUS")
    private String passwordStatus;
	
	    //支付密码状态  00：正常   01：未设置   02：锁定
    @Column(name = "PAY_PASSWORD_STATUS")
    private String payPasswordStatus;
	
	    //用户会员编号
    @Column(name = "PER_ID")
    private String perId;
	
	    //商户编号
    @Column(name = "MER_ID")
    private String merId;
	
	    //盐值
    @Column(name = "SALTVALUE")
    private String saltvalue;
	
	    //账户状态 00：正常，01：客户冻结
    @Column(name = "ACCOUNT_STATUS")
    private String accountStatus;
	
	    //最后登录角色 默认null  01 表示 猎手 02 表示悬赏方
	@Column(name = "LAST_LOGIN_ROLE")
	private String lastLoginRole;

	    //最后登录时间
    @Column(name = "LAST_LOGIN_TIME")
    private Timestamp lastLoginTime;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * 获取：手机号
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * 设置：登录密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 获取：登录密码
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 设置：支付密码
	 */
	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}
	/**
	 * 获取：支付密码
	 */
	public String getPayPassword() {
		return payPassword;
	}
	/**
	 * 设置：密码状态 00：正常，01：锁定
	 */
	public void setPasswordStatus(String passwordStatus) {
		this.passwordStatus = passwordStatus;
	}
	/**
	 * 获取：密码状态 00：正常，01：锁定
	 */
	public String getPasswordStatus() {
		return passwordStatus;
	}
	/**
	 * 设置：支付密码状态  00：正常   01：未设置   02：锁定
	 */
	public void setPayPasswordStatus(String payPasswordStatus) {
		this.payPasswordStatus = payPasswordStatus;
	}
	/**
	 * 获取：支付密码状态  00：正常   01：未设置   02：锁定
	 */
	public String getPayPasswordStatus() {
		return payPasswordStatus;
	}
	/**
	 * 设置：用户会员编号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户会员编号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：盐值
	 */
	public void setSaltvalue(String saltvalue) {
		this.saltvalue = saltvalue;
	}
	/**
	 * 获取：盐值
	 */
	public String getSaltvalue() {
		return saltvalue;
	}
	/**
	 * 设置：账户状态 00：正常，01：客户冻结
	 */
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	/**
	 * 获取：账户状态 00：正常，01：客户冻结
	 */
	public String getAccountStatus() {
		return accountStatus;
	}
	
	
	public String getLastLoginRole() {
		return lastLoginRole;
	}
	public void setLastLoginRole(String lastLoginRole) {
		this.lastLoginRole = lastLoginRole;
	}
	/**
	 * 设置：最后登录时间
	 */
	public void setLastLoginTime(Timestamp lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	/**
	 * 获取：最后登录时间
	 */
	public Timestamp getLastLoginTime() {
		return lastLoginTime;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", platformId=" + platformId + ", customerType=" + customerType
				+ ", mobile=" + mobile + ", password=" + password + ", payPassword=" + payPassword + ", passwordStatus="
				+ passwordStatus + ", payPasswordStatus=" + payPasswordStatus + ", perId=" + perId + ", merId=" + merId
				+ ", saltvalue=" + saltvalue + ", accountStatus=" + accountStatus + ", lastLoginRole=" + lastLoginRole
				+ ", lastLoginTime=" + lastLoginTime + ", createtime=" + createtime + ", updatetime=" + updatetime
				+ "]";
	}
	
}
