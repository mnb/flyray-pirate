package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournalExt;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountJournalExtMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 企业账户流水（充、转、提、退、冻结流水）
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class MerchantAccountJournalExtBiz extends BaseBiz<MerchantAccountJournalExtMapper,MerchantAccountJournalExt> {
}