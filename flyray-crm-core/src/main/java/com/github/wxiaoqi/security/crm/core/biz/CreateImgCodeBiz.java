package com.github.wxiaoqi.security.crm.core.biz;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.MobImgCodeUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class CreateImgCodeBiz {
    @Autowired
    private RedisTemplate redisTemplate;
	@Value("${bountyHunter.imgCodeTime}")
	private int imgCodeTime;
	
	/**
	 * 生成图形验证码
	 * 不在服务器存储
	 * @author centerroot
	 * @time 创建时间:2018年5月22日上午10:53:31
	 * @return
	 */
	public Map<String, Object> createImgCode(){

		log.info("【生成图形验证码】--开始");
		Map<String, Object> respMap = new HashMap<String, Object>();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Map<String, Object> map = MobImgCodeUtil.getMobImgCode();
		BufferedImage image = (BufferedImage) map.get("image");
		String strEnsure = (String) map.get("strEnsure");
		try {
			// 输出图像到页面
			ImageIO.write(image, "JPEG", os);
			byte[] imageByte = os.toByteArray();
			
			respMap.put("imgCodeImg", imageByte);
			respMap.put("imgCode", strEnsure);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
			os.close();
		} catch (IOException e) {
			log.info("【生成图形验证码】获取验证码失败");
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		log.info("【生成图形验证码】--结束：" + respMap);
		return respMap;
	}
	
	/**
	 * 生成图形验证码（手机号）
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午3:31:45
	 * @param param
	 * @return
	 */
	public Map<String, Object> createMobImgCode(Map<String, Object> param){

		log.info("【生成图形验证码】--开始：" + param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Map<String, Object> map = MobImgCodeUtil.getMobImgCode();
		BufferedImage image = (BufferedImage) map.get("image");
		String strEnsure = (String) map.get("strEnsure");
		try {
			// 输出图像到页面
			ImageIO.write(image, "JPEG", os);
			byte[] imageByte = os.toByteArray();
			
			respMap.put("imgCodeImg", imageByte);
			respMap.put("imgCode", strEnsure);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
			os.close();
		} catch (IOException e) {
			log.info("【生成图形验证码】获取验证码失败");
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		// 将生成的验证码放进redis中，留待登录验证
		Map<String, String> mapData = new HashMap<String, String>();
		mapData.put("strEnsure", strEnsure);
		redisTemplate.opsForValue().set("IMG_CODE_KEY:" + param.get("mobile"), mapData, imgCodeTime,TimeUnit.SECONDS);

		log.info("【生成图形验证码】--结束：" + respMap);
		return respMap;
	}
	
	/**
	 * 获取图形验证码（手机号）
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午4:12:52
	 * @param param
	 * @return
	 */
	public Map<String, Object> getMobImgCode(Map<String, Object> param){

		log.info("【获取图形验证码】--开始：" + param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String imgCode=String.valueOf(redisTemplate.opsForValue().get("IMG_CODE_KEY:" + param.get("mobile")));
		
		respMap.put("imgCode", imgCode);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【获取图形验证码】--结束：" + respMap);
		return respMap;
	}
	
	/**
	 * 生成图形验证码（注册）
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午3:31:45
	 * @param param
	 * @return
	 */
	public Map<String, Object> createRegisterImgCode(){

		log.info("【注册时生成图形验证码】--开始");
		Map<String, Object> respMap = new HashMap<String, Object>();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Map<String, Object> map = MobImgCodeUtil.getMobImgCode();
		BufferedImage image = (BufferedImage) map.get("image");
		String strEnsure = (String) map.get("strEnsure");
		try {
			// 输出图像到页面
			ImageIO.write(image, "JPEG", os);
			byte[] imageByte = os.toByteArray();
			
			respMap.put("imgCodeImg", imageByte);
			respMap.put("imgCode", strEnsure);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
			os.close();
		} catch (IOException e) {
			log.info("【注册时生成图形验证码】获取验证码失败");
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		// 将生成的验证码放进redis中，留待登录验证
		Map<String, String> mapData = new HashMap<String, String>();
		mapData.put("strEnsure", strEnsure);
		redisTemplate.opsForValue().set("REG_IMG_CODE_KEY:" + strEnsure, mapData, imgCodeTime,TimeUnit.SECONDS);

		log.info("【注册时生成图形验证码】--结束：" + respMap);
		return respMap;
	}
	
	/**
	 * 获取图形验证码（注册）
	 * 验证成功返回true，验证失败返回false
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午4:12:52
	 * @param param
	 * @return
	 */
	public Map<String, Object> getRegisterImgCode(Map<String, Object> param){

		log.info("【获取图形验证码】--开始：" + param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String imgCodeParam = (String) param.get("imgCode");
		String imgCode=String.valueOf(redisTemplate.opsForValue().get("REG_IMG_CODE_KEY:" + imgCodeParam));
		if (null != imgCodeParam && imgCodeParam.equals(imgCode)) {
			respMap.put("regImgCodeStatus", true);
		}else{
			respMap.put("regImgCodeStatus", false);
		}
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【获取图形验证码】--结束：" + respMap);
		return respMap;
	}
}
