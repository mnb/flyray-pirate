package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.MerchantBaseExt;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantBaseExtMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 商户基础信息扩展表
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class MerchantBaseExtBiz extends BaseBiz<MerchantBaseExtMapper,MerchantBaseExt> {
}