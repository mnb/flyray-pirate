package com.github.wxiaoqi.security.crm.core.feign;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.CustomerBiz;
import com.github.wxiaoqi.security.crm.core.entity.Customer;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.SHA256Utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("feign/customers")
public class CustomerFeign {
	@Autowired
	private CustomerBiz customerBiz;
	
	/**
	 * 查询用户信息
	 * @author centerroot
	 * @time 创建时间:2018年6月22日下午2:38:43
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/queryCustomerInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryCustomerInfo(@RequestBody Map<String, Object> param){
		log.info("查询用户信息    请求参数：{}",param);
		Map<String, Object> respMap = customerBiz.queryCustomerInfo(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("success", true);
		} else {
			respMap.put("success", false);
		}
		log.info("查询用户信息    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 判断用户交易密码是否正确
	 */
	@RequestMapping(value = "/verifyPayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> verifyPayPassword(@RequestBody Map<String, Object> reqs) {
		Map<String, Object> result = new HashMap<String, Object>();
		log.info("判断用户交易密码是否正确     请求参数：{}", reqs);
		String customerId = (String) reqs.get("customerId");
		String perId = (String) reqs.get("perId");
		String merId = (String) reqs.get("merId");
		String payPassword = (String) reqs.get("payPassword");
		Customer customerReq = new Customer();
		customerReq.setCustomerId(customerId);
		customerReq.setPerId(perId);
		customerReq.setMerId(merId);
		Customer customerResp = customerBiz.selectOne(customerReq);
		String payPWD = SHA256Utils.SHA256(payPassword+customerResp.getCustomerId());
		
		if (payPWD.equals(customerResp.getPayPassword())) {
			result.put("code", ResponseCode.OK.getCode());
			result.put("msg", ResponseCode.OK.getMessage());
			result.put("sucess", true);
		} else {
			result.put("code", ResponseCode.PAY_PWD_ERROR.getCode());
			result.put("msg", ResponseCode.PAY_PWD_ERROR.getMessage());
			result.put("sucess", false);
		}
		
		log.info("判断用户交易密码是否正确    响应参数：{}", result);
		return result;
	}
	
}
