package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBillingBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBilling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("personals/billing")
public class PersonalBillingController extends BaseController<PersonalBillingBiz,PersonalBilling> {

}