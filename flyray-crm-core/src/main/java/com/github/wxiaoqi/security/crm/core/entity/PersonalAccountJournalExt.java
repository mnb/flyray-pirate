package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 个人账户流水扩展表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "personal_account_journal_ext")
public class PersonalAccountJournalExt implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //任务编号
    @Column(name = "TASK_ID")
    private String taskId;
	
	    //简历编号
    @Column(name = "RESUME_ID")
    private String resumeId;
	
	    //悬赏金
    @Column(name = "BOUNTT")
    private BigDecimal bountt;
	
	    //管理费
    @Column(name = "MANAGE_FEE")
    private BigDecimal manageFee;
	
	    //服务费
    @Column(name = "SERVICE_FEE")
    private BigDecimal serviceFee;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：任务编号
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	 * 获取：任务编号
	 */
	public String getTaskId() {
		return taskId;
	}
	/**
	 * 设置：简历编号
	 */
	public void setResumeId(String resumeId) {
		this.resumeId = resumeId;
	}
	/**
	 * 获取：简历编号
	 */
	public String getResumeId() {
		return resumeId;
	}
	/**
	 * 设置：悬赏金
	 */
	public void setBountt(BigDecimal bountt) {
		this.bountt = bountt;
	}
	/**
	 * 获取：悬赏金
	 */
	public BigDecimal getBountt() {
		return bountt;
	}
	/**
	 * 设置：管理费
	 */
	public void setManageFee(BigDecimal manageFee) {
		this.manageFee = manageFee;
	}
	/**
	 * 获取：管理费
	 */
	public BigDecimal getManageFee() {
		return manageFee;
	}
	/**
	 * 设置：服务费
	 */
	public void setServiceFee(BigDecimal serviceFee) {
		this.serviceFee = serviceFee;
	}
	/**
	 * 获取：服务费
	 */
	public BigDecimal getServiceFee() {
		return serviceFee;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
}
