package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.FreezeJournalBiz;
import com.github.wxiaoqi.security.crm.core.entity.FreezeJournal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("freezeJournal")
public class FreezeJournalController extends BaseController<FreezeJournalBiz,FreezeJournal> {

}