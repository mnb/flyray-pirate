package com.github.wxiaoqi.security.crm.core.api;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.common.util.EntityUtils;


/**
 * 
 * */
public class BaseAction {

	/**
	 * 读取字节流参数转换成bean
	 * @throws IllegalAccessException 
	 * @throws Exception 
	 *
	 * */
	/*public <T> T ParamStr2Bean(HttpServletRequest request,Class<T> class1) throws Exception{
		String jsonStr =EntityUtils.getBodyData(request);
		Map<String, Object>map=(Map<String, Object>) JSONObject.parse(jsonStr);
		return EntityUtils.map2Bean(map, class1);
	}*/
	public Map<String, Object> ParamStr2Bean(HttpServletRequest request) throws Exception{
		String params=String.valueOf(request.getAttribute("param"));
		Map<String, Object>map=(Map<String, Object>) JSONObject.parse(params);
		return map;
	}
	/**
	 * 读取字节流参数转换成bean
	 * @throws IllegalAccessException 
	 * @throws Exception 
	 *
	 * */
	public <T> T ParamToBean(HttpServletRequest request,Class<T> class1) throws Exception{
		String params=String.valueOf(request.getAttribute("param"));
		Map<String, Object>map=(Map<String, Object>) JSONObject.parse(params);
		return EntityUtils.map2Bean(map, class1);
	}
	
}
