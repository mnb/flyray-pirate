package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournalInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalAccountJournalMapper extends Mapper<PersonalAccountJournal> {
	/**
	 * 查询用户账户记录
	 * @author centerroot
	 * @time 创建时间:2018年5月24日上午10:29:43
	 * @param param
	 * @return
	 */
	List<PersonalAccountJournalInfo> queryTransactionRecord(Map<String, Object> param);
	
	
	
	/**
	 * 查询流水记录
	 * @author chj
	 * @param map
	 * @return
	 */
	List<PersonalAccountJournal> queryAccountJournal(Map<String, Object> map);
}
