package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.CustomerMessageRel;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerMessageRelMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 16:17:27
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CustomerMessageRelBiz extends BaseBiz<CustomerMessageRelMapper,CustomerMessageRel> {
}