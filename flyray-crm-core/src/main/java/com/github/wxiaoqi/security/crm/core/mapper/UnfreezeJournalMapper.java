package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.UnfreezeJournal;

import tk.mybatis.mapper.common.Mapper;

/**
 * 解冻流水表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-01 14:54:58
 */
@org.apache.ibatis.annotations.Mapper
public interface UnfreezeJournalMapper extends Mapper<UnfreezeJournal> {
	
}
