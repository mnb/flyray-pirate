package com.github.wxiaoqi.security.crm.core.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountJournalBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournal;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;


/**
 * 个人账户流水
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("admin/personals/accountJournal")
public class PersonalAccountJournalController extends BaseController<PersonalAccountJournalBiz,PersonalAccountJournal> {

	@Autowired
	private PersonalAccountJournalBiz accountJournalBiz;
	
	/**
	 * 查询个人账户流水
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryAccountJournal", method = RequestMethod.GET)
	public TableResultResponse<PersonalAccountJournal> queryAccountJournal(@RequestParam(value = "orderNo") String orderNo, 
			@RequestParam(value = "tradeType") String tradeType, 
			@RequestParam(value = "perNo") String perNo,
			@RequestParam(value = "mobile") String mobile, 
			@RequestParam(value = "startDate") String startDate,
			@RequestParam(value = "endDate") String endDate, 
			@RequestParam(value = "page", defaultValue = "1") Integer page, 
			@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
		Map<String, Object> map = new HashMap<>();
		map.put("orderNo", orderNo);
		map.put("tradeType", tradeType);
		map.put("perNo", perNo);
		map.put("mobile", mobile);
		map.put("page", page);
		map.put("limit", limit);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		log.info("查询个人账户流水，请求参数。。。{}"+map);
		Query query = new Query(map);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<PersonalAccountJournal> list = accountJournalBiz.queryAccountJournalList(map);
		log.info("查询个人账户流水，响应参数。。。{}"+list);
		return new TableResultResponse<PersonalAccountJournal>(result.getTotal(), list);
	}
	
	
}