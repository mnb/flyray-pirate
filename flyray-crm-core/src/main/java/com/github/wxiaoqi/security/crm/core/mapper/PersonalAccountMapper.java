package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;

import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账户
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalAccountMapper extends Mapper<PersonalAccount> {
	/**
	 * 查询用户账户信息
	 * @author centerroot
	 * @time 创建时间:2018年5月28日下午4:30:48
	 * @param param
	 * @return
	 */
	List<PersonalAccount> queryPersonalAccount(Map<String, Object> param);
	
	/**
	 * 查询猎手账户
	 * @time 创建时间:2018年6月1日上午9:42:20
	 * @param param
	 * @return
	 */
	List<PersonalAccount> queryPersonalAccountInfo(Map<String, Object> param);
	
	/**
	 * 修改冻结状态
	 * @param map
	 */
	void updateAccountStatus(Map<String, Object> map);
	
}
