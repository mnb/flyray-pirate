package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.UnfreezeJournalBiz;
import com.github.wxiaoqi.security.crm.core.entity.UnfreezeJournal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin/unfreezeJournal")
public class UnfreezeJournalController extends BaseController<UnfreezeJournalBiz,UnfreezeJournal> {

}