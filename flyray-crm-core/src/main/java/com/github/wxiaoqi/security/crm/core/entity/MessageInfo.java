package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 消息表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@Table(name = "message_info")
public class MessageInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //消息编号
    @Id
    private String msgId;
	
	    //平台编号
	@Column(name = "PLATFORM_ID")
	private String platformId;
	
	//消息类型
	@Column(name = "MSG_TYPE")
	private String msgType;
	
	    //消息标题
    @Column(name = "MSG_TITLE")
    private String msgTitle;
	
	    //消息内容
    @Column(name = "MSG_CONTENT")
    private String msgContent;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	

	/**
	 * 设置：消息编号
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	/**
	 * 获取：消息编号
	 */
	public String getMsgId() {
		return msgId;
	}
	
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 设置：消息类型
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	/**
	 * 获取：消息类型
	 */
	public String getMsgType() {
		return msgType;
	}
	/**
	 * 设置：消息标题
	 */
	public void setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
	}
	/**
	 * 获取：消息标题
	 */
	public String getMsgTitle() {
		return msgTitle;
	}
	/**
	 * 设置：消息内容
	 */
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	/**
	 * 获取：消息内容
	 */
	public String getMsgContent() {
		return msgContent;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
}
