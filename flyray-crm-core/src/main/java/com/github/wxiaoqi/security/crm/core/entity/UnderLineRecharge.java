package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 线下充值记录
 * @author Administrator
 *
 */
public class UnderLineRecharge implements Serializable {

	private static final long serialVersionUID = 1L;

	//创建时间
	private Timestamp createtime;
	
	//手机号
	private String mobile;
	
	//充值类型  01余额；02次数
	private String rechargeType;
	
	//充值数量
	private String amt;
	
	//操作人id
	private String operatorId;
	
	//操作人
	private String operator;

	public Timestamp getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	@Override
	public String toString() {
		return "UnderLineRecharge [createtime=" + createtime + ", mobile=" + mobile + ", rechargeType=" + rechargeType
				+ ", amt=" + amt + ", operatorId=" + operatorId + ", operator=" + operator + "]";
	}
	
}
