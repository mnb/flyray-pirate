package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 企业账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "merchant_account_journal")
public class MerchantAccountJournal implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //账户编号
    @Column(name = "ACC_ID")
    private String accId;
	
	    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
	
	    //订单号
    @Column(name = "ORDER_NO")
    private String orderNo;
	
	    //商户编号
    @Column(name = "MER_ID")
    private String merId;
	
	
	    //账户类型    ACC001：余额账户
    @Column(name = "ACC_TYPE")
    private String accType;
	
	    //来往标志  1：来账   2：往账 
    @Column(name = "IN_OUT_FLAG")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "TRADE_AMT")
    private BigDecimal tradeAmt;
	
	    //交易类型  01：充值，02：提现，03：简历补贴，04：悬赏金 05：任务冻结 06：任务解冻
    @Column(name = "TRADE_TYPE")
    private String tradeType;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	
    
    //商户号
    @Transient
    private String merchantNo;
    
    //手机号
    @Transient
    private String mobile;

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：账户编号
	 */
	public void setAccId(String accId) {
		this.accId = accId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccId() {
		return accId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：账户类型    ACC001：余额账户
	 */
	public void setAccType(String accType) {
		this.accType = accType;
	}
	/**
	 * 获取：账户类型    ACC001：余额账户
	 */
	public String getAccType() {
		return accType;
	}
	/**
	 * 设置：来往标志  1：来账   2：往账 
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	/**
	 * 获取：来往标志  1：来账   2：往账 
	 */
	public String getInOutFlag() {
		return inOutFlag;
	}
	/**
	 * 设置：交易金额
	 */
	public void setTradeAmt(BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getTradeAmt() {
		return tradeAmt;
	}
	/**
	 * 设置：交易类型  01：充值，02：提现，03：简历补贴，04：悬赏金 05：任务冻结 06：任务解冻
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型  01：充值，02：提现，03：简历补贴，04：悬赏金 05：任务冻结 06：任务解冻
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	public String getMerchantNo() {
		return merchantNo;
	}
	public void setMerchantNo(String merchantNo) {
		this.merchantNo = merchantNo;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	@Override
	public String toString() {
		return "MerchantAccountJournal [journalId=" + journalId + ", accId=" + accId + ", platformId=" + platformId
				+ ", orderNo=" + orderNo + ", merId=" + merId + ", accType=" + accType
				+ ", inOutFlag=" + inOutFlag + ", tradeAmt=" + tradeAmt + ", tradeType=" + tradeType + ", createtime="
				+ createtime + ", updatetime=" + updatetime + ", merchantNo=" + merchantNo + ", mobile=" + mobile + "]";
	}
	
}
