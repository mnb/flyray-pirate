package com.github.wxiaoqi.security.crm.core.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountInfo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

/**
 * 商户账户
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("admin/merchants/account")
public class MerchantAccountController extends BaseController<MerchantAccountBiz,MerchantAccount> {

	@Autowired
	private MerchantAccountBiz merchantAccountBiz;
	
	
	/**
	 * 查询商户账户
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryMerchantAccount", method = RequestMethod.GET)
	public TableResultResponse<MerchantAccountInfo> queryMerchantAccount(@RequestParam(value = "accountStatus") String accountStatus, 
			@RequestParam(value = "merchantNo") String merchantNo, 
			@RequestParam(value = "mobile") String mobile,
			@RequestParam(value = "page", defaultValue = "1") Integer page, 
			@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
		Map<String, Object> param = new HashMap<>();
		param.put("accountStatus", accountStatus);
		param.put("merchantNo", merchantNo);
		param.put("mobile", mobile);
		param.put("page", page);
		param.put("limit", limit);
		log.info("查询商户账户，请求参数。。。{}"+param);
		Query query = new Query(param);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<MerchantAccountInfo> list = merchantAccountBiz.queryMerchantAccount(param);
		log.info("查询商户账户，响应参数。。。{}"+list);
		return new TableResultResponse<MerchantAccountInfo>(result.getTotal(), list);
	}
	
	
	
	/**
	 * 更新状态
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateAccountStatus", method = RequestMethod.POST)
	public Map<String, Object> updateAccountStatus(@RequestBody Map<String, Object> param) {
		log.info("修改账户状态，请求参数。。。{}"+param);
		Map<String, Object> resp = merchantAccountBiz.updateMerchantAccount(param);
		log.info("修改账户状态，响应参数。。。{}"+resp);
		return resp;
	}
	
	
	
 
}