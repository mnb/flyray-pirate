package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 商户基础信息
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "merchant_base")
public class MerchantBase implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //商户编号
    @Id
    private String merId;
	
	    //父级商户编号
    @Column(name = "PARENT_MER_ID")
    private String parentMerId;
	
	    //用户编号
	@Column(name = "CUSTOMER_ID")
	private String customerId;
	    //
    @Column(name = "MERCHANT_NO")
    private String merchantNo;
	
	    //商户类型  00：普通商户  01：子商户
    @Column(name = "MER_TYPE")
    private String merType;
	
	    //企业名称
    @Column(name = "CUST_NAME")
    private String custName;
	
	    //工商注册号
    @Column(name = "BUSINESS_NO")
    private String businessNo;
	
	    //法人姓名
    @Column(name = "LEGAL_PERSON_NAME")
    private String legalPersonName;
	
	    //法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
    @Column(name = "LEGAL_PERSON_CRED_TYPE")
    private String legalPersonCredType;
	
	    //法人证件号码
    @Column(name = "LEGAL_PERSON_CRED_NO")
    private String legalPersonCredNo;
	
	    //经营范围
    @Column(name = "BUSINESS_SCOPE")
    private String businessScope;
    
    //营业执照照片
	@Column(name = "BUSINESS_LICENCE")
	private String businessLicence;
	    //企业地址
    @Column(name = "ADDRESS")
    private String address;
	
	    //企业网址
    @Column(name = "HTTP_ADDRESS")
    private String httpAddress;
	
	    //企业注册资金
    @Column(name = "REGISTER_AMOUNT")
    private BigDecimal registerAmount;
	
	    //联系电话
    @Column(name = "TELEPHONE")
    private String telephone;
	
	    //实名状态  00：未实名  01：已实名  02：待审核
    @Column(name = "VERIFY_FLAG")
    private String verifyFlag;
    
	    //账户状态 00：正常，01：冻结
	@Column(name = "ACCOUNT_STATUS")
	private String accountStatus;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：商户编号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：父级商户编号
	 */
	public void setParentMerId(String parentMerId) {
		this.parentMerId = parentMerId;
	}
	/**
	 * 获取：父级商户编号
	 */
	public String getParentMerId() {
		return parentMerId;
	}
	/**
	 * 设置：
	 */
	public void setMerchantNo(String merchantNo) {
		this.merchantNo = merchantNo;
	}
	/**
	 * 获取：
	 */
	public String getMerchantNo() {
		return merchantNo;
	}
	/**
	 * 设置：商户类型  00：普通商户  01：子商户
	 */
	public void setMerType(String merType) {
		this.merType = merType;
	}
	/**
	 * 获取：商户类型  00：普通商户  01：子商户
	 */
	public String getMerType() {
		return merType;
	}
	/**
	 * 设置：企业名称
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	/**
	 * 获取：企业名称
	 */
	public String getCustName() {
		return custName;
	}
	/**
	 * 设置：工商注册号
	 */
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}
	/**
	 * 获取：工商注册号
	 */
	public String getBusinessNo() {
		return businessNo;
	}
	/**
	 * 设置：法人姓名
	 */
	public void setLegalPersonName(String legalPersonName) {
		this.legalPersonName = legalPersonName;
	}
	/**
	 * 获取：法人姓名
	 */
	public String getLegalPersonName() {
		return legalPersonName;
	}
	/**
	 * 设置：法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
	 */
	public void setLegalPersonCredType(String legalPersonCredType) {
		this.legalPersonCredType = legalPersonCredType;
	}
	/**
	 * 获取：法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
	 */
	public String getLegalPersonCredType() {
		return legalPersonCredType;
	}
	/**
	 * 设置：法人证件号码
	 */
	public void setLegalPersonCredNo(String legalPersonCredNo) {
		this.legalPersonCredNo = legalPersonCredNo;
	}
	/**
	 * 获取：法人证件号码
	 */
	public String getLegalPersonCredNo() {
		return legalPersonCredNo;
	}
	/**
	 * 设置：经营范围
	 */
	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}
	/**
	 * 获取：经营范围
	 */
	public String getBusinessScope() {
		return businessScope;
	}
	
	public String getBusinessLicence() {
		return businessLicence;
	}
	public void setBusinessLicence(String businessLicence) {
		this.businessLicence = businessLicence;
	}
	/**
	 * 设置：企业地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：企业地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：企业网址
	 */
	public void setHttpAddress(String httpAddress) {
		this.httpAddress = httpAddress;
	}
	/**
	 * 获取：企业网址
	 */
	public String getHttpAddress() {
		return httpAddress;
	}
	/**
	 * 设置：企业注册资金
	 */
	public void setRegisterAmount(BigDecimal registerAmount) {
		this.registerAmount = registerAmount;
	}
	/**
	 * 获取：企业注册资金
	 */
	public BigDecimal getRegisterAmount() {
		return registerAmount;
	}
	/**
	 * 设置：联系电话
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	/**
	 * 获取：联系电话
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * 设置：实名状态  00：未实名  01：已实名  02：待审核
	 */
	public void setVerifyFlag(String verifyFlag) {
		this.verifyFlag = verifyFlag;
	}
	/**
	 * 获取：实名状态  00：未实名  01：已实名  02：待审核
	 */
	public String getVerifyFlag() {
		return verifyFlag;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	@Override
	public String toString() {
		return "MerchantBase [merId=" + merId + ", parentMerId=" + parentMerId + ", customerId=" + customerId
				+ ", merchantNo=" + merchantNo + ", merType=" + merType + ", custName=" + custName + ", businessNo="
				+ businessNo + ", legalPersonName=" + legalPersonName + ", legalPersonCredType=" + legalPersonCredType
				+ ", legalPersonCredNo=" + legalPersonCredNo + ", businessScope=" + businessScope + ", businessLicence="
				+ businessLicence + ", address=" + address + ", httpAddress=" + httpAddress + ", registerAmount="
				+ registerAmount + ", telephone=" + telephone + ", verifyFlag=" + verifyFlag + ", accountStatus="
				+ accountStatus + ", createtime=" + createtime + ", updatetime=" + updatetime + "]";
	}
	
}
