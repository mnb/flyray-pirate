package com.github.wxiaoqi.security.crm.core.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;



/**
 * 个人账户
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("admin/personals/account")
public class PersonalAccountController extends BaseController<PersonalAccountBiz,PersonalAccount> {
	
	@Autowired
	private PersonalAccountBiz personalAccountBiz;
	
	
	/**
	 * 查询个人账户
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPersonalAccountInfos", method = RequestMethod.GET)
	public TableResultResponse<PersonalAccount> queryPersonalAccountInfos(@RequestParam(value = "accountStatus") String accountStatus, 
			@RequestParam(value = "perNo") String perNo, 
			@RequestParam(value = "mobile") String mobile,
			@RequestParam(value = "page", defaultValue = "1") Integer page, 
			@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
		Map<String, Object> map = new HashMap<>();
		map.put("accountStatus", accountStatus);
		map.put("perNo", perNo);
		map.put("mobile", mobile);
		map.put("page", page);
		map.put("limit", limit);
		log.info("查询个人账户，请求参数。。。{}"+map);
		Query query = new Query(map);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<PersonalAccount> list = personalAccountBiz.queryPersonalAccountInfos(map);
		log.info("查询个人账户，响应参数。。。{}"+list);
		return new TableResultResponse<PersonalAccount>(result.getTotal(), list);
	}
	
	
	/**
	 * 冻结/解冻 个人账户
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateAccountStatus", method = RequestMethod.POST)
	public Map<String, Object> updateAccountStatus(@RequestBody Map<String, Object> map) {
		log.info("冻结/解冻个人账户，请求参数。。。{}"+map);
		Map<String, Object> resp = personalAccountBiz.updateAccountStatus(map);
		log.info("冻结/解冻个人账户，响应参数。。。{}"+resp);
		return resp;
	}
	
	

}