package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 16:17:27
 */
@Table(name = "customer_message_rel")
public class CustomerMessageRel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private String seqNo;
	
	    //用户编号
    @Column(name = "CUSTOMER_ID")
    private String customerId;
	
	    //消息编号
    @Column(name = "MSG_ID")
    private String msgId;
	
	    //是否已读  00：未读  01：已读
    @Column(name = "ISREAD")
    private String isread;
	
	    //是否删除  00：正常  01：删除
    @Column(name = "ISDELETE")
    private String isdelete;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：序号
	 */
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：序号
	 */
	public String getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：消息编号
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	/**
	 * 获取：消息编号
	 */
	public String getMsgId() {
		return msgId;
	}
	/**
	 * 设置：是否已读  00：未读  01：已读
	 */
	public void setIsread(String isread) {
		this.isread = isread;
	}
	/**
	 * 获取：是否已读  00：未读  01：已读
	 */
	public String getIsread() {
		return isread;
	}
	/**
	 * 设置：是否删除  00：正常  01：删除
	 */
	public void setIsdelete(String isdelete) {
		this.isdelete = isdelete;
	}
	/**
	 * 获取：是否删除  00：正常  01：删除
	 */
	public String getIsdelete() {
		return isdelete;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	@Override
	public String toString() {
		return "CustomerMessageRel [seqNo=" + seqNo + ", customerId=" + customerId + ", msgId=" + msgId + ", isread="
				+ isread + ", isdelete=" + isdelete + ", createtime=" + createtime + ", updatetime=" + updatetime + "]";
	}
	
}
