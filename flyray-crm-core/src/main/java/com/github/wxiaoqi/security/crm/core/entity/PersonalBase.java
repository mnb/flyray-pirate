package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Base64;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.util.StringUtils;


/**
 * 个人基础信息
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "personal_base")
public class PersonalBase implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //个人信息编号
    @Id
    private String perId;
	
	    //用户编号
	@Column(name = "CUSTOMER_ID")
	private String customerId;

	    //会员号
    @Column(name = "PER_NO")
    private String perNo;
	
	    //所属商户
    @Column(name = "MER_ID")
    private String merId;
	
	    //用户名称
    @Column(name = "REAL_NAME")
    private String realName;
	
	    //身份证号
    @Column(name = "ID_NUMBER")
    private String idNumber;
	
	    //用户昵称
    @Column(name = "NICK_NAME")
    private String nickName;
	
	    //性别 1：男 2：女
    @Column(name = "SEX")
    private String sex;
	
	    //生日
    @Column(name = "BIRTHDAY")
    private String birthday;
	
	    //居住地
    @Column(name = "ADDRESS")
    private String address;
	
	    //家乡
    @Column(name = "HOMETOWN")
    private String hometown;
	
	    //实名状态  00：未实名  01：已实名  02：待审核
    @Column(name = "VERIFY_FLAG")
    private String verifyFlag;
	
	    //身份证正面
    @Column(name = "ID_POSITIVE")
    private String idPositive;
	
	    //身份证反面
    @Column(name = "ID_NEGATIVE")
    private String idNegative;
    
    //身份证正面字符串
    @Transient
    private String idPositiveStr;
	
	//身份证反面字符串
    @Transient
    private String idNegativeStr;
    
	    //账户状态 00：正常，01：冻结
	@Column(name = "ACCOUNT_STATUS")
	private String accountStatus;
    
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：个人信息编号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：会员号
	 */
	public void setPerNo(String perNo) {
		this.perNo = perNo;
	}
	/**
	 * 获取：会员号
	 */
	public String getPerNo() {
		return perNo;
	}
	/**
	 * 设置：所属商户
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：所属商户
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：用户名称
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/**
	 * 获取：用户名称
	 */
	public String getRealName() {
		return realName;
	}
	/**
	 * 设置：身份证号
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	/**
	 * 获取：身份证号
	 */
	public String getIdNumber() {
		return idNumber;
	}
	/**
	 * 设置：用户昵称
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	/**
	 * 获取：用户昵称
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * 设置：性别 1：男 2：女
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别 1：男 2：女
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * 设置：生日
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	/**
	 * 获取：生日
	 */
	public String getBirthday() {
		return birthday;
	}
	/**
	 * 设置：居住地
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：居住地
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：家乡
	 */
	public void setHometown(String hometown) {
		this.hometown = hometown;
	}
	/**
	 * 获取：家乡
	 */
	public String getHometown() {
		return hometown;
	}
	/**
	 * 设置：实名状态  00：未实名  01：已实名  02：待审核
	 */
	public void setVerifyFlag(String verifyFlag) {
		this.verifyFlag = verifyFlag;
	}
	/**
	 * 获取：实名状态  00：未实名  01：已实名  02：待审核
	 */
	public String getVerifyFlag() {
		return verifyFlag;
	}
	
	/**
	 * 设置：身份证正面
	 */
	public void setIdPositive(String idPositive) {
		this.idPositive = idPositive;
		
	}
	/**
	 * 获取：身份证正面
	 */
	public String getIdPositive() {
		return idPositive;
	}
	/**
	 * 设置：身份证反面
	 */
	public void setIdNegative(String idNegative) {
		this.idNegative = idNegative;
	}
	/**
	 * 获取：身份证反面
	 */
	public String getIdNegative() {
		return idNegative;
	}
	/**
	 * 获取：身份证正面字符串
	 */
	public String getIdPositiveStr() {
		return idPositiveStr;
	}
	/**
	 * 设置：身份证正面字符串
	 */
	public void setIdPositiveStr(String idPositiveStr) {
		this.idPositiveStr = idPositiveStr;
	}
	/**
	 * 获取：身份证反面字符串
	 */
	public String getIdNegativeStr() {
		return idNegativeStr;
	}
	/**
	 * 设置：身份证反面字符串
	 */
	public void setIdNegativeStr(String idNegativeStr) {
		this.idNegativeStr = idNegativeStr;
	}
	
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	@Override
	public String toString() {
		return "PersonalBase [perId=" + perId + ", customerId=" + customerId + ", perNo=" + perNo + ", merId=" + merId
				+ ", realName=" + realName + ", idNumber=" + idNumber + ", nickName=" + nickName + ", sex=" + sex
				+ ", birthday=" + birthday + ", address=" + address + ", hometown=" + hometown + ", verifyFlag="
				+ verifyFlag + ", idPositive=" + idPositive + ", idNegative=" + idNegative + ", idPositiveStr="
				+ idPositiveStr + ", idNegativeStr=" + idNegativeStr + ", accountStatus=" + accountStatus
				+ ", createtime=" + createtime + ", updatetime=" + updatetime + "]";
	}
}
