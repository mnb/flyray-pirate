package com.github.wxiaoqi.security.crm.core.feign;
/*package com.github.icloudpay.crm.core.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.crm.core.biz.SequenceNoBiz;
import com.github.wxiaoqi.security.common.rest.BaseController;

@Controller
@RequestMapping("sequenceNoFeign")
public class SequenceNoFeign extends BaseController<SequenceNoBiz,SequenceNo> {
	
	@Autowired
	private SequenceNoBiz sequenceNoBiz;
	
	*//**
	 * 获取用户编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getCustNo",method = RequestMethod.POST)
    @ResponseBody
	public String getCustNo(){
		return sequenceNoBiz.getCustNo();
	}
	
	*//**
	 * 获取商户编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getMerNo",method = RequestMethod.POST)
    @ResponseBody
	public String getMerNo(){
		return sequenceNoBiz.getMerNo();
	}
	*//**
	 * 获取用户账户编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getCustAccNo",method = RequestMethod.POST)
    @ResponseBody
	public String getCustAccNo(){
		return sequenceNoBiz.getCustAccNo();
	}
	*//**
	 * 获取商户账户编号
	 * 
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getMerAccNo",method = RequestMethod.POST)
    @ResponseBody
	public String getMerAccNo(){
		return sequenceNoBiz.getMerAccNo();
	}
	*//**
	 * 获取公共序列编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:45:27
	 * @return
	 *//*
	@RequestMapping(value = "/getSeqNo",method = RequestMethod.POST)
    @ResponseBody
	public String getSeqNo(){
		return sequenceNoBiz.getSeqNo();
	}
	*//**
	 * 获取接收任务表编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:45:27
	 * @return
	 *//*
	@RequestMapping(value = "/getReceiveSeqNo",method = RequestMethod.POST)
    @ResponseBody
	public String getReceiveSeqNo(){
		return sequenceNoBiz.getReceiveSeqNo();
	}
	*//**
	 * 获取任务用户简历关系表编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:45:27
	 * @return
	 *//*
	@RequestMapping(value = "/getTurSeqNo",method = RequestMethod.POST)
    @ResponseBody
	public String getTurSeqNo(){
		return sequenceNoBiz.getTurSeqNo();
	}
	*//**
	 * 获取职业编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getCareerNo",method = RequestMethod.POST)
    @ResponseBody
	public String getCareerNo(){
		return sequenceNoBiz.getCareerNo();
	}
	
	*//**
	 * 获取行业编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getIndustryNo",method = RequestMethod.POST)
    @ResponseBody
	public String getIndustryNo(){
		return sequenceNoBiz.getIndustryNo();
	}
	
	*//**
	 * 获取订单编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getOrderNo",method = RequestMethod.POST)
    @ResponseBody
	public String getOrderNo(){
		return sequenceNoBiz.getOrderNo();
	}
	
	*//**
	 * 获取简历编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getResumeNo",method = RequestMethod.POST)
    @ResponseBody
	public String getResumeNo(){
		return sequenceNoBiz.getResumeNo();
	}
	*//**
	 * 获取已使用简历编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getResumeUsedNo",method = RequestMethod.POST)
    @ResponseBody
	public String getResumeUsedNo(){
		return sequenceNoBiz.getResumeNo();
	}
	*//**
	 * 获取流水号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getSerialNo",method = RequestMethod.POST)
    @ResponseBody
	public String getSerialNo(){
		return sequenceNoBiz.getSerialNo();
	}
	
	*//**
	 * 获取标签编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getTagNo",method = RequestMethod.POST)
    @ResponseBody
	public String getTagNo(){
		return sequenceNoBiz.getTagNo();
	}
	
	*//**
	 * 获取任务编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	@RequestMapping(value = "/getTaskNo",method = RequestMethod.POST)
    @ResponseBody
	public String getTaskNo(){
		return sequenceNoBiz.getTaskNo();
	}
	


}*/