package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountJournalExtBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournalExt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("personals/accountJournalExt")
public class PersonalAccountJournalExtController extends BaseController<PersonalAccountJournalExtBiz,PersonalAccountJournalExt> {

}