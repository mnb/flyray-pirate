package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalVerifyRecord;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人基础信息
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("admin/personals/base")
public class PersonalBaseController extends BaseController<PersonalBaseBiz,PersonalBase> {

	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	
	/**
	 * 查询个人实名记录
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryRealNameRecord",method = RequestMethod.POST)
	public TableResultResponse<PersonalVerifyRecord> queryRealNameRecord(@RequestBody Map<String, Object> param) {
		log.info("查询个人实名记录，请求参数。。。{}");
		TableResultResponse<PersonalVerifyRecord> response = personalBaseBiz.queryRealNameRecord(param);
		log.info("查询个人实名记录，响应参数。。。{}"+response);
		return response;
	}
	
	
	/**
	 * 修改个人实名状态
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateVerifyFlag",method = RequestMethod.POST)
	public Map<String, Object> updateVerifyFlag(@RequestBody Map<String, Object> map) {
		log.info("修改个人实名状态，请求参数。。。{}"+map);
		String verifyFlag = (String) map.get("verifyFlag");
		Map<String, Object> res = personalBaseBiz.updateVerifyFlag(map);
		log.info("修改个人实名状态，响应参数。。。{}"+res);
		return res;
	}
}