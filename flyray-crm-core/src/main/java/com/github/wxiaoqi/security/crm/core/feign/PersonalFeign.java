package com.github.wxiaoqi.security.crm.core.feign;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.CustomerBiz;
import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseExtBiz;
import com.github.wxiaoqi.security.crm.core.entity.Customer;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("feign/personals")
public class PersonalFeign {
	@Autowired
	private CustomerBiz customerBiz;
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	@Autowired
	private PersonalAccountBiz personalAccountBiz;
	@Autowired
	private MerchantAccountBiz merchantAccountBiz;
	
	@Autowired
	private PersonalBaseExtBiz personalBaseExtBiz;
	/**
	 * 判断用户是否存在 
	 */
	@RequestMapping(value = "/exist", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> exist(@RequestBody Map<String, Object> reqs) {
		log.info("判断用户是否存在 - customerFeign - perIdExise 传入参数{}", reqs);
		String perId = (String) reqs.get("perId");
		PersonalBase base = personalBaseBiz.selectById(perId);
		Map<String, Object> result = new HashMap<String, Object>();
		if(base == null){
			result.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			result.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
			result.put("sucess", false);
		}else {
			result.put("code", ResponseCode.OK.getCode());
			result.put("msg", ResponseCode.OK.getMessage());
			result.put("sucess", true);
		}
		log.info("判断用户是否存在 - customerFeign - perIdExise 结束{}", result);
		return result;
	}
	/**
	 * 查询用户信息，编号，电话，出我关注的行业，我关注的职能，我的标签
	 */
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> info(@RequestBody Map<String, Object> reqs) {
		String custId = (String) reqs.get("custId");
		Customer base = customerBiz.selectById(custId);
		Map<String, Object> result = new HashMap<String, Object>();
		if(base == null){
			result.put("code", ResponseCode.PER_NOTEXIST.getCode());
			result.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
			result.put("sucess", false);
			return result;
		}else{
			PersonalBase perbase = personalBaseBiz.selectById(base.getPerId());
			Map<String, Object> baseMap=new HashMap<String, Object>();
			if(null!=perbase){
				 baseMap = EntityUtils.beanToMap(perbase);
			}
			result.put("personalBase",baseMap);
		}
		result.put("mobile",base.getMobile());
		PersonalBaseExt personalBaseExt = personalBaseExtBiz.selectById(base.getPerId());
		if (null != personalBaseExt) {
			result.put("attentionIndustry",personalBaseExt.getAttentionIndustry());
			result.put("attentionJobs", personalBaseExt.getAttentionJobs());
			result.put("selfTag", personalBaseExt.getSelfTag());
		}else{
			result.put("attentionIndustry","");
			result.put("attentionJobs", "");
			result.put("selfTag", "");
		}
		
		
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		result.put("sucess", true);
		return result;
	}
	
	
	/**
	 * 修改我关注的行业
	 */
	@RequestMapping(value = "/updateMineIndustry", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateMineIndustry(@RequestBody Map<String, Object> reqs) {
		String perId = (String) reqs.get("perId");
		String industry = (String) reqs.get("industry");
		PersonalBase base = personalBaseBiz.selectById(perId);
		Map<String, Object> result = new HashMap<String, Object>();
		if(base == null){
			result.put("code", ResponseCode.PER_NOTEXIST.getCode());
			result.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
			result.put("sucess", false);
		}
		PersonalBaseExt personalBaseExt = personalBaseExtBiz.selectById(perId);
		try {
			if(personalBaseExt == null){
				 personalBaseExt = new PersonalBaseExt();
				 personalBaseExt.setPerId(perId);
				 personalBaseExt.setAttentionIndustry(industry);
				 personalBaseExt.setCreatetime(new Timestamp(new Date().getTime()));
				 personalBaseExtBiz.insert(personalBaseExt);
			}else {
				personalBaseExt.setAttentionIndustry(industry);
				personalBaseExt.setUpdatetime(new Timestamp(new Date().getTime()));
				personalBaseExtBiz.updateById(personalBaseExt);
			}
			result.put("code", ResponseCode.OK.getCode());
			result.put("msg", ResponseCode.OK.getMessage());
			result.put("sucess", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("sucess", false);
		}
		return result;
		
	}
	/**
	 * 修改我关注的职能
	 */
	@RequestMapping(value = "/updateMineFunction", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateMineFunction(@RequestBody Map<String, Object> reqs) {
		String perId = (String) reqs.get("perId");
		String function = (String) reqs.get("function");
		PersonalBase base = personalBaseBiz.selectById(perId);
		Map<String, Object> result = new HashMap<String, Object>();
		if(base == null){
			result.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			result.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
			result.put("sucess", false);
		}
		PersonalBaseExt personalBaseExt = personalBaseExtBiz.selectById(perId);
		try {
			if(personalBaseExt == null){
				 personalBaseExt = new PersonalBaseExt();
				 personalBaseExt.setPerId(perId);
				 personalBaseExt.setAttentionJobs(function);
				 personalBaseExt.setCreatetime(new Timestamp(new Date().getTime()));
				 personalBaseExtBiz.insert(personalBaseExt);
			}else {
				personalBaseExt.setAttentionJobs(function);
				personalBaseExt.setUpdatetime(new Timestamp(new Date().getTime()));
				personalBaseExtBiz.updateById(personalBaseExt);
			}
			result.put("code", ResponseCode.OK.getCode());
			result.put("msg", ResponseCode.OK.getMessage());
			result.put("sucess", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("sucess", false);
		}
		return result;
		
	}
	/**
	 * 修改我的标签
	 */
	@RequestMapping(value = "/updateMineLable", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateLable(@RequestBody Map<String, Object> reqs) {
		String custId = (String) reqs.get("custId");
		String lable = (String) reqs.get("lable");
		Customer base = customerBiz.selectById(custId);
		Map<String, Object> result = new HashMap<String, Object>();
		if(base == null){
			result.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			result.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
			result.put("sucess", false);
		}
		PersonalBaseExt personalBaseExt = personalBaseExtBiz.selectById(base.getPerId());
		try {
			if(personalBaseExt == null){
				 personalBaseExt = new PersonalBaseExt();
				 personalBaseExt.setPerId(base.getPerId());
				 personalBaseExt.setSelfTag(lable);
				 personalBaseExt.setCreatetime(new Timestamp(new Date().getTime()));
				 personalBaseExtBiz.insert(personalBaseExt);
			}else {
				personalBaseExt.setSelfTag(lable);
				personalBaseExt.setUpdatetime(new Timestamp(new Date().getTime()));
				personalBaseExtBiz.updateById(personalBaseExt);
			}
			result.put("code", ResponseCode.OK.getCode());
			result.put("msg", ResponseCode.OK.getMessage());
			result.put("sucess", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("sucess", false);
		}
		return result;
		
	}

}
