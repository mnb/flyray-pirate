package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountJournalExtBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournalExt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin/merchants/accountJournalExt")
public class MerchantAccountJournalExtController extends BaseController<MerchantAccountJournalExtBiz,MerchantAccountJournalExt> {

}