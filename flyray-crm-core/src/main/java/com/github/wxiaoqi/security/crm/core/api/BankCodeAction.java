package com.github.wxiaoqi.security.crm.core.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.admin.pay.response.QueryBankCodeResponse;
import com.github.wxiaoqi.security.crm.core.client.BankCodeBizService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="银行卡管理")
@Controller
@RequestMapping("bankCode")
public class BankCodeAction {
	
	@Autowired
	private BankCodeBizService bankCodeBizService;
	
    @ApiOperation("获取银行编号")
	@RequestMapping(value = "/queryBankCode",method = RequestMethod.POST)
    @ResponseBody
	public QueryBankCodeResponse queryBankCode(){
		return bankCodeBizService.queryBankCode();
	}

}
