package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 企业账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "merchant_account_journal_ext")
public class MerchantAccountJournalExt implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //简历编号
    @Column(name = "RESUME_ID")
    private String resumeId;
	
	    //手续费
    @Column(name = "TRADE_FEE")
    private BigDecimal tradeFee;
    
	    //操作人ID
	@Column(name = "OPERATOR_ID")
	private String operatorId;
	
	
	//操作人
	@Column(name = "OPERATOR")
	private String operator;
	
	
	//邀请次数
	@Column(name = "INVITATION_NUM")
	private int invitationNum;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：简历编号
	 */
	public void setResumeId(String resumeId) {
		this.resumeId = resumeId;
	}
	/**
	 * 获取：简历编号
	 */
	public String getResumeId() {
		return resumeId;
	}
	/**
	 * 设置：手续费
	 */
	public void setTradeFee(BigDecimal tradeFee) {
		this.tradeFee = tradeFee;
	}
	/**
	 * 获取：手续费
	 */
	public BigDecimal getTradeFee() {
		return tradeFee;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}

	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public int getInvitationNum() {
		return invitationNum;
	}
	public void setInvitationNum(int invitationNum) {
		this.invitationNum = invitationNum;
	}
	@Override
	public String toString() {
		return "MerchantAccountJournalExt [journalId=" + journalId + ", resumeId=" + resumeId + ", tradeFee=" + tradeFee
				+ ", operatorId=" + operatorId + ", operator=" + operator + ", invitationNum=" + invitationNum
				+ ", createtime=" + createtime + ", updatetime=" + updatetime + "]";
	}
	
	
}
