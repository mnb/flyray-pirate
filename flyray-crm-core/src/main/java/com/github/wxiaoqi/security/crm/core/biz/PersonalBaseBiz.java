package com.github.wxiaoqi.security.crm.core.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.crm.core.client.FlyrayBizService;
import com.github.wxiaoqi.security.crm.core.entity.Customer;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import com.github.wxiaoqi.security.crm.core.entity.PersonalVerifyRecord;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseExtMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.SavePersonalBaseInfoParam;
import com.github.wxiaoqi.security.common.crm.request.VerifiedRealNameParam;
import com.github.wxiaoqi.security.common.msg.AccountType;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人基础信息
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class PersonalBaseBiz extends BaseBiz<PersonalBaseMapper,PersonalBase> {
	@Autowired
	private CustomerMapper customerMapper;
	@Autowired
	private PersonalBaseMapper personalBaseMapper;
	@Autowired
	private PersonalBaseExtMapper personalBaseExtMapper;
	@Autowired
	private PersonalAccountMapper personalAccountMapper;
	@Autowired
	private PersonalAccountJournalBiz personalAccountJournalBiz;
	@Autowired
	private SmsGateWayBiz smsGateWayBiz;
	@Autowired
	private FlyrayBizService flyrayBizService;
	
	@Value("${bountyHunter.acceptBizNo}")
	private String acceptBizNo;
	
	/**
	 * 获取猎手中心统计数据
	 * @author centerroot
	 * @time 创建时间:2018年5月18日上午10:37:58
	 * @return
	 */
	public Map<String, Object> getCustStatistics(Map<String, Object> param){
		log.info("获取猎手中心统计数据 请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String perId = (String) param.get("perId");
		PersonalBase personalBase = personalBaseMapper.selectByPrimaryKey(perId);
		if (null != personalBase) {
			// 查询用户账户可用赏金
			PersonalAccount reqCustAcc = new PersonalAccount();
			reqCustAcc.setPerId(personalBase.getPerId());
			reqCustAcc.setAccType(AccountType.ACC_BALANCE.getCode());// 余额账户
			PersonalAccount respCustAcc = personalAccountMapper.selectOne(reqCustAcc);
			if (null != respCustAcc) {
				respMap.put("accBalance", respCustAcc.getAccBalance());
			}else{
				respMap.put("accBalance", "0.00");
			}
			
			// 查询用户昨日收到赏金
			Map<String, Object> yestMap = personalAccountJournalBiz.getYesterdayIncome(personalBase);
			if (null != yestMap && ResponseCode.OK.getCode().equals(yestMap.get("code"))) {
				respMap.put("yesterdayIncome", yestMap.get("yesterdayIncome"));
			}else{
				respMap.put("yesterdayIncome", "0.00");
			}
			
			
			PersonalBaseExt personalBaseExt = personalBaseExtMapper.selectByPrimaryKey(perId);
			if (null != personalBaseExt) {
				//  查询用户关注的行业匹配悬赏、关注的职位匹配悬赏、我的标签匹配悬赏、昨日被查看简历数、昨日已淘汰简历数
				Map<String, Object> reqHunterMap = new HashMap<String, Object>();
				reqHunterMap.put("attentionIndustry", personalBaseExt.getAttentionIndustry());
				reqHunterMap.put("companyFunction", personalBaseExt.getAttentionJobs());
				reqHunterMap.put("selfTag", personalBaseExt.getSelfTag());
				reqHunterMap.put("custNo", perId);
				Map<String, Object> hunterMap = flyrayBizService.getHunterCenterInfo(reqHunterMap);
				if (null != hunterMap && ResponseCode.OK.getCode().equals(hunterMap.get("code"))) {
					// 关注的行业匹配悬赏
					respMap.put("industryCount", hunterMap.get("industryCount"));
					// 关注的职位匹配悬赏
					respMap.put("functionCount", hunterMap.get("functionCount"));
					// 我的标签匹配悬赏
					respMap.put("tagCount", hunterMap.get("tagCount"));
					// 昨日被查看简历数
					respMap.put("viewCount", hunterMap.get("viewCount"));
					// 昨日已淘汰简历数
					respMap.put("failedCount", hunterMap.get("failedCount"));
				} else {
					respMap.put("industryCount", "0");
					respMap.put("functionCount", "0");
					respMap.put("tagCount", "0");
					respMap.put("viewCount", "0");
					respMap.put("failedCount", "0");
				}
			} else {
				respMap.put("industryCount", "0");
				respMap.put("functionCount", "0");
				respMap.put("tagCount", "0");
				respMap.put("viewCount", "0");
				respMap.put("failedCount", "0");
			}
			
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		}else{
			respMap.put("code", ResponseCode.PER_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
		}

		log.info("获取猎手中心统计数据 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 保存用户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年5月21日上午10:53:24
	 * @param param
	 * @return
	 */
	public Map<String, Object> saveCustBaseInfo(SavePersonalBaseInfoParam baseInfoParam){
		log.info("保存用户基础信息 请求参数：{}",EntityUtils.beanToMap(baseInfoParam));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String perId = baseInfoParam.getPerId();
		try {
			PersonalBase personalBase = new PersonalBase();
			if (StringUtils.isEmpty(perId)) {
				String newPerId = String.valueOf(SnowFlake.getId());
				String customerId = baseInfoParam.getCustomerId();
				personalBase.setPerId(newPerId);
				personalBase.setCustomerId(customerId);
				personalBase.setPerNo(newPerId);
				personalBase.setNickName(baseInfoParam.getNickName());
				personalBase.setRealName(baseInfoParam.getRealName());
				personalBase.setSex(baseInfoParam.getSex());
				personalBase.setBirthday(baseInfoParam.getBirthday());
				personalBase.setAddress(baseInfoParam.getAddress());
				personalBase.setHometown(baseInfoParam.getHometown());
				personalBase.setVerifyFlag("00");
				personalBase.setAccountStatus("00");
				personalBase.setCreatetime(new Timestamp(System.currentTimeMillis()));
				personalBaseMapper.insert(personalBase);
				Customer customer = customerMapper.selectByPrimaryKey(customerId);
				customer.setPerId(newPerId);
				customerMapper.updateByPrimaryKey(customer);
			} else {
				personalBase = personalBaseMapper.selectByPrimaryKey(perId);
				personalBase.setNickName(baseInfoParam.getNickName());
				if ("00".equals(personalBase.getVerifyFlag())) {
					personalBase.setRealName(baseInfoParam.getRealName());
				}
				personalBase.setSex(baseInfoParam.getSex());
				personalBase.setBirthday(baseInfoParam.getBirthday());
				personalBase.setAddress(baseInfoParam.getAddress());
				personalBase.setHometown(baseInfoParam.getHometown());
				personalBaseMapper.updateByPrimaryKey(personalBase);
			}
			
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		}catch(Exception e){
			e.printStackTrace();
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		
		
		log.info("保存用户基础信息 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 用户安全信息实名认证
	 * @author centerroot
	 * @time 创建时间:2018年5月22日下午2:21:01
	 * @param param
	 * @return
	 */
	public Map<String, Object> verifiedRealName(VerifiedRealNameParam param){
		log.info("用户安全信息实名认证   请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		/*String perId = (String) param.get("perId");
		String realName = (String) param.get("realName");
		String idNumber = (String) param.get("idNumber");
		String mobile = (String) param.get("mobile");
		String idPositive = (String) param.get("idPositive");
		String idNegative = (String) param.get("idNegative");*/
		PersonalBase personalBase = personalBaseMapper.selectByPrimaryKey(param.getPerId());
		if (null != personalBase) {
			personalBase.setRealName(param.getRealName());
			personalBase.setIdNumber(param.getIdNumber());
			personalBase.setIdNegative(param.getIdNegativeStr());
			personalBase.setIdPositive(param.getIdPositiveStr());
			personalBase.setVerifyFlag("02"); // 待审核
			personalBaseMapper.updateByPrimaryKey(personalBase);
			
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
			
		}else{
			respMap.put("code", ResponseCode.PER_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
		}
		
		
		log.info("用户安全信息实名认证   响应参数：{}",respMap);
		return respMap;
	}
	
	
	/**
	 * 查询实名记录
	 * @param param
	 * @return
	 */
	public TableResultResponse<PersonalVerifyRecord> queryRealNameRecord(Map<String, Object> param) {
		log.info("查询个人实名认证记录参数。。。{}");
		Integer page = (Integer) param.get("page");
		Integer limit = (Integer) param.get("limit");
		Map<String, Object> respMap = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		Query query = new Query(params);
		 Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<PersonalVerifyRecord> res = personalBaseMapper.queryRealNameRecord(param);
		TableResultResponse<PersonalVerifyRecord> response = new TableResultResponse<PersonalVerifyRecord>(result.getTotal(), res);
		Map<String, Object> responseMap =EntityUtils.beanToMap(response);
		log.info("查询个人实名认证记录。。。{}"+responseMap);
		return response;
	}
	
	/**
	 * 修改实名状态
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateVerifyFlag(Map<String, Object> param) {
		log.info("修改个人实名状态参数。。。{}"+param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		personalBaseMapper.updateVerifyFlag(param);
		log.info("修改个人实名状态完成。。。{}");
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		return respMap;
	}
	
	
}