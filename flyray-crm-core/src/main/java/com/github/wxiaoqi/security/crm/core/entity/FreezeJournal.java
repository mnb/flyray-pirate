package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 冻结流水表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-01 14:54:58
 */
@Table(name = "freeze_journal")
public class FreezeJournal implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //客户类型  1：个人客户  2：企业客户
    @Column(name = "CON_TYPE")
    private String conType;
	
	    //客户账户编号
    @Column(name = "CON_ACC_ID")
    private String conAccId;
	
	    //订单号
    @Column(name = "ORDER_NO")
    private String orderNo;
	
	    //订单类型  01：悬赏订单 02：提现订单
    @Column(name = "ORDER_TYPE")
    private String orderType;
	
	    //冻结类型  1：资金冻结
    @Column(name = "FREEZE_TYPE")
    private String freezeType;
	
	    //金额
    @Column(name = "BALANCE")
    private BigDecimal balance;
	
	    //冻结状态 1：已冻结  2：部分解冻  3：已解冻
    @Column(name = "STATUS")
    private String status;
	
	    //时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：客户类型  1：个人客户  2：企业客户
	 */
	public void setConType(String conType) {
		this.conType = conType;
	}
	/**
	 * 获取：客户类型  1：个人客户  2：企业客户
	 */
	public String getConType() {
		return conType;
	}
	/**
	 * 设置：客户账户编号
	 */
	public void setConAccId(String conAccId) {
		this.conAccId = conAccId;
	}
	/**
	 * 获取：客户账户编号
	 */
	public String getConAccId() {
		return conAccId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：订单类型  01：悬赏订单 02：提现订单
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	/**
	 * 获取：订单类型  01：悬赏订单 02：提现订单
	 */
	public String getOrderType() {
		return orderType;
	}
	/**
	 * 设置：冻结类型  1：资金冻结
	 */
	public void setFreezeType(String freezeType) {
		this.freezeType = freezeType;
	}
	/**
	 * 获取：冻结类型  1：资金冻结
	 */
	public String getFreezeType() {
		return freezeType;
	}
	/**
	 * 设置：金额
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：冻结状态 1：已冻结  2：部分解冻  3：已解冻
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：冻结状态 1：已冻结  2：部分解冻  3：已解冻
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
}
