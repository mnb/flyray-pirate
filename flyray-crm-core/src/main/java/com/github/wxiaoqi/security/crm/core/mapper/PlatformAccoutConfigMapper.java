package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PlatformAccoutConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * 平台账户配置表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformAccoutConfigMapper extends Mapper<PlatformAccoutConfig> {
	
}
