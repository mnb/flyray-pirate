package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;


/**
 * 企业认证记录
 * @author chj
 *
 */
public class MerchantVerifyRecord implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//商户编号
	private String merId;
	
	//法人姓名
	private String legalPersonName;
	
	//法人证件号码
	private String legalPersonCredNo;
	
	//企业名称
	private String custName;
	
	//企业营业执照
	private String licenceStr;
	
	//企业营业执照
	private byte[] businessLicence;
	
	//认证状态
	private String verifyFlag;
	
	//创建时间
	private Timestamp createtime;

	public String getMerId() {
		return merId;
	}

	public void setMerId(String merId) {
		this.merId = merId;
	}

	public String getLegalPersonName() {
		return legalPersonName;
	}

	public void setLegalPersonName(String legalPersonName) {
		this.legalPersonName = legalPersonName;
	}

	public String getLegalPersonCredNo() {
		return legalPersonCredNo;
	}

	public void setLegalPersonCredNo(String legalPersonCredNo) {
		this.legalPersonCredNo = legalPersonCredNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getLicenceStr() {
		return licenceStr;
	}

	public void setLicenceStr(String licenceStr) {
		this.licenceStr = licenceStr;
	}

	public byte[] getBusinessLicence() {
		return businessLicence;
	}

	public void setBusinessLicence(byte[] businessLicence) {
		this.businessLicence = businessLicence;
	}

	public String getVerifyFlag() {
		return verifyFlag;
	}

	public void setVerifyFlag(String verifyFlag) {
		this.verifyFlag = verifyFlag;
	}

	public Timestamp getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}

	@Override
	public String toString() {
		return "MerchantVerifyRecord [merId=" + merId + ", legalPersonName=" + legalPersonName + ", legalPersonCredNo="
				+ legalPersonCredNo + ", custName=" + custName + ", licenceStr=" + licenceStr + ", businessLicence="
				+ Arrays.toString(businessLicence) + ", verifyFlag=" + verifyFlag + ", createtime=" + createtime + "]";
	}

}
