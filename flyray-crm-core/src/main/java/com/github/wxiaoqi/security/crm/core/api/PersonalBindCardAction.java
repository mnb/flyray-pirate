package com.github.wxiaoqi.security.crm.core.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.deser.Deserializers.Base;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBindCardBiz;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller
@RequestMapping("personals/cardInfo")
public class PersonalBindCardAction extends BaseAction{
	@Autowired
	private PersonalBindCardBiz personalBindCardBiz;
	
	/**
	 * 添加用户银行卡信息
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午1:37:30
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/savePersonalBindCardInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> savePersonalBindCardInfo(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("添加用户银行卡信息  请求参数：{}",param);
		Map<String, Object> respMap = personalBindCardBiz.savePersonalBindCardInfo(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("添加用户银行卡信息  响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 删除用户银行卡信息
	 * @author centerroot
	 * @time 创建时间:2018年5月18日下午5:01:13
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/deletePersonalBindCardInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> deletePersonalBindCardInfo(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("删除用户银行卡信息 请求参数：{}",param);
		Map<String, Object> respMap = personalBindCardBiz.deletePersonalBindCardInfo(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("删除用户银行卡信息 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询用户银行卡信息
	 * @author centerroot
	 * @time 创建时间:2018年5月21日上午10:43:56
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getPersonalBindCardInfoList",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getPersonalBindCardInfoList(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		log.info("查询用户银行卡信息 请求参数：{}",param);
		Map<String, Object> respMap = personalBindCardBiz.getPersonalBindCardInfoList(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查询用户银行卡信息 响应参数：{}",respMap);
		return respMap;
	}
	
}