package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;

import tk.mybatis.mapper.common.Mapper;

/**
 * 个人基础信息扩展表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBaseExtMapper extends Mapper<PersonalBaseExt> {
	
}
