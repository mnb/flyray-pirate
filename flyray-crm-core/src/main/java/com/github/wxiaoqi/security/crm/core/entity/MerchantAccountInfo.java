package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 企业账户
 * @author chj
 *
 */
public class MerchantAccountInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//商户号
    private String merchantNo;
	
	//会员手机号
    private String mobile;
	
	//账户编号
    private String accId;
	
	//账户余额
    private BigDecimal accBalance;
	
	//可用余额
    private BigDecimal viableBalance;
	
	//冻结余额
    private BigDecimal frezeeBalance;

	//面试次数
	private Integer interviews;
	
	//账户状态
    private String accountStatus;
    
    //账户类型
    private String accType;
    
    //用户类型 00 平台；01普通客户
    private String customerType;

	public String getMerchantNo() {
		return merchantNo;
	}

	public void setMerchantNo(String merchantNo) {
		this.merchantNo = merchantNo;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAccId() {
		return accId;
	}

	public void setAccId(String accId) {
		this.accId = accId;
	}

	public BigDecimal getAccBalance() {
		return accBalance;
	}

	public void setAccBalance(BigDecimal accBalance) {
		this.accBalance = accBalance;
	}

	public BigDecimal getViableBalance() {
		return viableBalance;
	}

	public void setViableBalance(BigDecimal viableBalance) {
		this.viableBalance = viableBalance;
	}

	public BigDecimal getFrezeeBalance() {
		return frezeeBalance;
	}

	public void setFrezeeBalance(BigDecimal frezeeBalance) {
		this.frezeeBalance = frezeeBalance;
	}

	public Integer getInterviews() {
		return interviews;
	}

	public void setInterviews(Integer interviews) {
		this.interviews = interviews;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	@Override
	public String toString() {
		return "MerchantAccountInfo [merchantNo=" + merchantNo + ", mobile=" + mobile + ", accId=" + accId
				+ ", accBalance=" + accBalance + ", viableBalance=" + viableBalance + ", frezeeBalance=" + frezeeBalance
				+ ", interviews=" + interviews + ", accountStatus=" + accountStatus + ", accType=" + accType
				+ ", customerType=" + customerType + "]";
	}

	
}
