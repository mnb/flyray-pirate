package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBindCard;
import tk.mybatis.mapper.common.Mapper;

/**
 * 用户绑卡表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 16:28:16
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBindCardMapper extends Mapper<PersonalBindCard> {
	/**
	 * 查询用户绑卡列表
	 * @author centerroot
	 * @time 创建时间:2018年5月29日下午3:45:13
	 * @param param
	 * @return
	 */
	List<PersonalBindCard> queryPersonalBindCardMap(Map<String, Object> param);
	
}
