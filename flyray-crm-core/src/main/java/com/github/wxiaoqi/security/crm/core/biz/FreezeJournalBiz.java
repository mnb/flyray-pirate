package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.FreezeJournal;
import com.github.wxiaoqi.security.crm.core.mapper.FreezeJournalMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 冻结流水表
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-01 14:54:58
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class FreezeJournalBiz extends BaseBiz<FreezeJournalMapper,FreezeJournal> {
}