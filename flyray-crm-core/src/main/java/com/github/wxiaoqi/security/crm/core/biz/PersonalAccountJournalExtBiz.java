package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournalExt;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountJournalExtMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人账户流水扩展表
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class PersonalAccountJournalExtBiz extends BaseBiz<PersonalAccountJournalExtMapper,PersonalAccountJournalExt> {
}