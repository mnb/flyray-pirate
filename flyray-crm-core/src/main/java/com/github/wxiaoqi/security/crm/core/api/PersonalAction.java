package com.github.wxiaoqi.security.crm.core.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.VerifiedRealNameParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.biz.PasswordBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;
import com.github.wxiaoqi.security.crm.core.biz.SmsGateWayBiz;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller
@RequestMapping("personals")
public class PersonalAction extends BaseAction{
	private static final Logger logger = LoggerFactory.getLogger(PersonalAction.class);
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	@Autowired
	private SmsGateWayBiz smsGateWayBiz;
	@Autowired
	private PasswordBiz passwordBiz;

	
	
	
	/**
	 * 用户安全信息实名认证
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午2:48:35
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/verifiedRealName",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> verifiedRealName(VerifiedRealNameParam param) throws Exception{
		//Map<String, Object> param=ParamStr2Bean(request);
		logger.info("用户安全信息实名认证    请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = personalBaseBiz.verifiedRealName(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		logger.info("用户安全信息实名认证 响应参数：{}",respMap);
		return respMap;
	}
	

	
}