package com.github.wxiaoqi.security.crm.core.client;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "flyray-biz")
public interface FlyrayBizService {
	/**
	 * 获取猎手中心任务简历统计数据
	 */
	@RequestMapping(value = "feign/userCenter/hunterCenterInfo",method = RequestMethod.POST)
	public Map<String, Object> getHunterCenterInfo(Map<String, Object> req);
	
	/**
	 * 获取悬赏中心任务简历统计数据
	 */
	@RequestMapping(value = "feign/userCenter/rewardCenterInfo",method = RequestMethod.POST)
	public Map<String, Object> getRewardCenterInfo(Map<String, Object> req);
}
