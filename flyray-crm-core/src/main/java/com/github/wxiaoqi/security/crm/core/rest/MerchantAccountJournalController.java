package com.github.wxiaoqi.security.crm.core.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountJournalBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.UnderLineRecharge;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;


/**
 * 商户账户流水
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("admin/merchants/accountJournal")
public class MerchantAccountJournalController extends BaseController<MerchantAccountJournalBiz,MerchantAccountJournal> {

	@Autowired
	private MerchantAccountJournalBiz merchantAccountJournalBiz;
	
	/**
	 * 查询商户账户流水
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryMerchantAccountJournal", method = RequestMethod.GET)
	public TableResultResponse<MerchantAccountJournal> queryMerchantAccountJournal(@RequestParam(value = "tradeType") String tradeType, 
			@RequestParam(value = "orderNo") String orderNo, 
			@RequestParam(value = "journalId") String journalId,
			@RequestParam(value = "merchantNo") String merchantNo, 
			@RequestParam(value = "mobile") String mobile,
			@RequestParam(value = "startDate") String startDate, 
			@RequestParam(value = "endDate") String endDate, 
			@RequestParam(value = "page", defaultValue = "1") Integer page, 
			@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
		Map<String, Object> param = new HashMap<>();
		param.put("tradeType", tradeType);
		param.put("orderNo", orderNo);
		param.put("journalId", journalId);
		param.put("merchantNo", merchantNo);
		param.put("mobile", mobile);
		param.put("startDate", startDate);
		param.put("endDate", endDate);
		param.put("page", page);
		param.put("limit", limit);
		log.info("查询商户账户流水，请求参数。。。{}"+param);
		Query query = new Query(param);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<MerchantAccountJournal> list = merchantAccountJournalBiz.queryAccountJournal(param);
		log.info("查询商户账户流水，响应参数。。。{}"+list);
		return new TableResultResponse<>(result.getTotal(), list);
	}
	
	
	
	
	/**
	 * 查询线下充值记录
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryUnderLineRecharge", method = RequestMethod.GET)
	public TableResultResponse<UnderLineRecharge> queryUnderLineRecharge(@RequestParam(value = "page", defaultValue = "1") Integer page, 
			@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
		
		Map<String, Object> param = new HashMap<>();
		param.put("page", page);
		param.put("limit", limit);
		log.info("查询线下充值记录，请求参数。。。{}"+param);
		Query query = new Query(param);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<UnderLineRecharge> list = merchantAccountJournalBiz.queryUnderLineRecharge(param);
		log.info("查询线下充值记录，响应参数。。。{}"+list);
		return new TableResultResponse<>(result.getTotal(), list);
	}
	

}