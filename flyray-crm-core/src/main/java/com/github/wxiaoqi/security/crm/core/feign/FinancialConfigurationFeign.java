package com.github.wxiaoqi.security.crm.core.feign;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.FinancialConfigurationBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("feign/financial/config")
public class FinancialConfigurationFeign {
	@Autowired
	private FinancialConfigurationBiz financialConfigurationBiz;
	
	/**
	 * 平台服务费  and 平台管理费
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:31:22
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformServiceAndManageFee",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformServiceAndManageFee(@RequestBody Map<String, Object> params){
		log.info("平台服务费  and 平台管理费    请求参数：{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();
		Map<String, Object> respServiceMap = financialConfigurationBiz.platformServiceFee(params);
		Map<String, Object> respManageMap = financialConfigurationBiz.platformManagementFee(params);
		respMap.put("serviceFee", respServiceMap.get("amount"));
		respMap.put("manageFee", respManageMap.get("amount"));
		respMap.put("sucess", true);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("平台服务费  and 平台管理费    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 平台服务费计算
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:31:22
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformServiceFee",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformServiceFee(@RequestBody Map<String, Object> params){
		log.info("平台服务费计算    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformServiceFee(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("平台服务费计算    响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 平台管理费计算
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:31:10
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformManagementFee",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformManagementFee(@RequestBody Map<String, Object> params){
		log.info("平台管理费计算    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformManagementFee(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("平台管理费计算    响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 提现手续费计算
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:31:01
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformWithdrawFee",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformWithdrawFee(@RequestBody Map<String, Object> params){
		log.info("提现手续费计算    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformWithdrawFee(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("提现手续费计算    响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 充值最小金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:30:43
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformRechargeAmtMin",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformRechargeAmtMin(@RequestBody Map<String, Object> params){
		log.info("充值最小金额    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformRechargeAmtMin(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("充值最小金额    响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 悬赏单人最小金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:30:20
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformWithdrawAmtMin",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformWithdrawAmtMin(@RequestBody Map<String, Object> params){
		log.info("悬赏单人最小金额    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformWithdrawAmtMin(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("悬赏单人最小金额    响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 提现最小金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:30:12
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformRewardAmtMin",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformRewardAmtMin(@RequestBody Map<String, Object> params){
		log.info("悬赏单人最小金额    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformRewardAmtMin(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("悬赏单人最小金额    响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 查看简历悬赏方扣除金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:29:50
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformResumeDeductionAmt",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformResumeDeductionAmt(@RequestBody Map<String, Object> params){
		log.info("查看简历悬赏方扣除金额    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformResumeDeductionAmt(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查看简历悬赏方扣除金额    响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 查看简历猎手的补贴金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:29:24
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/platformResumeSubsidiesAmt",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> platformResumeSubsidiesAmt(@RequestBody Map<String, Object> params){
		log.info("查看简历猎手的补贴金额    请求参数：{}",params);
		Map<String, Object> respMap = financialConfigurationBiz.platformResumeSubsidiesAmt(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查看简历猎手的补贴金额    响应参数：{}",respMap);
		return respMap;
	}
}
