package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 平台账户配置表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@Table(name = "platform_accout_config")
public class PlatformAccoutConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private String seqNo;
	
	    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
	
	    //客户类型  CON001：个人客户    CON002：企业客户
    @Column(name = "CON_TYPE")
    private String conType;
	
	    //账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：已冻结账户 等......
    @Column(name = "ACC_TYPE")
    private String accType;
	

	/**
	 * 设置：序号
	 */
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：序号
	 */
	public String getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：客户类型  CON001：个人客户    CON002：企业客户
	 */
	public void setConType(String conType) {
		this.conType = conType;
	}
	/**
	 * 获取：客户类型  CON001：个人客户    CON002：企业客户
	 */
	public String getConType() {
		return conType;
	}
	/**
	 * 设置：账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：已冻结账户 等......
	 */
	public void setAccType(String accType) {
		this.accType = accType;
	}
	/**
	 * 获取：账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：已冻结账户 等......
	 */
	public String getAccType() {
		return accType;
	}
}
