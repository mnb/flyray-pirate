package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * 企业账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
public class MerchantAccountJournalInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    private String journalId;
	
	    //账户编号
    private String accId;
	
	    //平台编号
    private String platformId;
	
	    //订单号
    private String orderNo;
	
	    //商户编号
    private String merId;
	
	    //个人信息编号
    private String perId;
	
	    //账户类型    ACC001：余额账户
    private String accType;
	
	    //来往标志  1：来账   2：往账 
    private String inOutFlag;
	
	    //交易金额
    private BigDecimal tradeAmt;
	
	    //交易类型  01：充值，02：提现，03：简历补贴，04：悬赏金 05：任务冻结 06：任务解冻
    private String tradeType;
    
	    //简历编号
	private String resumeId;
	
	    //手续费
	private BigDecimal tradeFee;
	
	    //操作人ID
	private String operatorId;
	
	
	//操作人
	private String operator;
	
	
	//邀请次数
	private int invitationNum;
	
	    //创建时间
    private Timestamp createtime;
	
	    //更新时间
    private Timestamp updatetime;
	

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：账户编号
	 */
	public void setAccId(String accId) {
		this.accId = accId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccId() {
		return accId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerId(String merId) {
		this.merId = merId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerId() {
		return merId;
	}
	/**
	 * 设置：个人信息编号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：账户类型    ACC001：余额账户
	 */
	public void setAccType(String accType) {
		this.accType = accType;
	}
	/**
	 * 获取：账户类型    ACC001：余额账户
	 */
	public String getAccType() {
		return accType;
	}
	/**
	 * 设置：来往标志  1：来账   2：往账 
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	/**
	 * 获取：来往标志  1：来账   2：往账 
	 */
	public String getInOutFlag() {
		return inOutFlag;
	}
	/**
	 * 设置：交易金额
	 */
	public void setTradeAmt(BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getTradeAmt() {
		return tradeAmt;
	}
	/**
	 * 设置：交易类型  01：充值，02：提现，03：简历补贴，04：悬赏金 05：任务冻结 06：任务解冻
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型  01：充值，02：提现，03：简历补贴，04：悬赏金 05：任务冻结 06：任务解冻
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：简历编号
	 */
	public void setResumeId(String resumeId) {
		this.resumeId = resumeId;
	}
	/**
	 * 获取：简历编号
	 */
	public String getResumeId() {
		return resumeId;
	}
	/**
	 * 设置：手续费
	 */
	public void setTradeFee(BigDecimal tradeFee) {
		this.tradeFee = tradeFee;
	}
	/**
	 * 获取：手续费
	 */
	public BigDecimal getTradeFee() {
		return tradeFee;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public int getInvitationNum() {
		return invitationNum;
	}
	public void setInvitationNum(int invitationNum) {
		this.invitationNum = invitationNum;
	}
	@Override
	public String toString() {
		return "MerchantAccountJournalInfo [journalId=" + journalId + ", accId=" + accId + ", platformId=" + platformId
				+ ", orderNo=" + orderNo + ", merId=" + merId + ", perId=" + perId + ", accType=" + accType
				+ ", inOutFlag=" + inOutFlag + ", tradeAmt=" + tradeAmt + ", tradeType=" + tradeType + ", resumeId="
				+ resumeId + ", tradeFee=" + tradeFee + ", operatorId=" + operatorId + ", operator=" + operator
				+ ", invitationNum=" + invitationNum + ", createtime=" + createtime + ", updatetime=" + updatetime
				+ "]";
	}
	
}
