package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.crm.core.entity.MerchantVerifyRecord;

import tk.mybatis.mapper.common.Mapper;

/**
 * 商户基础信息
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantBaseMapper extends Mapper<MerchantBase> {
	
	
	/**
	 * 查询企业认证记录
	 * @author chj
	 * @return
	 */
	List<MerchantVerifyRecord> queryRealNameRecord();
	
	
	/**
	 * 修改认证状态
	 * @author chj
	 */
	void updateVerifyFlag(Map<String, Object> map);
}
