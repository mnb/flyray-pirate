package com.github.wxiaoqi.security.crm.core.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.FinancialConfigurationBiz;
import com.github.wxiaoqi.security.crm.core.entity.FinancialConfiguration;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("admin/financialConfiguration")
public class FinancialConfigurationController extends BaseController<FinancialConfigurationBiz,FinancialConfiguration> {

	
	@Autowired
	private FinancialConfigurationBiz biz;
	/**
	 * 财务配置
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> queryPersonalAccountInfos(@RequestBody Map<String, Object> param) {
		log.info("财务配置操作，请求参数。。。{}"+param);
		String platformId = (String) param.get("platformId");
		String projectCode = (String) param.get("projectCode");
		String projectName = (String) param.get("projectName");
		String configType = (String) param.get("configType");
		String value = (String) param.get("value");
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", platformId);
		reqMap.put("projectCode", projectCode);
		List<FinancialConfiguration> list = biz.queryList(reqMap);
		if (list != null && list.size() > 0) {
			FinancialConfiguration configuration = list.get(0);
			configuration.setConfigType(configType);
			configuration.setProjectName(projectName);
			configuration.setValue(Double.parseDouble(value)/100);
			biz.updateConfiguration(configuration);
		}else {
			biz.addConfiguration(param);
		}
		return new HashMap<>();
	}
}