package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 个人基础信息扩展表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:47
 */
@Table(name = "personal_base_ext")
public class PersonalBaseExt implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //个人信息编号
    @Id
    private String perId;
	
	    //关注行业
    @Column(name = "ATTENTION_INDUSTRY")
    private String attentionIndustry;
	
	    //关注职位
    @Column(name = "ATTENTION_JOBS")
    private String attentionJobs;
	
	    //标签
    @Column(name = "SELF_TAG")
    private String selfTag;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	

	/**
	 * 设置：个人信息编号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：关注行业
	 */
	public void setAttentionIndustry(String attentionIndustry) {
		this.attentionIndustry = attentionIndustry;
	}
	/**
	 * 获取：关注行业
	 */
	public String getAttentionIndustry() {
		return attentionIndustry;
	}
	/**
	 * 设置：关注职位
	 */
	public void setAttentionJobs(String attentionJobs) {
		this.attentionJobs = attentionJobs;
	}
	/**
	 * 获取：关注职位
	 */
	public String getAttentionJobs() {
		return attentionJobs;
	}
	/**
	 * 设置：标签
	 */
	public void setSelfTag(String selfTag) {
		this.selfTag = selfTag;
	}
	/**
	 * 获取：标签
	 */
	public String getSelfTag() {
		return selfTag;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
}
