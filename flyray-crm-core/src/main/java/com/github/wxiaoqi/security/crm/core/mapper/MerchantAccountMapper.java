package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 企业账户
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantAccountMapper extends Mapper<MerchantAccount> {
	/**
	 * 查询用户账户信息
	 * @author centerroot
	 * @time 创建时间:2018年5月28日下午4:30:48
	 * @param param
	 * @return
	 */
	List<MerchantAccount> queryMerchantAccount(Map<String, Object> param);
	
	
	/**
	 * 查询商户账户
	 * @author chj
	 * @param param
	 * @return
	 */
	List<MerchantAccountInfo> queryMerchantAccountInfos(Map<String, Object> param);
	
	
	/**
	 * 修改账户状态
	 * @param param
	 */
	void updateAccountStatus(Map<String, Object> param);
}
