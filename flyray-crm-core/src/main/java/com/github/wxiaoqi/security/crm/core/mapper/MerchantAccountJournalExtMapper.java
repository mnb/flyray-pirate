package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournalExt;
import tk.mybatis.mapper.common.Mapper;

/**
 * 企业账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantAccountJournalExtMapper extends Mapper<MerchantAccountJournalExt> {
	
}
