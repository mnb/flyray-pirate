package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;



/**
 * 个人账户
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@Table(name = "personal_account")
public class PersonalAccount implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //账户编号
    @Id
    private String accId;
	
	    //个人信息编号
    @Column(name = "PER_ID")
    private String perId;
	
	    //账户类型    ACC001：余额账户
    @Column(name = "ACC_TYPE")
    private String accType;
	
	    //币种  CNY：人民币
    @Column(name = "CCY")
    private String ccy;
	
	    //账户余额
    @Column(name = "ACC_BALANCE")
    private BigDecimal accBalance;
	
	    //
    @Column(name = "CHECKSUM")
    private String checksum;
    
	    //账户状态 00：正常，01：冻结
	@Column(name = "ACCOUNT_STATUS")
	private String accountStatus;
	
	    //创建时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	
	    //更新时间
    @Column(name = "UPDATETIME")
    private Timestamp updatetime;
	
    //手机号
    @Transient
    private String mobile;
    
    //会员编号
    @Transient
    private String perNo;
    
    //用户类型
    @Transient
    private String customerType;
    
	/**
	 * 设置：账户编号
	 */
	public void setAccId(String accId) {
		this.accId = accId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccId() {
		return accId;
	}
	/**
	 * 设置：个人信息编号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：账户类型    ACC001：余额账户
	 */
	public void setAccType(String accType) {
		this.accType = accType;
	}
	/**
	 * 获取：账户类型    ACC001：余额账户
	 */
	public String getAccType() {
		return accType;
	}
	/**
	 * 设置：币种  CNY：人民币
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	/**
	 * 获取：币种  CNY：人民币
	 */
	public String getCcy() {
		return ccy;
	}
	/**
	 * 设置：账户余额
	 */
	public void setAccBalance(BigDecimal accBalance) {
		this.accBalance = accBalance;
	}
	/**
	 * 获取：账户余额
	 */
	public BigDecimal getAccBalance() {
		return accBalance;
	}
	/**
	 * 设置：
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	/**
	 * 获取：
	 */
	public String getChecksum() {
		return checksum;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：创建时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}
	/**
	 * 获取：更新时间
	 */
	public Timestamp getUpdatetime() {
		return updatetime;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPerNo() {
		return perNo;
	}
	public void setPerNo(String perNo) {
		this.perNo = perNo;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	@Override
	public String toString() {
		return "PersonalAccount [accId=" + accId + ", perId=" + perId + ", accType=" + accType + ", ccy=" + ccy
				+ ", accBalance=" + accBalance + ", checksum=" + checksum + ", accountStatus=" + accountStatus
				+ ", createtime=" + createtime + ", updatetime=" + updatetime + ", mobile=" + mobile + ", perNo="
				+ perNo + ", customerType=" + customerType + "]";
	}
	
}
