package com.github.wxiaoqi.security.crm.core.biz;
/*package com.github.icloudpay.crm.core.biz;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.crm.core.entity.SequenceNo;
import com.github.icloudpay.crm.core.mapper.SequenceNoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

*//**
 * 序列表   负责存放sequence滚动序列值
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-17 15:32:12
 *//*
 *
@Transactional(rollbackFor = Exception.class)
@Service
public class SequenceNoBiz extends BaseBiz<SequenceNoMapper,SequenceNo> {

	@Autowired
	private SequenceNoMapper sequenceNoMapper;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private String dateStr = sdf.format(new Date());
	*//**
	 * 获取公共序列编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:45:27
	 * @return
	 *//*
	public String getSeqNo(){
		String param = "seqNo";
		return String.valueOf(getSeqNo(param));
	}
	
	public String getReceiveSeqNo(){
		String param = "receiveNo";
		return String.valueOf(getSeqNo(param));
	}
	
	public String getTurSeqNo(){
		String param = "turNo";
		return String.valueOf(getSeqNo(param));
	}
	*//**
	 * 获取用户号
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午1:47:47
	 * @return
	 *//*
	public String getUserNo(){
		String param = "userNo";
		return String.valueOf(getSeqNo(param));
	}
	
	
	
	*//**
	 * 获取用户编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getCustNo(){
		return getSerialNumber28("custNo");
	}
	
	*//**
	 * 获取会员号
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午1:47:47
	 * @return
	 *//*
	public String getMemberNo(){
		String param = "memberNo";
		return String.valueOf(getSeqNo(param));
	}
	
	*//**
	 * 获取用户账户编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getCustAccNo(){
		String param = "custAccNo";
		int seqNo = getSeqNo(param);
		String seqNoStr=String.format("%010d", seqNo);
		return "622" + seqNoStr;
	}
	
	
	
	
	*//**
	 * 获取商户编号
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getMerNo(){
		return getSerialNumber28("merNo");
	}
	*//**
	 * 获取商户号
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午1:48:57
	 * @return
	 *//*
	public String getMerchantNo(){
		String param = "merchantNo";
		return String.valueOf(getSeqNo(param));
	}
	
	*//**
	 * 获取商户账户编号
	 * 
	 * （纯数字字符串）
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getMerAccNo(){
		String param = "merAccNo";
		int seqNo = getSeqNo(param);
		String seqNoStr=String.format("%010d", seqNo);
		return "622" + seqNoStr;
	}
	
	
	
	*//**
	 * 获取职业编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getCareerNo(){
		return getSerialNumber28("careerNo");
	}
	
	*//**
	 * 获取行业编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getIndustryNo(){
		return getSerialNumber28("industryNo");
	}
	
	*//**
	 * 获取订单编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getOrderNo(){
		return getSerialNumber28("orderNo");
	}
	
	*//**
	 * 获取简历编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getResumeNo(){
		return getSerialNumber28("resumeNo");
	}
	public String getResumeUsedNo(){
		return getSerialNumber28("resumeUsedNo");
	}
	*//**
	 * 获取流水号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getSerialNo(){
		return getSerialNumber28("serialNo");
	}
	
	*//**
	 * 获取标签编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getTagNo(){
		return getSerialNumber28("tagNo");
	}
	
	*//**
	 * 获取标签编号
	 * （纯数字字符串）
	 * 8位日期+2位标识（00：数据sequence 01：随机数）+ 10位数字
	 * @author centerroot
	 * @time 创建时间:2018年5月17日下午4:30:48
	 * @return
	 *//*
	public String getTaskNo(){
		return getSerialNumber28("taskNo");
	}
	
	public String getSerialNumber28(String param){
		String serialNumber = null;
		try {
			int seqNo = getSeqNo(param);
			String seqNoStr=String.format("%010d", seqNo);
			serialNumber = (dateStr + "00" + seqNoStr);
		} catch (Exception e) {
			Random ne=new Random();
	        int fiveNo1 = ne.nextInt(99999-10000+1)+10000;
	        int fiveNo2 = ne.nextInt(99999-10000+1)+10000;
	        serialNumber = (dateStr + "01" + fiveNo1) + fiveNo2;
		}
		return serialNumber;
	}
	
	
	public synchronized int getSeqNo(String param){
		SequenceNo sequenceNo = sequenceNoMapper.selectByPrimaryKey(param);
		int seqNo = sequenceNo.getSeqNo();
		sequenceNo.setSeqNo(sequenceNo.getSeqNo()+1);
		sequenceNoMapper.updateByPrimaryKey(sequenceNo);
        return seqNo;
    }
}*/