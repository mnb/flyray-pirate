package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 财务配置
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-20 14:19:07
 */
@Table(name = "financial_configuration")
public class FinancialConfiguration implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer seqNo;
	
	    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
	
	    //项目编码
    @Column(name = "PROJECT_CODE")
    private String projectCode;
	
	    //项目名称
    @Column(name = "PROJECT_NAME")
    private String projectName;
	
	    //配置类型  01：固定值  02：百分比
    @Column(name = "CONFIG_TYPE")
    private String configType;
	
	    //参数值
    @Column(name = "VALUE")
    private Double value;
	

	/**
	 * 设置：序号
	 */
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：序号
	 */
	public Integer getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：项目编码
	 */
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	/**
	 * 获取：项目编码
	 */
	public String getProjectCode() {
		return projectCode;
	}
	/**
	 * 设置：项目名称
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * 获取：项目名称
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * 设置：配置类型  01：固定值  02：百分比
	 */
	public void setConfigType(String configType) {
		this.configType = configType;
	}
	/**
	 * 获取：配置类型  01：固定值  02：百分比
	 */
	public String getConfigType() {
		return configType;
	}
	/**
	 * 设置：参数值
	 */
	public void setValue(Double value) {
		this.value = value;
	}
	/**
	 * 获取：参数值
	 */
	public Double getValue() {
		return value;
	}
	@Override
	public String toString() {
		return "FinancialConfiguration [seqNo=" + seqNo + ", platformId=" + platformId + ", projectCode=" + projectCode
				+ ", projectName=" + projectName + ", configType=" + configType + ", value=" + value + "]";
	}
	
	
}
