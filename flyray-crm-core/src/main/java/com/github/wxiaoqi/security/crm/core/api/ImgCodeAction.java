package com.github.wxiaoqi.security.crm.core.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.biz.CreateImgCodeBiz;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller
@RequestMapping("imgCode")
public class ImgCodeAction extends BaseAction{
	@Autowired
	private CreateImgCodeBiz createImgCodeBiz;
	
	/**
	 * 获取图形验证码
	 * 服务器不存储，前段自己校验
	 * @author centerroot
	 * @time 创建时间:2018年5月22日上午10:51:49
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/getImgCode",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getImgCode(){
		Map<String, Object> respMap = createImgCodeBiz.createImgCode();
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		return respMap;
	}
	
	/**
	 * 注册获取图形验证码
	 * 保存到缓存中
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午5:21:42
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/getRegistImgCode",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getRegistImgCode(){
		Map<String, Object> respMap = createImgCodeBiz.createRegisterImgCode();
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		return respMap;
	}
	
	/**
	 * 根据手机号获取图形验证码
	 * 根据手机号做key保存到缓存中
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午5:21:42
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getMobImgCode",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getMobImgCode(HttpServletRequest request) throws Exception{
		Map<String, Object> param=ParamStr2Bean(request);
		Map<String, Object> respMap = createImgCodeBiz.createMobImgCode(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		return respMap;
	}
	
}