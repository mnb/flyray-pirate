package com.github.wxiaoqi.security.crm.core.mapper;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.crm.core.entity.Customer;

import java.util.List;
import java.util.Map;

import tk.mybatis.mapper.common.Mapper;

/**
 * 注册客户（会员）信息表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface CustomerMapper extends Mapper<Customer> {
	Customer queryUserinfoByMobile(@Param(value="mobile") String mobile,@Param(value="platformId") String platformId);
	
	
	/**
	 * 查询个人会员列表
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> queryPersonalMemberList(Map<String, Object> map);
	

	/**
	 * 查询商户会员列表
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> queryMerchantMemberList(Map<String, Object> map);
	
	/**
	 * 查看会员信息
	 * @param map
	 * @return
	 */
	Map<String, Object> queryMemberInfo(Map<String, Object> map);
	
	/**
	 * 更改账户状态
	 * @param map
	 */
	void updateAccountStatus(Map<String, Object> map);
	
}
