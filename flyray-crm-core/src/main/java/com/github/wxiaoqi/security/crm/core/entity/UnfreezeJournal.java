package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 解冻流水表
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-01 14:54:58
 */
@Table(name = "unfreeze_journal")
public class UnfreezeJournal implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
    
	    //冻结流水号
	@Column(name = "FREEZE_ID")
	private String freezeId;
	
	    //订单号
	@Column(name = "ORDER_NO")
	private String orderNo;
	
	    //订单类型  01：悬赏订单 02：提现订单
	@Column(name = "ORDER_TYPE")
	private String orderType;
	
	    //金额
    @Column(name = "BALANCE")
    private BigDecimal balance;
	
	    //时间
    @Column(name = "CREATETIME")
    private Timestamp createtime;
	

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}

	/**
	 * 获取：冻结流水号
	 */
	public String getFreezeId() {
		return freezeId;
	}
	/**
	 * 设置：冻结流水号
	 */
	public void setFreezeId(String freezeId) {
		this.freezeId = freezeId;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：订单类型  01：悬赏订单 02：提现订单
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	/**
	 * 获取：订单类型  01：悬赏订单 02：提现订单
	 */
	public String getOrderType() {
		return orderType;
	}
	/**
	 * 设置：金额
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * 设置：时间
	 */
	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：时间
	 */
	public Timestamp getCreatetime() {
		return createtime;
	}
}
