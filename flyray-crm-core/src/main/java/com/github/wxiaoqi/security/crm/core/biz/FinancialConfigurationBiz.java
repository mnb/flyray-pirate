package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.entity.FinancialConfiguration;
import com.github.wxiaoqi.security.crm.core.mapper.FinancialConfigurationMapper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

/**
 * 财务配置
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-06-20 14:19:07
 */
@Slf4j
@Service
public class FinancialConfigurationBiz extends BaseBiz<FinancialConfigurationMapper,FinancialConfiguration> {
	

	@Autowired
	private FinancialConfigurationMapper configurationMapper;
	/**
	 * 平台服务费计算
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:31:22
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformServiceFee(Map<String, Object> params){
		params.put("projectCode", "platformServiceFee");
		return platformFee(params);
	}
	/**
	 * 平台管理费计算
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:31:10
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformManagementFee(Map<String, Object> params){
		params.put("projectCode", "platformManagementFee");
		return platformFee(params);
	}
	/**
	 * 提现手续费计算
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:31:01
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformWithdrawFee(Map<String, Object> params){
		params.put("projectCode", "platformWithdrawFee");
		return platformFee(params);
	}
	/**
	 * 充值最小金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:30:43
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformRechargeAmtMin(Map<String, Object> params){
		params.put("projectCode", "platformRechargeAmtMin");
		return platformFee(params);
	}
	/**
	 * 悬赏单人最小金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:30:20
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformWithdrawAmtMin(Map<String, Object> params){
		params.put("projectCode", "platformWithdrawAmtMin");
		return platformFee(params);
	}
	/**
	 * 提现最小金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:30:12
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformRewardAmtMin(Map<String, Object> params){
		params.put("projectCode", "platformRewardAmtMin");
		return platformFee(params);
	}
	/**
	 * 查看简历悬赏方扣除金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:29:50
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformResumeDeductionAmt(Map<String, Object> params){
		params.put("projectCode", "platformResumeDeductionAmt");
		return platformFee(params);
	}
	/**
	 * 查看简历猎手的补贴金额
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:29:24
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformResumeSubsidiesAmt(Map<String, Object> params){
		params.put("projectCode", "platformResumeSubsidiesAmt");
		return platformFee(params);
	}

	
	
	/**
	 * 获取平台费用
	 * @author centerroot
	 * @time 创建时间:2018年6月20日下午3:21:03
	 * @param params
	 * @return
	 */
	public Map<String, Object> platformFee(Map<String, Object> params){
		log.info("获取平台费用   请求参数：{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = (String) params.get("platformId");
		String projectCode = (String) params.get("projectCode");
		String totalAmt = (String) params.get("totalAmt");
		BigDecimal feeAmt = BigDecimal.ZERO;
		
		Example example = new Example(FinancialConfiguration.class);
	    Example.Criteria criteria = example.createCriteria();
	    criteria.andEqualTo("platformId", platformId);
	    criteria.andEqualTo("projectCode", projectCode);
		List<FinancialConfiguration> financialConfigurations = mapper.selectByExample(example);
		if (null != financialConfigurations && financialConfigurations.size() > 0) {
			FinancialConfiguration financialConfiguration = financialConfigurations.get(0);
			Double value = financialConfiguration.getValue();
			if (null != value) {
				String valueStr = String.valueOf(value);
				if ("01".equals(financialConfiguration.getConfigType())) {
					feeAmt = new BigDecimal(valueStr);
				} else {
					if (!StringUtils.isEmpty(totalAmt)) {
						feeAmt = new BigDecimal(totalAmt).multiply(new BigDecimal(valueStr));
					}
				}
			}
		}

		log.info("计算平台费用   响应参数：{}",feeAmt.toString());
		feeAmt = feeAmt.setScale(2, RoundingMode.HALF_UP);//保留两位小数
		
		respMap.put("amount", feeAmt.toString());
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());

		log.info("获取平台费用   响应参数：{}",respMap);
		return respMap;
	}
	
	
	
	/**
	 * 新增财务配置
	 * @param map
	 */
	public void addConfiguration(Map<String, Object> map) {
		log.info("新增财务配置。。。{}"+map);
		String platformId = (String) map.get("platformId");
		String projectCode = (String) map.get("projectCode");
		String projectName = (String) map.get("projectName");
		String configType = (String) map.get("configType");
		String value = (String) map.get("value");
		FinancialConfiguration configuration = new FinancialConfiguration();
		configuration.setConfigType(configType);
		configuration.setPlatformId(platformId);
		configuration.setProjectCode(projectCode);
		configuration.setProjectName(projectName);
		configuration.setValue(Double.parseDouble(value)/100);
		configurationMapper.addConfiguration(configuration);
	}
	
	/**
	 * 修改财务配置
	 * @param map
	 */
	public void updateConfiguration(FinancialConfiguration configuration) {
		configurationMapper.updateConfiguration(configuration);
	}
	
	/**
	 * 查询列表
	 * @param map
	 * @return
	 */
	public List<FinancialConfiguration> queryList(Map<String, Object> map) {
		log.info("查询财务配置列表。。。{}"+map);
		return configurationMapper.queryConfiguration(map);
	}
}