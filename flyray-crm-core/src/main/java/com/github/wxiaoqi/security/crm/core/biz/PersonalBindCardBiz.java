package com.github.wxiaoqi.security.crm.core.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.crm.core.entity.Customer;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBindCard;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBindCardMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Base64Utils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.RSAUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 用户绑卡表
 *
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 16:28:16
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class PersonalBindCardBiz extends BaseBiz<PersonalBindCardMapper,PersonalBindCard> {

	@Autowired
	private PersonalBindCardMapper personalBindCardMapper;
	@Autowired
	private CustomerMapper customerMapper;
	@Value("${platform.cardNoPrivateKey}")
	private String cardNoPrivateKey;
	/**
	 * 添加用户银行卡信息
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午1:39:55
	 * @param param
	 * @return
	 */
	public Map<String, Object> savePersonalBindCardInfo(Map<String, Object> param){
		log.info("添加用户银行卡信息 请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String customerId = (String) param.get("customerId");
		Customer customer = customerMapper.selectByPrimaryKey(customerId);
		if (null != customer && null != customer.getPerId()) {
			
			PersonalBindCard personalBindCard = new PersonalBindCard();
			String seqNo = String.valueOf(SnowFlake.getId());
			personalBindCard.setSeqNo(seqNo);
			personalBindCard.setPerId(customer.getPerId());
			personalBindCard.setPayeeName((String) param.get("realName"));
			String bindEncryptCardNo = (String) param.get("bindEncryptCardNo");
			//银行卡解密
			String bankNo = null;
			byte[] andEnData = Base64Utils.decode(bindEncryptCardNo);
	    	try {
	    		byte[] dedata = RSAUtil.pcDecryptByPrivateKey(cardNoPrivateKey,andEnData);
	    		bankNo = new String(dedata);
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	}
			
			personalBindCard.setBindEncryptCardNo(bindEncryptCardNo);
			String startCardNo = bankNo.substring(0, 4);
			String endCardNo = bankNo.substring(bankNo.length()-4, bankNo.length());
			personalBindCard.setBindCardNo(startCardNo + " **** **** " + endCardNo);
			personalBindCard.setBankNo((String) param.get("bankNo"));
			personalBindCard.setBankName((String) param.get("bankName"));
			personalBindCard.setSubbranchName((String) param.get("subbranchName"));
			personalBindCard.setCreatetime(new Timestamp(System.currentTimeMillis()));
			personalBindCard.setStatus("00");
			personalBindCardMapper.insert(personalBindCard);
			
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		}else{
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}
		log.info("添加用户银行卡信息 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 删除用户银行卡信息
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午1:39:55
	 * @param param
	 * @return
	 */
	public Map<String, Object> deletePersonalBindCardInfo(Map<String, Object> param){
		log.info("删除用户银行卡信息 请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		try {
			param.put("status", "00");
			List<PersonalBindCard> personalBindCards = personalBindCardMapper.queryPersonalBindCardMap(param);
			if (null != personalBindCards && personalBindCards.size() > 0) {
				PersonalBindCard personalBindCard = personalBindCards.get(0);
				personalBindCard.setStatus("01");
				personalBindCardMapper.updateByPrimaryKey(personalBindCard);
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
			} else {
				respMap.put("code", ResponseCode.BIND_CARD_NOTEXIST.getCode());
				respMap.put("msg", ResponseCode.BIND_CARD_NOTEXIST.getMessage());
			}
		}catch(Exception e){
			e.printStackTrace();
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		
		log.info("删除用户银行卡信息 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询用户银行卡信息
	 * @author centerroot
	 * @time 创建时间:2018年5月21日下午1:39:55
	 * @param param
	 * @return
	 */
	public Map<String, Object> getPersonalBindCardInfoList(Map<String, Object> param){
		log.info("添加用户银行卡信息 请求参数：{}",param);
		Map<String, Object> respMap = new HashMap<String, Object>();
		String customerId = (String) param.get("customerId");
		int page = (int) param.get("page");
		int limit = (int) param.get("limit");
		Customer customer = customerMapper.selectByPrimaryKey(customerId);
		if (null != customer && null != customer.getPerId()) {
			PersonalBindCard personalBindCard = new PersonalBindCard();
			personalBindCard.setPerId(customer.getPerId());
			personalBindCard.setStatus("00");
			
			Map<String, Object> params = new HashMap<String, Object>();
		    params.put("page", page);
		    params.put("limit", limit);
		    Query query = new Query(params);
		    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
			List<PersonalBindCard> custBindCards = personalBindCardMapper.select(personalBindCard);
			TableResultResponse<PersonalBindCard> table = new TableResultResponse<PersonalBindCard>(result.getTotal(), custBindCards);
			respMap.put("body", table);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		}else{
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}
		
		log.info("添加用户银行卡信息 响应参数：{}",respMap);
		return respMap;
	}
}