package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalVerifyRecord;

import tk.mybatis.mapper.common.Mapper;

/**
 * 个人基础信息
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 15:20:48
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBaseMapper extends Mapper<PersonalBase> {
	
	
	/**
	 * 查询个人实名记录
	 * @author chj
	 * @return
	 */
	List<PersonalVerifyRecord> queryRealNameRecord(Map<String, Object> param);
	
	/**
	 * 修改实名状态
	 * @param map
	 */
	void updateVerifyFlag(Map<String, Object> map);
}
