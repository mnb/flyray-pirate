package com.github.wxiaoqi.security.crm.core.feign;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.CustomerBiz;
import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseExtBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("feign/merchants/account")
public class MerchantAccountFeign {
	@Autowired
	private CustomerBiz customerBiz;
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	@Autowired
	private PersonalAccountBiz personalAccountBiz;
	@Autowired
	private MerchantAccountBiz merchantAccountBiz;
	
	@Autowired
	private PersonalBaseExtBiz personalBaseExtBiz;
	
	/**
	 * 查询企业账户信息
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/queryMerchantAccount", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryMerchantAccount(@RequestBody Map<String, Object> param) {
		log.info("查询用户账户信息 请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.queryMerchantAccountInfo(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查询用户账户信息 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 企业账户资金冻结
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/freezeAmount", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> freezeAmount(@RequestBody Map<String, Object> param) {
		log.info("企业账户资金冻结 请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.freeze(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("企业账户资金冻结 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 企业账户资金解冻
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/unfreezeAmount", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> unfreezeAmount(@RequestBody Map<String, Object> param) {
		log.info("企业账户资金冻结 请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.unfreeze(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("企业账户资金冻结 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 企业账户资金解冻并转账
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/unfreezeTransfer", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> unfreezeTransfer(@RequestBody Map<String, Object> param) {
		log.info("企业账户资金解冻并转账    请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.unfreezeTransfer(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("企业账户资金解冻并转账    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 企业邀请面试扣除次数或金额
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/inviteToInterview", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> inviteToInterview(@RequestBody Map<String, Object> param) {
		log.info("企业邀请面试扣除次数或金额    请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.inviteToInterview(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("企业邀请面试扣除次数或金额    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 线下充值
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/offlineRecharge", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> offlineRecharge(@RequestBody Map<String, Object> param) {
		log.info("线下充值    请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.offlineRecharge(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("线下充值    响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 企业账户入金
	 * @author centerroot
	 * @time 创建时间:2018年6月14日下午5:13:53
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deposit(@RequestBody Map<String, Object> param) {
		log.info("企业账户入金 请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.deposit(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("企业账户入金 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 企业账户出金
	 * @author centerroot
	 * @time 创建时间:2018年6月14日下午5:14:04
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> withdraw(@RequestBody Map<String, Object> param) {
		log.info("企业账户出金 请求参数：{}",param);
		Map<String, Object> respMap = merchantAccountBiz.withdraw(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("企业账户出金 响应参数：{}",respMap);
		return respMap;
	}

}
