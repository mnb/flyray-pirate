package com.github.wxiaoqi.security.crm.core.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.admin.pay.request.Testparam;
import com.github.wxiaoqi.security.common.admin.pay.response.QueryBankCodeResponse;
import com.github.wxiaoqi.security.crm.core.client.BankCodeBizService;

@RestController
@RequestMapping("test")
public class TestAction {
	
	@Autowired
	private BankCodeBizService bankCodeBizService;

	@RequestMapping(value = "/havetest",method = RequestMethod.POST)
	public QueryBankCodeResponse queryBankCode(@RequestBody Testparam testparam){
		QueryBankCodeResponse resp= new QueryBankCodeResponse();
		resp.setCode("200");
		resp.setMsg("is ok");
		return resp;
	}

}
