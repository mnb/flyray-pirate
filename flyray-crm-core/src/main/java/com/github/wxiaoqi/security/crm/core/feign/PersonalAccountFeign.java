package com.github.wxiaoqi.security.crm.core.feign;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.crm.core.biz.CustomerBiz;
import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseExtBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("feign/personals/account")
public class PersonalAccountFeign {
	@Autowired
	private CustomerBiz customerBiz;
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	@Autowired
	private PersonalAccountBiz personalAccountBiz;
	@Autowired
	private MerchantAccountBiz merchantAccountBiz;
	
	@Autowired
	private PersonalBaseExtBiz personalBaseExtBiz;
	
	/**
	 * 查询用户账户信息
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/queryPersonalAccount", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryPersonalAccount(@RequestBody Map<String, Object> param) {
		log.info("查询用户账户信息 请求参数：{}",param);
		Map<String, Object> respMap = personalAccountBiz.queryPersonalAccountInfo(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("查询用户账户信息 响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 用户账户余额冻结
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/freeze", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> freeze(@RequestBody Map<String, Object> param) {
		log.info("用户账户余额冻结 请求参数：{}",param);
		Map<String, Object> respMap = personalAccountBiz.freeze(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("用户账户余额冻结 响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 用户账户余额解冻
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/unfreeze", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> unfreeze(@RequestBody Map<String, Object> param) {
		log.info("用户账户余额解冻 请求参数：{}",param);
		Map<String, Object> respMap = personalAccountBiz.unfreeze(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("用户账户余额解冻 响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 提现（解冻并出账）
	 * @author centerroot
	 * @time 创建时间:2018年6月4日上午11:46:57
	 * @param reqs
	 * @return
	 */
	@RequestMapping(value = "/unfreezeDebit", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> unfreezeDebit(@RequestBody Map<String, Object> param) {
		log.info("提现（解冻并出账） 请求参数：{}",param);
		Map<String, Object> respMap = personalAccountBiz.unfreezeDebit(param);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("sucess", true);
		} else {
			respMap.put("sucess", false);
		}
		log.info("提现（解冻并出账） 响应参数：{}",respMap);
		return respMap;
	}

}
