package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * 
 * 
 * @author centerroot
 * @email lfw6699@163.com
 * @date 2018-05-25 16:17:27
 */
public class CustomerMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private String msgId;
	
	    //消息类型
    private String msgType;
	
	    //消息标题
    private String msgTitle;
	
	    //消息内容
    private String msgContent;
	
	    //是否已读  00：未读  01：已读
    private String isread;
	
	    //创建时间
    private Timestamp createtime;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMsgTitle() {
		return msgTitle;
	}

	public void setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public String getIsread() {
		return isread;
	}

	public void setIsread(String isread) {
		this.isread = isread;
	}

	public Timestamp getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}

	@Override
	public String toString() {
		return "CustomerMessage [msgId=" + msgId + ", msgType=" + msgType + ", msgTitle=" + msgTitle + ", msgContent="
				+ msgContent + ", isread=" + isread + ", createtime=" + createtime + "]";
	}

}
