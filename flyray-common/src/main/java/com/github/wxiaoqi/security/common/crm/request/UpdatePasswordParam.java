package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("修改用户登录密码")
public class UpdatePasswordParam {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	@ApiModelProperty("旧密码")
	private String oldPassword;
	@NotNull(message="新密码不能为空")
	@ApiModelProperty("新密码")
	private String password;
}
