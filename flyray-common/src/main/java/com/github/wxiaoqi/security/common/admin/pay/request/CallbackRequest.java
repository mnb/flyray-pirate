package com.github.wxiaoqi.security.common.admin.pay.request;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 回调请求
 * @author hexufeng
 *
 */
public class CallbackRequest {

    /**
     * 支付通道编号
     */
    private String payChannelNo;
    /**
     * 支付请求号
     */
    private String requestNo;
    /**
     * 第三方交易流水号
     */
    private String remoteTxJournalNo;
    /**
     * 第三方交易日期
     */
    private Date remoteTxtDate;
    /**
     * 手续费
     */
    private BigDecimal fee;
    /**
     * 交易金额
     */
    private BigDecimal orderAmt;
    /**
     * 交易状态
     */
    private String status;
    /**
     * 错误码
     */
    private String code;
    /**
     * 错误消息
     */
    private String msg;

    public String getPayChannelNo() {
        return payChannelNo;
    }

    public void setPayChannelNo(String payChannelNo) {
        this.payChannelNo = payChannelNo;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getRemoteTxJournalNo() {
        return remoteTxJournalNo;
    }

    public void setRemoteTxJournalNo(String remoteTxJournalNo) {
        this.remoteTxJournalNo = remoteTxJournalNo;
    }

    public Date getRemoteTxtDate() {
        return remoteTxtDate;
    }

    public void setRemoteTxtDate(Date remoteTxtDate) {
        this.remoteTxtDate = remoteTxtDate;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getOrderAmt() {
        return orderAmt;
    }

    public void setOrderAmt(BigDecimal orderAmt) {
        this.orderAmt = orderAmt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
