package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/**
 * 代付状态查询响应
 * @author hexufeng
 *
 */
public class PayForAnotherQueryResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 代付状态 RECEIVE 已接收 INIT初始化 DOING处理中 SUCCESS成功 FAIL失败 REFUND退款

	 */
	private String orderStatus;

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

}
