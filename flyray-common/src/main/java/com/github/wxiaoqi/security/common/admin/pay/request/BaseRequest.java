package com.github.wxiaoqi.security.common.admin.pay.request;

/**
 * 请求参数基类
 * */
public class BaseRequest {
	private String version;//接口版本号
	private String sign;//签名
	private String reqJrnNo;//请求流水号
	private String reqDatetime;//请求时间戳
	private String txCode;//请求方法名
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getReqJrnNo() {
		return reqJrnNo;
	}
	public void setReqJrnNo(String reqJrnNo) {
		this.reqJrnNo = reqJrnNo;
	}
	public String getReqDatetime() {
		return reqDatetime;
	}
	public void setReqDatetime(String reqDatetime) {
		this.reqDatetime = reqDatetime;
	}
	public String getTxCode() {
		return txCode;
	}
	public void setTxCode(String txCode) {
		this.txCode = txCode;
	}
	
	
}
