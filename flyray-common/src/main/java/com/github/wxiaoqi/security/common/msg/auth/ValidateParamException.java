package com.github.wxiaoqi.security.common.msg.auth;

import com.github.wxiaoqi.security.common.exception.BaseException;

/**
 * 验证参数是否准确
 * 
 */
public class ValidateParamException extends BaseException {
	private String code;
	private String msg;
	private boolean sucess;

	
	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getMsg() {
		return msg;
	}



	public void setMsg(String msg) {
		this.msg = msg;
	}



	public boolean isSucess() {
		return sucess;
	}



	public void setSucess(boolean sucess) {
		this.sucess = sucess;
	}



	public ValidateParamException(String code, String msg, boolean sucess) {
		this.code = code;
		this.msg = msg;
		this.sucess = sucess;
	}

	

}
