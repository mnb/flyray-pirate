package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/**
 * 代付订单查询响应
 * @author hexufeng
 *
 */
public class QueryPayForAnotherOrderStatusResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 代付状态
	 */
	private String payForAnotherStatus;

	public String getPayForAnotherStatus() {
		return payForAnotherStatus;
	}

	public void setPayForAnotherStatus(String payForAnotherStatus) {
		this.payForAnotherStatus = payForAnotherStatus;
	}

}
