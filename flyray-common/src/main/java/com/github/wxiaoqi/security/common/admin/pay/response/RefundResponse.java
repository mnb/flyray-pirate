package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/** 
* @author: bolei
* @date：2017年2月23日 下午12:33:08 
* @description：类说明 
*/

public class RefundResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
    
    private String status;
    
    private String outHtml;
    
    private String errorMsg;

    public String getOutHtml() {
        return outHtml;
    }

    public void setOutHtml(String outHtml) {
        this.outHtml = outHtml;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }


}
