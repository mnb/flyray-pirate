package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;
import java.util.List;

/**
 * 银行列表查询响应
 * @author hexufeng
 *
 */
public class QueryBankCodeResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<BankCode> bankList;

	public List<BankCode> getBankList() {
		return bankList;
	}

	public void setBankList(List<BankCode> bankList) {
		this.bankList = bankList;
	}

}
