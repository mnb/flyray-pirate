package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/**
 * 支付订单查询响应
 * @author hexufeng
 *
 */
public class QueryRefundOrderStatusResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 退款状态
	 */
	private String refundStatus;

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

}
