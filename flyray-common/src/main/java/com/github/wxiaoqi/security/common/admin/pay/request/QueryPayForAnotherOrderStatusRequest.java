package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/**
 * 代付订单查询请求
 * @author hexufeng
 *
 */
public class QueryPayForAnotherOrderStatusRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 平台编号
	 */
	private String platformId;
	
	/**
	 * 代付订单号
	 */
	private String orderId;

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
