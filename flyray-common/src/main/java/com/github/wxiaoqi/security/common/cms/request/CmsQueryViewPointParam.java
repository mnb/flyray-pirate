package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询社区观点参数")
public class CmsQueryViewPointParam {
	@NotNull(message="商户号不能为空")
	@ApiModelProperty("商户号")
	private String merchantId;
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String customerId;
	@ApiModelProperty("观点类型")
	private String viewType;
	@ApiModelProperty("当前页")
	private Integer currentPage;
	@ApiModelProperty("数据条数")
	private Integer pageSize;
}
