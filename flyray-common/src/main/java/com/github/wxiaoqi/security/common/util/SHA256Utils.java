package com.github.wxiaoqi.security.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
* @author : bolei
* @version : 2017年3月18日 上午11:44:08
* @description : sha-256加密
*/

public class SHA256Utils {

	/** 
	   * 传入文本内容，返回 SHA-256 串 
	   *  
	   * @param strText 
	   * @return 
	   */  
	  public static String SHA256(final String strText)  
	  {  
	    return SHA(strText, "SHA-256");  
	  }  
	  
	  /** 
	   * 传入文本内容，返回 SHA-512 串 
	   *  
	   * @param strText 
	   * @return 
	   */  
	  public String SHA512(final String strText)  
	  {  
	    return SHA(strText, "SHA-512");  
	  }  
	  
	  /** 
	   * 字符串 SHA 加密 
	   *  
	   * @param strSourceText 
	   * @return 
	   */  
	  private static String SHA(final String strText, final String strType)  
	  {  
	    // 返回值  
	    String strResult = null;  
	  
	    // 是否是有效字符串  
	    if (strText != null && strText.length() > 0)  
	    {  
	      try  
	      {  
	        // SHA 加密开始  
	        // 创建加密对象 并傳入加密類型  
	        MessageDigest messageDigest = MessageDigest.getInstance(strType);  
	        // 传入要加密的字符串  
	        messageDigest.update(strText.getBytes());  
	        // 得到 byte 類型结果  
	        byte byteBuffer[] = messageDigest.digest();  
	  
	        // 將 byte 轉換爲 string  
	        StringBuffer strHexString = new StringBuffer();  
	        // 遍歷 byte buffer  
	        for (int i = 0; i < byteBuffer.length; i++)  
	        {  
	          String hex = Integer.toHexString(0xff & byteBuffer[i]);  
	          if (hex.length() == 1)  
	          {  
	            strHexString.append('0');  
	          }  
	          strHexString.append(hex);  
	        }  
	        // 得到返回結果  
	        strResult = strHexString.toString();  
	      }  
	      catch (NoSuchAlgorithmException e)  
	      {  
	        e.printStackTrace();  
	      }  
	    }  
	  
	    return strResult;  
	  } 
	  
	  public static void main(String args[]){
		  //dc483e80a7a0bd9ef71d8cf973673924
		  //34635c159694bf921e19eb8f3e939367
//	     String s=SHA256("123456");
//	     String pwd = SHA256(s+"ceshi11");
//	     System.out.println(MD5.md5(pwd));
	    
	    // System.out.println(MD5.md5("123456"));
	    }
	  
}


