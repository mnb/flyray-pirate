package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/**
 * 代付查询请求
 * @author hexufeng
 *
 */
public class PayForAnotherQueryRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;

	//外部订单号
	private String outOrderNo;

	//平台编号
	private String platformId;

	// 支付通道编号
	private String payChannelNo;

	//支付公司编号
	private String payCompanyNo;

	//商户号
	private String merId;

	public String getOutOrderNo() {
		return outOrderNo;
	}

	public void setOutOrderNo(String outOrderNo) {
		this.outOrderNo = outOrderNo;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getPayChannelNo() {
		return payChannelNo;
	}

	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}

	public String getPayCompanyNo() {
		return payCompanyNo;
	}

	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}

	public String getMerId() {
		return merId;
	}

	public void setMerId(String merId) {
		this.merId = merId;
	}
	
}
